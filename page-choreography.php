<?php

global $root_url_path;

get_header(); ?>

    <div class="main page__main">
        <div class="main__header">
            <div class="main__container container">
                <h3 class="main__title">Хореография</h3>
                <ul class="breadcrumbs main__breadcrumbs">
                    <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/">Главная</a>
                    </li>
                    <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Хореография</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main__content" style="z-index:-1;position: relative;">
            <div class="terms main__terms">
                <div class="terms__container container">
                    <div class="terms__body">
                        <div class="terms__section">
                            <div class="terms__content">
                                <p><strong><?php Output('first_tb'); ?> </strong> <?php Output('first_par'); ?></p>
                                <div class="terms__grid">
                                    <div class="terms__column">
                                    	<a href="<?php OutputImage("image1"); ?>" data-fancybox="at">
                                        	<img class="terms__img" src="<?php OutputImage("image1"); ?>" alt="">
                                        </a>
                                    </div>
                                    <div class="terms__column">
                                    	<a href="<?php OutputImage("image2"); ?>" data-fancybox="at">
                                        	<img class="terms__img" src="<?php OutputImage("image2"); ?>" alt="">
                                        </a>
                                    </div>
                                </div>
                                <p><?php Output('second_par'); ?></p>
                            </div>
                        </div>
                        <div class="terms__section">
                            <div class="terms__head">
                                <h3 class="terms__title"><?php Output('first_tittle'); ?></h3>
                            </div>
                            <div class="terms__content">
                                <p><?php Output('third_par1'); ?></p>
                                <p><?php Output('third_par2'); ?></p>
                            </div>
                        </div>
                        <div class="terms__section">
                            <div class="terms__head">
                                <h3 class="terms__title"><?php Output('second_tittle'); ?></h3>
                            </div>
                            <div class="terms__content">
                                <?php if(false)
                                    {?>
                                <div class="video terms__video">
                                    <a class="video__link" href="<?php echo $root_url_path;?>/static/img/general/vd-1.png" data-fancybox="">
                                        <img class="video__img" src="<?php echo $root_url_path;?>/static/img/general/vd-1.png" alt="">
                                        <div class="video__actions">
                                            <img class="video__icon" src="<?php echo $root_url_path;?>/static/img/general/pl-ic.png" alt=""><span class="video__text">Посмотреть ролик</span>
                                        </div>
                                    </a>
                                </div>
                                        <?php }?>
                                <p><?php Output('fourth_par1'); ?> <a href="<?php Output('link_text'); ?>"><?php Output('name_text'); ?></a><?php Output('fourth_par2'); ?></p>
                                <ul>
                                    <li style="padding:0;">
                                        <?php

                                        ?>
                                        <ul>

                                            <?php OutputMarkedList(get_theme_mod('mark_list')); ?>

                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php get_footer();
