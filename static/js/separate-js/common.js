$(function (){

    var obj = {
        window: $(window),
        html: $('html'),
        body: $('body'),
    }

    $("input[type=tel]").mask("+7 (999) 999 99 99");


    $('.pupils__button, .header__button--order').on('click', function(){
        $('.modal--order').addClass('visible');
        orderSwiper.init();
    });

    $('.price-card__cost-text').on('click', function(){
        return false;
    });


    $('input[type=radio], input[type=checkbox], input[type=file], .students__box-select').styler();

    $('.jq-selectbox__dropdown ul').addClass('scrollbar-inner');

    $('.scrollbar-inner, .js-coach-card-scroll, .js-leasons-scrollbar').scrollbar();

    $('.js-scroll-to').on('click', function(event) {
        event.preventDefault();

        var section = $($(this).attr('href'));

          $('html, body').animate({scrollTop: section.offset().top});
    }); 


    $('.place__header').on('click', function(){
        $(this).toggleClass('opened');
        $(this).parent('.place__section').children('.place__info').slideToggle();
    });


    $('.filter__checkbox').on('click', function () {
        if ($(this).hasClass('checked')) {
            $(this).parent('.filter__label').parent('.filter__item').addClass('checked');
        }  else {
            $(this).parent('.filter__label').parent('.filter__item').removeClass('checked');
        }
    });

     $('.leasons__header').on('click', function () {
        $(this).next().slideToggle(200).parent().siblings().find('.leasons__dropdown').slideUp(200);
        $(this).next().parent().toggleClass('active').siblings().removeClass('active');
    });


    // var postion = $('.coach__content').offset().top,
    //     height = $('.coach__content').height();
    // $(document).on('scroll', function (){
    //   var scroll = $(document).scrollTop();
    //   if(scroll  > postion && scroll < (postion + height) ) {
    //      $('.coach__wrp').addClass('scrolled')
    //      }else {
    //         $('.coach__wrp').removeClass('scrolled')
    //      }
    // })

    // var postion = $('.coach__content').offset().top,
    //     height = $('.coach__content').height();
    // $(document).on('scroll', function (){
    //   var scroll = $(document).scrollTop();
    //   if(scroll  > postion && scroll < (postion + height) ) {
    //      $('.coach__wrp').addClass('scrolled')
    //      }else {
    //         $('.coach__wrp').removeClass('scrolled')
    //      }
    // })



    $('.students__radio').on('click', function () {
        if ($(this).hasClass('checked')) {
            $(this).parent('.students__label').parent('.students__item').addClass('checked').siblings().removeClass('checked');
        }   
    });

    $('.footer__title').on('click', function () {
      if ($(window).width() < 480) {
        $(this).next().slideToggle(200).parent().siblings().find('.footer__list').slideUp(200);
        $(this).next().parent().toggleClass('active').siblings().removeClass('active');
      }
    });

    $('.menu-btn').on('click', function(){
        return false;
    });

    $('.students__box-dropdown-chekbox').on('click', function (){
        if ($(this).hasClass('checked')) {
            $(this).parent().parent().addClass('checked');
        } else {
            $(this).parent().parent().removeClass('checked');
        }
    }); 

    $('.selection__row--age .selection__radio').on('click', function () {
        if ($(this).hasClass('checked')) {
            $(this).parent('.selection__label').parent('.selection__item').addClass('checked').siblings().removeClass('checked color');
        }   
    });


    $('.selection__row--type .selection__radio').on('click', function () {
        if ($(this).hasClass('checked')) {
            $(this).parent('.selection__label').parent('.selection__item').addClass('checked').siblings().removeClass('checked');
        }   
    });

    $('.selection__row--quantity .selection__radio').on('click', function () {
        if ($(this).hasClass('checked')) {
            $(this).parent('.selection__label').parent('.selection__item').addClass('checked color').siblings().removeClass('checked color');
        }   
    });


    $('.students__box-age').on('click', function() {
        $('.students__box-dropdown').slideToggle('fast');
        $(this).toggleClass('active');
    });


    $(document).click( function(event){
        if( $(event.target).closest(".students__dropbox, .students__box-age, .students__box-dropdown").length ) 
        return;
        $('.students__box-age').removeClass('active');
        $(".students__box-dropdown").fadeOut("fast");
        event.stopPropagation();
    });


    var informerSwiper = new Swiper('.js-informer-swiper', {
        slidesPerView: 1,
        autoplay: {
            delay: 5000,
        },
        pagination: {
            el: '.informer__swiper-controls .swiper-controls__pagination',
            type: 'bullets',
            clickable: true,
        },
    });

    var pupilsSwiper = new Swiper('.js-pupils-swiper', {
        slidesPerView: 4,
        spaceBetween: 20,
        loop: true,
        pagination: {
            el: '.pupils__swiper-controls .swiper-controls__pagination',
            type: 'bullets',
            clickable: true,
        },

        navigation: {
            nextEl: '.pupils__swiper-controls .swiper-controls__button--next',
            prevEl: '.pupils__swiper-controls .swiper-controls__button--prev',
          },

        breakpoints: {
            992: {
                slidesPerView: 3,
            },

            767: {
                slidesPerView: 2,
            },
        }
    });


    var gallerySwiper = new Swiper('.js-gallery-swiper', {
        slidesPerView: 5,
        spaceBetween: 45,
        loop: true,
        autoplay: {
            delay: 5000,
        },
        pagination: {
            el: '.gallery__swiper-controls .swiper-controls__pagination',
            type: 'bullets',
            clickable: true,
        },

        navigation: {
            nextEl: '.gallery__swiper-controls .swiper-controls__button--next',
            prevEl: '.gallery__swiper-controls .swiper-controls__button--prev',
        },

        breakpoints: {
            1320: {
                spaceBetween: 5,
            },

            1200: {
                slidesPerView: 3,
                spaceBetween: 10,
            },

            992: {
                slidesPerView: 2,
                spaceBetween: 10,
            },

            767: {
                slidesPerView: 1,
            },
        }
    });


    $(window).resize(function() {
      gallerySwiper.update();
    });

    var reviewsSwiper = new Swiper('.js-reviews-swiper', {
        slidesPerView: 4,
        spaceBetween: 20,
        loop: true,
        navigation: {
            nextEl: '.reviews__swiper-controls .swiper-controls__button--next',
            prevEl: '.reviews__swiper-controls .swiper-controls__button--prev',
        },

        breakpoints: {
            992: {
                slidesPerView: 2,
            },

            639: {
                slidesPerView: 1,
            },
        }
    });

    var sertificatesSwiper = new Swiper('.js-sertificates-swiper', {
        slidesPerView: 4,
        spaceBetween: 20,
        loop: true,
        navigation: {
            nextEl: '.sertificates__swiper-controls .swiper-controls__button--next',
            prevEl: '.sertificates__swiper-controls .swiper-controls__button--prev',
        },

        breakpoints: {
            992: {
                slidesPerView: 3,
            },

            640: {
                slidesPerView:2,
                autoplay: {
                    delay: 2500,
                    disableOnInteraction: false,
                }, 
            },
            480: {
                slidesPerView:1,
            },
        }
    });

    var mastersThumbs = new Swiper('.js-masters-thumbs-swiper', {
        spaceBetween: 30,
        slidesPerView: 3,    
        loop: true, 
        loopedSlides: 3, 
        allowTouchMove: false,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        direction: 'vertical',
        pagination: {
            el: '.masters__swiper-controls .swiper-controls__pagination',
            type: 'fraction',
        },
        navigation: {
            nextEl: '.masters__swiper-controls .swiper-controls__button--next',
            prevEl: '.masters__swiper-controls .swiper-controls__button--prev',
        },

        breakpoints: {

            480: {
                slidesPerView: 2,
                spaceBetween: 10,
            },
        }
    });

    var captionSwiper = new Swiper('.js-caption-swiper', {
        spaceBetween: 30,
        slidesPerView: 1,    
        loop: true,
        effect: 'fade',
        allowTouchMove: false,
        navigation: {
            nextEl: '.masters__swiper-controls .swiper-controls__button--next',
            prevEl: '.masters__swiper-controls .swiper-controls__button--prev',
        }

    });

    var orderSwiper = new Swiper('.js-order-swiper', {
    	init: false,
        spaceBetween: 10,
        slidesPerView: 1,
        loop: true,
        autoplay: {
	        delay: 2500,
	        disableOnInteraction: false,
	    }, 
        pagination: {
            el: '.modal__swiper-controls .swiper-controls__pagination',
            clickable: true,
        }
    });

    var detailSwiper = new Swiper('.js-detail-swiper', {
        spaceBetween: 15,
        slidesPerView: 4,    
        navigation: {
            nextEl: '.detail__swiper-controls .swiper-controls__button--next',
            prevEl: '.detail__swiper-controls .swiper-controls__button--prev',
        },
        breakpoints: {
            992: {
                slidesPerView: 3,
            },
            640: {
                slidesPerView: 2,
            },
            480: {
                slidesPerView: 1,
            },
        }
    });


    



    $('.masters-thumbs--first, .masters-thumbs--1').on('click', function() { 
        mastersSwiper.slideToLoop(0);
        mastersThumbs.slideToLoop(0);
        captionSwiper.slideToLoop(0);
    }); 

    $('.masters-thumbs--second, .masters-thumbs--2').on('click', function() { 
        mastersSwiper.slideToLoop(1);
        mastersThumbs.slideToLoop(1);
        captionSwiper.slideToLoop(1);
    });

    $('.masters-thumbs--third, .masters-thumbs--3').on('click', function() { 
        mastersSwiper.slideToLoop(2);
        mastersThumbs.slideToLoop(2);
        captionSwiper.slideToLoop(2);
    });

    $('.masters-thumbs--fourth, .masters-thumbs--4').on('click', function() { 
        mastersSwiper.slideToLoop(3);
        mastersThumbs.slideToLoop(3);
        captionSwiper.slideToLoop(3);
    });

    $('.masters-thumbs--fifth, .masters-thumbs--5').on('click', function() { 
        mastersSwiper.slideToLoop(4);
        mastersThumbs.slideToLoop(4);
        captionSwiper.slideToLoop(4);
    });

    $('.masters-thumbs--sixth, .masters-thumbs--6').on('click', function() { 
        mastersSwiper.slideToLoop(5);
        mastersThumbs.slideToLoop(5);
        captionSwiper.slideToLoop(5);
    });

    $('.masters-thumbs--seventh, .masters-thumbs--7').on('click', function() { 
        mastersSwiper.slideToLoop(6);
        mastersThumbs.slideToLoop(6);
        captionSwiper.slideToLoop(6);
    });

    $('.masters-thumbs--eighth, .masters-thumbs--8').on('click', function() { 
        mastersSwiper.slideToLoop(7);
        mastersThumbs.slideToLoop(7);
        captionSwiper.slideToLoop(7);
    });

    $('.masters-thumbs--ninth, .masters-thumbs--9').on('click', function() { 
        mastersSwiper.slideToLoop(8);
        mastersThumbs.slideToLoop(8);
        captionSwiper.slideToLoop(8);
    });

    $('.masters-thumbs--tenth, .masters-thumbs--10').on('click', function() { 
        mastersSwiper.slideToLoop(9);
        mastersThumbs.slideToLoop(9);
        captionSwiper.slideToLoop(9);
    });

    $('.masters-thumbs--eleventh, .masters-thumbs--11').on('click', function() { 
        mastersSwiper.slideToLoop(10);
        mastersThumbs.slideToLoop(10);
        captionSwiper.slideToLoop(10);
    });



    var mastersSwiper = new Swiper('.js-masters-swiper', {
        slidesPerView: 1,
        loop: true,
        spaceBetween: 10,
        allowTouchMove: false,

        navigation: {
            nextEl: '.masters__swiper-controls .swiper-controls__button--next',
            prevEl: '.masters__swiper-controls .swiper-controls__button--prev',
        },
    });


    $('.form__input').on('focus', function(){
        $(this).parent().addClass('active');
    });

    $('.form__input').on('blur', function(){
        $(this).parent().removeClass('active');
    });


   $('.progressBarContainer').slick({
        slidesToShow: 3,
        asNavFor: '.slider',
        dots: true,
        centerMode: true,
        pauseOnHover: false,
        focusOnSelect: true,
        infinite: false,
    });


   $(".slider").slick({
        arrows: false,
        dots: false,
        autoplay: false,
        speed: 1000,
        fade: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        asNavFor: '.progressBarContainer'
    })

    var percentTime;
    var tick;
    var time = 1;
    var progressBarIndex = 0;

    $('.progressBarContainer .progressBar').each(function(index) {
        var progress = "<div class='inProgress inProgress" + index + "'></div>";
        $(this).html(progress);
    });

    function startProgressbar() {
        resetProgressbar();
        percentTime = 0;
        tick = setInterval(interval, 10);
    }

    function interval() {
        if (($('.slider .slick-track div[data-slick-index="' + progressBarIndex + '"]').attr("aria-hidden")) === "true") {
            progressBarIndex = $('.slider .slick-track div[aria-hidden="false"]').data("slickIndex");
            startProgressbar();
        } else {
            percentTime += 1 / (time + 5);
            $('.inProgress' + progressBarIndex).css({
                width: percentTime + "%"
            });
            if (percentTime >= 100) {
                $('.single-item').slick('slickNext');
                progressBarIndex++;
                if (progressBarIndex > 2) {
                    progressBarIndex = 0;
                }
                startProgressbar();
            }
        }
    }

    function resetProgressbar() {
        $('.inProgress').css({
            width: 0 + '%'
        });
        clearInterval(tick);
    }
    startProgressbar();
    // End ticking machine

    $('.progressBarContainer__inner').click(function () {
        clearInterval(tick);
        var goToThisIndex = $(this).data("slickIndex");
        $('.single-item').slick('slickGoTo', goToThisIndex, false);
        startProgressbar();
    });

    var pass = $('#password');
    $('.form__show-pass').click(function() {
        $(this).toggleClass('active');
        pass.attr('type', pass.attr('type') === 'password' ? 'text' : 'password');
        return false;
    });


    var galleryThumbs = new Swiper('.js-artcile-thumbs', {
        spaceBetween: 10,
        slidesPerView: 3,
        freeMode: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        breakpoints: {
            639: {
                slidesPerView: 2,
            },
        }
    });

    var galleryTop = new Swiper('.js-artcile-swiper', {
      spaceBetween: 10,
      navigation: {
        nextEl: '.article__swiper-controls .swiper-controls__button--next',
        prevEl: '.article__swiper-controls .swiper-controls__button--prev',
      },
      thumbs: {
        swiper: galleryThumbs
      }
    });

    var otherSwiper = new Swiper('.js-other-swiper', {
        slidesPerView: 3,
        spaceBetween: 30,
        navigation: {
            nextEl: '.other__swiper-controls .swiper-controls__button--next',
            prevEl: '.other__swiper-controls .swiper-controls__button--prev',
        },
        breakpoints: {
            992: {
                slidesPerView: 2,
            },
            639: {
                slidesPerView: 1,
            },
        }
    });

});
