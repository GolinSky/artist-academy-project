<?php
global $root_url_path;

?>

<?php get_header(); ?>
        <div class="main page__main">
            <div class="main__header">
                <div class="main__container container">
                    <h3 class="main__title">Цены на услуги</h3>
                    <ul class="breadcrumbs main__breadcrumbs">
                        <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/">Главная</a>
                        </li>
                        <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Цены</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="main__content">
                <div class="price main__price">
                    <div class="price__container container">
                        <div class="price__content">
                            <div class="price__grid">
                                <div class="price__column">
                                    <div class="price-card">
                                        <div class="price-card__gradient"></div>
                                        <div class="price-card__inner">
                                            <img class="price-card__inner-label" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/intro-label.png" width="181" height="177" alt="">
                                            <div class="price-card__header"><span class="price-card__title">Базовый</span>
                                                <ul class="price-card__age-list">
                                                    <li class="price-card__age-item price-card__age-item--red"><span class="price-card__age-text">3-5 лет</span>
                                                    </li>
                                                    <li class="price-card__age-item price-card__age-item--orange"><span class="price-card__age-text">6-8 лет</span>
                                                    </li>
                                                    <li class="price-card__age-item price-card__age-item--gold"><span class="price-card__age-text">9-12 лет</span>
                                                    </li>
                                                    <li class="price-card__age-item price-card__age-item--gray"><span class="price-card__age-text">18+ лет</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="price-card__content">
                                                <ul class="price-card__list">
                                                    <li class="price-card__item"><img class="price-card__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/price-1.png" alt=""><span class="price-card__text">Актёрское мастерство</span>
                                                    </li>
                                                    <li class="price-card__item"><img class="price-card__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/price-2.png" alt=""><span class="price-card__text">Вокал</span>
                                                    </li>
                                                    <li class="price-card__item"><img class="price-card__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/price-3.png" alt=""><span class="price-card__text">Хореография</span>
                                                    </li>
                                                </ul>
                                                <ul class="price-card__sked-list">
                                                    <li class="price-card__sked-item"><img class="price-card__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/price-4.png" alt=""><span class="price-card__text">12 занятий (по 3 занятия в день)</span>
                                                    </li>
                                                    <li class="price-card__sked-item"><img class="price-card__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/price-5.png" alt=""><span class="price-card__text">1 раз в неделю</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="price-card__footer">
                                            <ul class="price-card__cost-list">
                                                <li class="price-card__cost-item"><span class="price-card__cost-label"><img class="price-card__cost-icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/price-6.png" alt=""><span class="price-card__cost-text">без перерасчёта
                                                    <span class="price-card__dropdown"><span class="price-card__dropdown-text">Тариф "без перерерасчёта" предлагает более низкую цену, но любые пропуски по любым причинам не возвращаются.</span></span>
                                                </span></span><span class="price-card__value">5 200<i class="fa fa-rub"></i></span>
                                                </li>
                                                <li class="price-card__cost-item"><span class="price-card__cost-label"><img class="price-card__cost-icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/price-6.png" alt=""><span class="price-card__cost-text">с перерасчётом
                                                    <span class="price-card__dropdown"><span class="price-card__dropdown-text">Тариф "с перерасчётом" включает в себя перенос пропущенных по болезни занятий. Действует только при предоставлении оригинала медицинской справки.    </span></span>
                                                </span></span><span class="price-card__value">6 000<i class="fa fa-rub"></i></span>
                                                </li>
                                            </ul>
                                            <button class="button price-card__button" href="#price" data-fancybox onclick="fill_price('Базовый')">Выбрать тариф</button>
                                        </div>
                                    </div>
                                </div>


                                <!-- 2 -->

                                <div class="price__column">
                                    <div class="price-card">
                                        <div class="price-card__gradient"></div>
                                        <div class="price-card__inner">
                                            <img class="price-card__inner-label" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/intro-label.png" width="181" height="177" alt="">
                                            <div class="price-card__header"><span class="price-card__title">Базовый<img class="price-card__title-icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/price-plus.png" alt=""></span>
                                                <ul class="price-card__age-list">
                                                    <li class="price-card__age-item price-card__age-item--purple"><span class="price-card__age-text">12-15 лет</span>
                                                    </li>
                                                </ul>
                                            </div>


                                            <div class="price-card__content">

                                                <ul class="price-card__list">
                                                    <li class="price-card__item"><img class="price-card__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/price-1.png" alt=""><span class="price-card__text">Актёрское мастерство</span>
                                                    </li>
                                                    <li class="price-card__item"><img class="price-card__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/price-2.png" alt=""><span class="price-card__text">Вокал</span>
                                                    </li>
                                                    <li class="price-card__item"><img class="price-card__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/price-3.png" alt=""><span class="price-card__text">Хореография</span>
                                                    </li>
                                                </ul>
                                                <ul class="price-card__sked-list">
                                                    <li class="price-card__sked-item"><img class="price-card__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/price-4.png" alt=""><span class="price-card__text">16 занятий (по 4 занятия в день)</span>
                                                    </li>
                                                    <li class="price-card__sked-item"><img class="price-card__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/price-5.png" alt=""><span class="price-card__text">1 раз в неделю</span>
                                                    </li>
                                                </ul>

                                            </div>


                                        </div>
                                        <div class="price-card__footer">
                                            <ul class="price-card__cost-list">
                                                <li class="price-card__cost-item"><span class="price-card__cost-label"><img class="price-card__cost-icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/price-6.png" alt=""><span class="price-card__cost-text">без перерасчёта
                                                    <span class="price-card__dropdown"><span class="price-card__dropdown-text">Тариф "без перерерасчёта" предлагает более низкую цену, но любые пропуски по любым причинам не возвращаются.</span></span>
                                                </span></span><span class="price-card__value">6 400<i class="fa fa-rub"></i></span>
                                                </li>


                                                <li class="price-card__cost-item">
                                                	<span class="price-card__cost-label">
                                                		<img class="price-card__cost-icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/price-6.png" alt="">
                                            			<span class="price-card__cost-text">с перерасчётом
	                                                    	<span class="price-card__dropdown">
	                                                    		<span class="price-card__dropdown-text">
	                                                    			Тариф "с перерасчётом" включает в себя перенос пропущенных по болезни занятий. Действует только при предоставлении оригинала медицинской справки. 
	                                                    		</span>
	                                                    	</span>
                                            			</span>
                                                	</span>
                                                	<span class="price-card__value">7 300<i class="fa fa-rub"></i></span>
                                                </li>


                                            </ul>
                                            <button class="button price-card__button" href="#price" data-fancybox onclick="fill_price('Базовый+')">Выбрать тариф</button>
                                        </div>
                                    </div>
                                </div>

                                <!-- 2 -->

                                <!-- 3 -->

                               
                                <div class="price__column">
                                    <div class="price-card">
                                        <div class="price-card__gradient"></div>
                                        <div class="price-card__inner">
                                            <img class="price-card__inner-label" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/intro-label.png" width="181" height="177" alt="">
                                            <div class="price-card__header"><span class="price-card__title">Интенсивный</span>
                                                <ul class="price-card__age-list">
                                                    <li class="price-card__age-item price-card__age-item--red"><span class="price-card__age-text">3-5 лет</span>
                                                    </li>
                                                    <li class="price-card__age-item price-card__age-item--orange"><span class="price-card__age-text">6-8 лет</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="price-card__content">
                                                <ul class="price-card__list">
                                                    <li class="price-card__item"><img class="price-card__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/price-1.png" alt=""><span class="price-card__text">Актёрское мастерство</span>
                                                    </li>
                                                    <li class="price-card__item"><img class="price-card__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/price-2.png" alt=""><span class="price-card__text">Вокал</span>
                                                    </li>
                                                    <li class="price-card__item"><img class="price-card__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/price-3.png" alt=""><span class="price-card__text">Хореография</span>
                                                    </li>
                                                </ul>
                                                <ul class="price-card__sked-list">
                                                    <li class="price-card__sked-item"><img class="price-card__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/price-4.png" alt=""><span class="price-card__text">24 занятий (по 3 занятия в день)</span>
                                                    </li>
                                                    <li class="price-card__sked-item"><img class="price-card__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/price-5.png" alt=""><span class="price-card__text">2 раза в неделю</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="price-card__footer">
                                            <ul class="price-card__cost-list">
                                                <li class="price-card__cost-item"><span class="price-card__cost-label"><img class="price-card__cost-icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/price-6.png" alt=""><span class="price-card__cost-text">без перерасчёта
                                                    <span class="price-card__dropdown"><span class="price-card__dropdown-text">Тариф "без перерерасчёта" предлагает более низкую цену, но любые пропуски по любым причинам не возвращаются.</span></span>
                                                </span></span><span class="price-card__value">8 800<i class="fa fa-rub"></i></span>
                                                </li>


                                                <li class="price-card__cost-item">
                                                	<span class="price-card__cost-label">
                                                		<img class="price-card__cost-icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/price-6.png" alt="">
                                            			<span class="price-card__cost-text">с перерасчётом
	                                                    	<span class="price-card__dropdown">
	                                                    		<span class="price-card__dropdown-text">
	                                                    			Тариф "с перерасчётом" включает в себя перенос пропущенных по болезни занятий. Действует только при предоставлении оригинала медицинской справки. 
	                                                    		</span>
	                                                    	</span>
                                            			</span>
                                                	</span>
                                                	<span class="price-card__value">10 000<i class="fa fa-rub"></i></span>
                                                </li>
                                            </ul>
                                            <button class="button price-card__button" href="#price" data-fancybox onclick="fill_price('Интенсивный')">Выбрать тариф</button>
                                        </div>
                                    </div>
                                </div>

                                <!-- 3 -->

                            </div>
                        </div>

                        <div class="price__footer">
                            <div class="why price__why">
                                <div class="why__header"><span class="why__title">Почему стоит выбрать Мастерскую Артист?</span>
                                </div>
                                <div class="why__content">
                                    <div class="why__grid">
                                        <div class="why__column">
                                            <div class="why-card">
                                                <img class="why-card__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/price/1.svg" alt=""><span class="why-card__title">Комплексное обучение </span>
                                                <p class="why-card__caption">Занятия, включающие в себя сразу 3 кружка для вашего ребёнка, а соответственно сокращающие время на дорогу и расходы.</p>
                                            </div>
                                        </div>
                                        <div class="why__column">
                                            <div class="why-card">
                                                <img class="why-card__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/price/2.svg" alt=""><span class="why-card__title">Личное портфолио</span>
                                                <p class="why-card__caption">Много фото и видео с занятий и студийных мероприятий, а также профессиональное базовое портфолио для всех учеников.</p>
                                            </div>
                                        </div>
                                        <div class="why__column">
                                            <div class="why-card">
                                                <img class="why-card__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/price/3.svg" alt=""><span class="why-card__title">Бесплатные занятия</span>
                                                <p class="why-card__caption">Пробное занятие наравне с учениками абсолютно бесплатно и ни к чему вас не обязывает.</p>
                                            </div>
                                        </div>
                                        <div class="why__column">
                                            <div class="why-card">
                                                <img class="why-card__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/price/4.svg" alt=""><span class="why-card__title">Насыщенная программа</span>
                                                <p class="why-card__caption">150 минут интересных занятий в каждый из учебных дней.</p>
                                            </div>
                                        </div>
                                        <div class="why__column">
                                            <div class="why-card">
                                                <img class="why-card__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/price/5.svg" alt=""><span class="why-card__title">Звездные гости</span>
                                                <p class="why-card__caption">Мастер-классы и творческие встречи с актёрами театра и кино.</p>
                                            </div>
                                        </div>
                                        <div class="why__column">
                                            <div class="why-card">
                                                <img class="why-card__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/price/6.svg" alt=""><span class="why-card__title">Гибкая система оплаты</span>
                                                <p class="why-card__caption">Вы выбираете график обучения и нужны ли вам перерасчёты по болезни.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script>
  function fill_price(value) {

      $("#price-form input[name = user_rate]").val(value);

  }

  function FormPriceClick(path) {
      var name = $("#price-form input[name=user_rate_name]").val();
      var number = $("#price-form input[name=user_rate_number]").val();
      var tarif = $("#price-form input[name = user_rate]").val();
      var target = 'Форма Выбора Тарифа (' +  tarif + ')';



      $.ajax({
          url: path + '/functions/form/formNumberNameHandler.php',
          method: 'get',


          data: {
              name: name,
              number: number,
              target: target
          },
          success: function (response) {
              //  alert(response);
              //$('#submit-ajax').html(response);
          }
      });
  }

  function FormPriceClickCallBack( path  ) {

      var valid = $("#price-form input");
      for(var i = 0;i<valid.length;i++)
      {
          if(!valid[i].checkValidity())
          {
              valid[i].closest('div').classList.add('invalid');
              return false;
          }
          else
          {
              valid[i].closest('div').classList.remove('invalid');
          }
      }

      FormPriceClick(path);
      $("#price-form")[0].reset();
      $.fancybox.close();
      return false;
  }


</script>


 <?php get_footer();