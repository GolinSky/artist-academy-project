<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<title><?php echo get_bloginfo('name');?> </title>

<link rel="icon" type="image/x-icon" href="<?php echo get_stylesheet_directory_uri()?>/favicon.ico">
<!--[if (gt IE 9)|!(IE)]><!-->
<link href="<?php echo get_stylesheet_directory_uri()?>/static/css/main.min.css" rel="stylesheet" type="text/css">
<!--<![endif]-->
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<center>	<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php echo 'Ой! Страница не найдена.'; ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
                    <h3 class="page-title"><?php echo 'По этому адресу ничего не найдено.'; ?></h3>

                    <p> <a class="button subs__send" href="<?php echo get_home_url();?>"> <?php echo 'Главная'; ?></a></p>

				</div><!-- .page-content -->
			</section></center><!-- .error-404 -->

		</main><!-- .site-main -->



	</div><!-- .content-area -->

<?php get_footer(); ?>
