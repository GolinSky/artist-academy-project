<?php

global $root_url_path;

get_header(); ?>


    <div class="main page__main">
        <div class="main__header">
            <div class="main__container container">
                <h3 class="main__title">Отзывы</h3>
                <ul class="breadcrumbs main__breadcrumbs">
                    <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/">Главная</a>
                    </li>
                    <li class="breadcrumbs__item"><a class="breadcrumbs__link" >Отзывы</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main__content">
            <div class="reviews main__reviews">
                <div class="reviews__container container">
                    <div class="reviews__body">
                        <div class="reviews__content">
                        <style>
                            span.test_kostil
                            {
                                color: #8f8da6;
                            }
                        </style>
                            <span class="test_kostil" >На этой странице Вы можете увидеть впечатления родителей о нашей Мастерской.<br>
                                Если Вы хотите оставить отзыв о нас, можете сделать это прямо сейчас!</span>
                            <a class="button button-secondary reviews__button"
                               href="#add-reviews" data-fancybox="">
                                <img class="button__icon" src="<?php echo $root_url_path ?>/static/img/pencil.svg"
                                     alt=""><span class="button__text">Оставить отзыв</span>
                            </a>
                        </div>
                        <div class="reviews__grid">
                            <?php $counter = 0;
                            $close_tag_index = 3;
                            $reviews = new WP_Query(
                                array(
                                    'post_type' => 'review',
                                    'posts_per_page' => -1,
                                    'post_status' => 'publish',
                                    'order' => 'ASC',
                                    'orderby' => 'title'
                                )); ?>
                            <?php if ($reviews->have_posts()) : while ($reviews->have_posts()) : $reviews->the_post(); ?>
                                <?php
                                $myExcerpt = get_the_content();
                                $tags = array("<p>", "</p>");
                                $myExcerpt = str_replace($tags, "", $myExcerpt);
                                ?>
                                <?php $echo_column_div = $counter == 0 || $counter % 4 == 0;
                                if ($echo_column_div) {
                                    $close_tag_index = $counter + 3;
                                    echo '<div class="reviews__column">';
                                } ?>

                                <div class="reviews-card">
                                    <div class="reviews-card__photo">
                                        <img class="reviews-card__img" src="<?php the_post_thumbnail_url('full'); ?>"
                                             alt="">
                                    </div>
                                    <div class="reviews-card__content"><span
                                                class="reviews-card__date"><?php echo get_the_date('j F Y'); ?></span>
                                        <h4 class="reviews-card__author"><?php the_title(); ?></h4>
                                        <p class="reviews-card__caption"> <?php echo $myExcerpt; ?></p>
                                    </div>
                                </div>

                                <?php if ($close_tag_index == $counter) {
                                    echo '</div>';
                                }
                                $counter++; ?>

                            <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php get_footer();