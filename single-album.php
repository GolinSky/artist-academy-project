<?php
    /**
     * The template for displaying all single posts and attachments
     *

     */
global $root_url_path;

    get_header();


    // Start the loop.
    while ( have_posts() ) :
        the_post();

        // Include the single post content template.
        get_template_part( 'template-parts/album-detail', 'album-detail' );

        // End of the loop.
    endwhile;


get_footer();