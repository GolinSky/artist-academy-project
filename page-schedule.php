<?php
global $root_url_path;
 get_header(); ?>

    <div class="main">
        <div class="main__header">
            <div class="main__container container">
                <h3 class="main__title">Расписание </h3>
                <ul class="breadcrumbs main__breadcrumbs">
                    <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/">Главная</a>
                    </li>
                    <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Расписание</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main__content">
            <div class="leasons">
                <div class="leasons__container container">
                    <div class="filter leasons__filter">
                        <div class="filter__body">
                            <div class="filter__column">
                                <div class="filter__row">
                                    <form class="filter__form" action="#"><span class="filter__title">Дни недели:</span>
                                        <ul class="filter__list filter__list--days">
                                            <li class="filter__item">
                                                <label class="filter__label">
                                                    <input class="filter__checkbox" type="checkbox" onclick="sort_by_day('Пн')"><span class="filter__text">Пн</span>
                                                </label>
                                            </li>
                                            <li class="filter__item">
                                                <label class="filter__label">
                                                    <input class="filter__checkbox" type="checkbox" onclick="sort_by_day('Вт')"><span class="filter__text">Вт</span>
                                                </label>
                                            </li>
                                            <li class="filter__item">
                                                <label class="filter__label">
                                                    <input class="filter__checkbox" type="checkbox" onclick="sort_by_day('Ср')"><span class="filter__text">Ср</span>
                                                </label>
                                            </li>
                                            <li class="filter__item">
                                                <label class="filter__label">
                                                    <input class="filter__checkbox" type="checkbox" onclick="sort_by_day('Чт')"><span class="filter__text">Чт</span>
                                                </label>
                                            </li>
                                            <li class="filter__item">
                                                <label class="filter__label">
                                                    <input class="filter__checkbox" type="checkbox" onclick="sort_by_day('Пт')"><span class="filter__text">Пт</span>
                                                </label>
                                            </li>
                                            <li class="filter__item">
                                                <label class="filter__label">
                                                    <input class="filter__checkbox" type="checkbox" onclick="sort_by_day('Сб')"><span class="filter__text">Сб</span>
                                                </label>
                                            </li>
                                            <li class="filter__item filter__item--disabled">
                                                <label class="filter__label">
                                                    <input class="filter__checkbox" type="checkbox" onclick="sort_by_day('Вс')"><span class="filter__text">Вс</span>
                                                </label>
                                            </li>
                                        </ul>
                                    </form>
                                </div>
                            </div>
                            <div class="filter__column">
                                <div class="filter__row">
                                    <form class="filter__form" action="#"><span class="filter__title">Возрастная категория детей: </span>
                                        <ul class="filter__list filter__list--age">
                                            <?php


                                            $age_items = array(
                                                '3-5 лет',
                                                '6-8 лет',
                                                '9-12 лет',
                                                '13-15 лет',
                                                '16+ лет'
                                            );

                                            foreach($age_items as $items)
                                            {
                                                echo '<li class="filter__item">
                                                <label class="filter__label">
                                                    <input class="filter__checkbox" type="checkbox" onclick="sort_by_sequence(\''. $items .'\')"><span class="filter__text"> ' .$items. '</span>
                                                </label>
                                            </li>';
                                            }
                                            ?>

                                        </ul>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="leasons__body">
                        <div class="leasons__data">
                            <div class="leasons__top">
                                <div class="leasons__row">
                                    <div class="leasons__grid">
                                        <div class="leasons__column"><span class="leasons__heading">Наименование группы</span>
                                        </div>
                                        <div class="leasons__column"><span class="leasons__heading">Возраст</span>
                                        </div>
                                        <div class="leasons__column"><span class="leasons__heading">Дни недели</span>
                                        </div>
                                        <div class="leasons__column"><span class="leasons__heading">Цена</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="leasons__content scrollbar-inner js-leasons-scrollbar" id="load_container">

                        <?php get_groups();?>
                            <script>

                                function sort_by_day(value) {
                                    var day_query = value;
                                    jQuery.ajax({
                                        async:true,
                                        url: '<?php echo admin_url( 'admin-ajax.php' );?>',
                                        data: {
                                            'action' : 'group_action',
                                            'day_query': day_query
                                        },
                                        type: 'POST',
                                        success: function(data) {
                                            jQuery("#load_container").html(data);
                                        }
                                    });
                                }
                                function sort_by_sequence(value) {
                                    console.log(value);
                                    var sequence_query = value;
                                    jQuery.ajax({
                                        async:true,
                                        url: '<?php echo admin_url( 'admin-ajax.php' );?>',
                                        data: {
                                            'action' : 'group_action',
                                            'sequence_query': sequence_query
                                        },
                                        type: 'POST',
                                        success: function(data) {
                                            jQuery("#load_container").html(data);
                                        }
                                    });
                                }

                            </script>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php get_footer();