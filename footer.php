<?php global $root_url_path; ?>

<?php global $url;
$url="https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>

<div class="page__footer">
    <footer class="footer">
        <div class="footer__container container">
            <div class="footer__top">
                <div class="subs footer__subs">
                    <div class="subs__logo">
                        <img class="subs__logo-img" src="<?php echo $root_url_path?>/static/img/general/icons/logo-small.svg" alt="">
                    </div>
                    <div class="subs__data">
                        <h3 class="subs__title">НОВОСТИ И СПЕЦИАЛЬНЫЕ ПРЕДЛОЖЕНИЯ</h3>
                        <form class="subs__form" id = "fourth-form">
                            <div class="subs__field">
                                <img class="subs__icon" src="<?php echo $root_url_path?>/static/img/general/icons/envelope-orange.png" alt="">

                                <input class="subs__input" type="email" placeholder="Введите адрес электронной почты" name="user_email" required>
                                <button class="button subs__send" onclick="return Form4ClickCallBack()" type = "submit">Подписаться</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="footer__content">
                <div class="footer__grid">
                    <div class="footer__column"><span class="footer__title">Направления</span>
                        <ul class="footer__list">
                            <li class="footer__item"><a class="footer__link" href="#dev" data-fancybox>Актёрское мастерство</a>
                            </li>
                            <li class="footer__item"><a class="footer__link" href="<?php get_page_url_in_menu('choreography', $url); ?>" >Хореография</a>
                            </li>
                            <li class="footer__item"><a class="footer__link" href="#dev" data-fancybox>Вокал</a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer__column"><span class="footer__title">О нас</span>
                        <ul class="footer__list">
                            <li class="footer__item"><a class="footer__link" href="<?php get_student_url_in_menu($url); ?>">Ученики</a>
                            </li>
                            <li class="footer__item"><a class="footer__link" href="#dev" data-fancybox>Наставники</a>
                            </li>
                            <li class="footer__item"><a class="footer__link" href="<?php get_page_url_in_menu('feedback', $url); ?>" >Отзывы</a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer__column"><span class="footer__title">Информация</span>
                        <ul class="footer__list">
                            <li class="footer__item"><a class="footer__link" href="#dev" data-fancybox>Расписание</a>
                            </li>
                            <li class="footer__item"><a class="footer__link" href="#dev" data-fancybox>Новости</a>
                            </li>
                            <li class="footer__item"><a class="footer__link" href="<?php get_page_url_in_menu('contacts', $url); ?>">Контакты</a>
                            </li>
                            </li>
                            <li class="footer__item"><a class="footer__link" href="<?php get_page_url_in_menu('site-rules', $url); ?>" >Правила сайта</a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer__column"><span class="footer__title">Прайс-лист</span>
                        <ul class="footer__list">
                            <li class="footer__item"><a class="footer__link" href="<?php get_page_url_in_menu('price', $url); ?>" >Цены</a>
                            </li>
                            <li class="footer__item"><a class="footer__link" href="#dev" data-fancybox>О поступлении</a>
                            </li>
                            <li class="footer__item"><a class="footer__link" href="#dev" data-fancybox>Акции</a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer__column"><span class="footer__title">Контакты и социальные сети</span>
                        <div class="footer__inner">
                            <ul class="info-box footer__info-box">
                                <li class="info-box__item">
                                    <a href="tel:+7 (910) 469-12-91" class="info-box__icon"><i class="fa fa-phone"></i></a>
                                    <div class="info-box__data"><span class="info-box__label">Наш телефон:</span><a class="info-box__link" href="tel:+79104691291">+7 (910) 469-12-91</a>
                                    </div>
                                </li>
                                <li class="info-box__item">
                                    <a href="mailto:info@artist-m.ru" class="info-box__icon"><i class="fa fa-envelope"></i>
                                    </a>
                                    <div class="info-box__data"><span class="info-box__label">Нашa почта:</span><a class="info-box__link" href="mailto:info@artist-m.ru">info@artist-m.ru</a>
                                    </div>
                                </li>
                            </ul>
                            <ul class="socials footer__socials">
                                <li class="socials__item">
                                    <a class="socials__link" href="https://vk.com/artist_m" target="_blank">
                                        <img class="socials__icon" src="<?php echo $root_url_path?>/static/img/general/icons/vk.svg" alt="">
                                    </a>
                                </li>
                                <li class="socials__item">
                                    <a class="socials__link" href="https://www.instagram.com/mast_artist/" target = "_blank">
                                        <img class="socials__icon" src="<?php echo $root_url_path?>/static/img/general/icons/inst.svg" alt="">
                                    </a>
                                </li>
                                <li class="socials__item">
                                    <a class="socials__link" href="https://www.facebook.com/mast.artist/" target = "_blank">
                                        <img class="socials__icon" src="<?php echo $root_url_path?>/static/img/general/icons/fb.svg" alt="">
                                    </a>
                                </li>
                                <li class="socials__item">
                                    <a class="socials__link" href="https://ok.ru/artist.m" target = "_blank">
                                        <img class="socials__icon" src="<?php echo $root_url_path?>/static/img/general/icons/ok.svg" alt="">
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>


            <div class="copyright footer__copyright">
                <img class="copyright__img" src="<?php echo $root_url_path?>/static/img/general/smm-lg.svg" alt=""><span class="copyright__text">© 2019,  Все права защищены</span>
                <a href="https://www.instagram.com/explore/tags/%D0%BC%D0%B0%D1%81%D1%82%D0%B5%D1%80%D1%81%D0%BA%D0%B0%D1%8F%D0%B0%D1%80%D1%82%D0%B8%D1%81%D1%82/" target="_blank" class="copyright__hashtag"><span class="copyright__hashtag-label">#</span><span class="copyright__hashtag-text">мастерскаяартист</span>
                </a>
            </div>
        </div>
    </footer>
    <div class="modals page__modals">
        <div class="modal modal--login" id="login">
            <form class="form modal__form">
                <div class="form__header"><span class="form__title">Добро пожаловать!</span>
                </div>
                <div class="form__content">
                    <div class="form__field">
                        <div class="form__icon">
                            <img class="form__img" src="<?php echo $root_url_path?>/static/img/general/icons/envelope-wh.svg" alt="">
                        </div>
                        <input class="form__input" type="email" placeholder="Введитrequiredе почту">
                    </div>
                    <div class="form__field">
                        <div class="form__icon">
                            <img class="form__img" src="<?php echo $root_url_path?>/static/img/general/icons/lock-full.svg" alt="">
                        </div>
                        <input class="form__input" id="password" type="password" placeholder="Введитеrequired пароль">
                        <button class="form__show-pass"></button>
                    </div>
                </div>
                <div class="form__footer">
                    <button class="form__send">
                        <img class="form__send-icon form__send-icon--left" src="<?php echo $root_url_path?>/static/img/general/icons/lock.svg" alt=""><span class="form__send-text">Отправить заявку</span>
                    </button><a class="button button-default form__reg" href="#reg" data-fancybox>Зарегистрироваться</a>
                    <a class="form__restore" href="#restore" data-fancybox>
                        <img class="form__restore-img" src="<?php echo $root_url_path?>/static/img/general/icons/info.png" alt=""><span class="form__restore-text">Забыли пароль?</span>
                    </a>
                </div>
            </form>
        </div>

        <div class="modal modal--reg" id="reg">
            <form class="form modal__form">
                <div class="form__header"><span class="form__title">Регистрация</span>
                </div>
                <div class="form__content">
                    <div class="form__field">
                        <div class="form__icon">
                            <img class="form__img" src="<?php echo $root_url_path?>/static/img/general/icons/lock-bg.png" alt="">
                        </div>
                        <input class="form__input" type="email" placeholder="Введите пин-код requiredученика">
                    </div>
                </div>
                <div class="form__footer"><a class="form__send" href="#reg2" data-fancybox><span class="form__send-text">Продолжить регистрацию</span></a>
                </div>
            </form>
        </div>
        <div class="modal modal--reg-second" id="reg2">
            <form class="form modal__form">
                <div class="form__header"><span class="form__label">Здравствуйте,</span><span class="form__title form__title--small">Виктория кожевникова</span>
                </div>
                <div class="form__content">
                    <div class="form__field">
                        <div class="form__icon">
                            <img class="form__img" src="<?php echo $root_url_path?>/static/img/general/icons/envelope-wh.svg" alt="">
                        </div>
                        <input class="form__input" type="email" placeholder="Введитrequiredе почту">
                    </div>
                    <div class="form__field">
                        <div class="form__icon">
                            <img class="form__img" src="<?php echo $root_url_path?>/static/img/general/icons/lock-full.svg" alt="">
                        </div>
                        <input class="form__input" type="password" placeholder="Введитеrequired пароль">
                    </div>
                    <div class="form__field">
                        <div class="form__icon">
                            <img class="form__img" src="<?php echo $root_url_path?>/static/img/general/icons/lock-full.svg" alt="">
                        </div>
                        <input class="form__input" type="password" placeholder="Подтвердитеrequired пароль">
                    </div>
                </div>
                <div class="form__footer">
                    <button class="form__send"><span class="form__send-text">Зарегистрироваться</span>
                    </button>
                </div>
            </form>
        </div>
        <div class="modal modal--restore" id="restore">
            <form class="form modal__form">
                <div class="form__header"><span class="form__label">Мы вышлем письмо вам на почту!</span><span class="form__title form__title--small">восстановление пароля</span>
                </div>
                <div class="form__content">
                    <div class="form__field">
                        <div class="form__icon">
                            <img class="form__img" src="<?php echo $root_url_path?>/static/img/general/icons/envelope-wh.svg" alt="">
                        </div>
                        <input class="form__input" type="email" placeholder="Введитrequiredе почту">
                    </div>
                </div>
                <div class="form__footer"><a class="button button-secondary form__reg" href="#restore-second" data-fancybox>Восстановить пароль</a>
                </div>
            </form>
        </div>
        <div class="modal modal--restore-second" id="restore-second">
            <form class="form modal__form">
                <div class="form__header"><span class="form__label">Введите новый пароль и запомните его :)</span><span class="form__title form__title--small">восстановление пароля</span>
                </div>
                <div class="form__content">
                    <div class="form__field">
                        <div class="form__icon">
                            <img class="form__img" src="<?php echo $root_url_path?>/static/img/general/icons/lock-full.svg" alt="">
                        </div>
                        <input class="form__input" type="password" placeholder="Введите новыйrequired пароль">
                    </div>
                    <div class="form__field">
                        <div class="form__icon">
                            <img class="form__img" src="<?php echo $root_url_path?>/static/img/general/icons/lock-full.svg" alt="">
                        </div>
                        <input class="form__input" type="password" placeholder="Подтвердитеrequired пароль">
                    </div>
                </div>
                <div class="form__footer">
                    <button class="button button-secondary form__reg">Обновить пароль</button>
                </div>
            </form>
        </div>
        <div class="modal modal--add-reviews" id="add-reviews">
            <form class="form modal__form" id="third_form" enctype="multipart/form-data" method="post"  action="<?php echo $root_url_path?>/functions/form/formFileHandler.php" target="_blank">
                <div class="form__header"><span class="form__title">форма отзыва</span>
                </div>
                <div class="form__content">
                    <div class="form__field">
                        <div class="form__icon">
                            <img class="form__img" src="<?php echo $root_url_path?>/static/img/general/icons/user.svg" alt="">
                        </div>
                        <input class="form__input" type="text" placeholder="Имя и фамилия" name = "name_requiredsurname" required>

                        <div class="form__field-label">*</div>
                    </div>
                    <div class="form__field">
                        <div class="form__icon">
                            <img class="form__img" src="<?php echo $root_url_path?>/static/img/general/icons/envelope-wh.svg" alt="">
                        </div>
                        <input class="form__input" type="email" placeholder="Введите почту" name="userequiredr_email" required>
                        <div class="form__field-label">*</div>
                    </div>
                    <div class="form__field">
                        <div class="form__icon">
                            <img class="form__img" src="<?php echo $root_url_path?>/static/img/general/icons/gallery.svg" alt="">
                        </div>
                        <input class="form__input" type="file" placeholder="Выберите фотографию" name = "fileToUpload">
                        <img class="form__field-img" src="<?php echo $root_url_path?>/static/img/general/icons/file.svg" alt="">
                    </div>
                    <div class="form__field">
                        <textarea class="form__textarea"  placeholder="Ваше сообщение" name ="message"></textarea>
                    </div>
                </div>
                <div class="form__footer">
                    <button class="button button-secondary form__reg" id="form3-button" onclick="return Form3ClickCallBack();" type = "submit"  > Отправить отзыв</button>
                </div>
            </form>
        </div>
        <div class="modal modal--order" id="order">
            <div class="modal__inner">
                <div class="modal__swiper">
                    <div class="modal__swiper-container swiper-container js-order-swiper">
                        <div class="modal__swiper-wrapper swiper-wrapper">
                            <div class="modal__swiper-slide swiper-slide">
                                <div class="modal-card">
                                    <div class="modal-card__photo">
                                        <img class="modal-card__img" src="<?php echo $root_url_path?>/static/img/general/g8.png" alt="">
                                    </div>
                                    <div class="modal-card__data">
                                        <img class="modal-card__data-img" src="<?php echo $root_url_path?>/static/img/general/logo-purple.svg" alt=""><span class="modal-card__data-title">МАСТЕРСКАЯ АРТИСТ</span>
                                    </div>
                                </div>
                            </div>
                            <div class="modal__swiper-slide swiper-slide">
                                <div class="modal-card">
                                    <div class="modal-card__photo">
                                        <img class="modal-card__img" src="<?php echo $root_url_path?>/static/img/general/g6.png" alt="">
                                    </div>
                                    <div class="modal-card__data">
                                        <img class="modal-card__data-img" src="<?php echo $root_url_path?>/static/img/general/logo-purple.svg" alt=""><span class="modal-card__data-title">МАСТЕРСКАЯ АРТИСТ</span>
                                    </div>
                                </div>
                            </div>
                            <div class="modal__swiper-slide swiper-slide">
                                <div class="modal-card">
                                    <div class="modal-card__photo">
                                        <img class="modal-card__img" src="<?php echo $root_url_path?>/static/img/general/g7.png" alt="">
                                    </div>
                                    <div class="modal-card__data">
                                        <img class="modal-card__data-img" src="<?php echo $root_url_path?>/static/img/general/logo-purple.svg" alt=""><span class="modal-card__data-title"> МАСТЕРСКАЯ АРТИСТ</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-controls modal__swiper-controls">
                        <div class="swiper-controls__pagination"></div>
                    </div>
                </div>
                <form class="form modal__form" id = "first-form" >
                    <div class="form__header"><span class="form__title">Форма заявки</span>
                    </div>
                    <div class="form__content">
                        <div class="form__field">
                            <div class="form__icon">
                                <img class="form__img" src="<?php echo $root_url_path?>/static/img/general/icons/user.svg" alt="">
                            </div>
                            <input class="form__input" type="text" placeholder="Ваше имя" name="applicant_nrequiredame" required>
                        </div>
                        <div class="form__field">
                            <div class="form__icon">
                                <img class="form__img" src="<?php echo $root_url_path?>/static/img/general/icons/mobile.svg" alt="">
                            </div>
                            <input class="form__input" type="tel" placeholder="Номер телефона" name="applicant_numrequiredber" required>
                        </div>
                        <div class="rebate form__rebate">
                            <div class="rebate__inner">
                                <div class="rebate__left"><span class="rebate__title">Скидка 10%</span>
                                    <h3 class="rebate__caption">для второго ребёнка </h3>
                                </div>
                                <div class="rebate__right">
                                    <img class="rebate__img" src="<?php echo $root_url_path?>/static/img/general/rebate-2.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form__footer" id="submit-ajax">
                        <button  type="submit" class="form__send"   type = "submit"  href="javascript:void(0);" onclick="return Form1ClickCallBack('<?php echo $root_url_path?>');">
                            <span class="form__send-text" >Отправить заявку</span>
                            <img class="form__send-icon" src="<?php echo $root_url_path?>/static/img/general/icons/plane.svg" alt="">
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <div class="modal modal--dev" id="dev">
            <form class="form modal__form">
                <div class="form__header"><span class="form__title">пока в разработке :)</span>
                </div>
                <img class="form__img" src="<?php echo $root_url_path?>/static/img/general/odr.gif" alt="">
            </form>
        </div>

        <div class="modal modal--price" id="price">
            <div class="modal__inner">

                <form class="form modal__form" id = "price-form">
                    <div class="form__header"><span class="form__title">Выбор Тарифа</span>
                    </div>
                    <div class="form__content">
                        <div class="form__field">
                            <div class="form__icon">
                                <img class="form__img" src="<?php echo $root_url_path?>/static/img/general/icons/user.svg" alt="">
                            </div>
                            <input class="form__input" type="text" placeholder="Ваше имя" name="user_rate_name" required>
                        </div>
                        <div class="form__field">
                            <div class="form__icon">
                                <img class="form__img" src="<?php echo $root_url_path?>/static/img/general/icons/mobile.svg" alt="">
                            </div>
                            <input class="form__input" type="tel" placeholder="Номер телефона" name="user_rate_number" required>
                        </div>
                        <div class="form__field">

                            <div class="form__icon">
                                <img class="form__img" src="<?php echo $root_url_path?>/static/img/general/icons/star.svg" alt="">
                            </div>
                            <input class="form__input" type="tel" placeholder="Выбранный тариф" name="user_rate" disabled>
                        </div>

                    </div>
                    <div class="form__footer" id="submit-ajax">
                        <button  type="submit" class="form__send" id ="target-submit-button1" href="javascript:void(0);" onclick="return FormPriceClickCallBack('<?php echo $root_url_path?>');">
                            <span class="form__send-text" >Отправить заявку</span>
                            <img class="form__send-icon" src="<?php echo $root_url_path?>/static/img/general/icons/plane.svg" alt="">
                        </button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
<script src="<?php echo $root_url_path?>/static/js/main.min.js"></script>
<script src="<?php echo $root_url_path?>/static/js/separate-js/common.js"></script>
<script type="text/javascript" src="<?php echo $root_url_path?>/functions/js/form/form1.js"></script>

</body>
</html>