
<?php global $root_url_path; ?>

<?php get_header();?>
<div class="page__intro">
    <div class="intro">
        <div class="intro__sliderContainer sliderContainer">
            <div class="intro__slider slider single-item">
                <div class="intro__slide" style="background: url(<?php echo $root_url_path;?>/static/img/general/bg-intro.png) no-repeat; background-size: cover;">
                    <div class="intro__container container">
                        <img class="intro__tag" src="<?php echo $root_url_path;?>/static/img/general/tag.png" alt="">
                        <h1 class="intro__title"><?php Output('index_direction1'); ?></h1>
                        <p class="intro__caption"><?php Output('index_direction_desc1'); ?></p>
                        <img class="intro__label" src="<?php echo $root_url_path;?>/static/img/general/intro-label.png" alt="">
                       <!-- <a class="intro__play" href="#" data-fancybox>
                            <div class="intro__play-icon">
                                <img class="intro__play-img" src="<?php /*echo $root_url_path;*/?>/static/img/general/icons/play.svg" alt="">
                            </div><span class="intro__play-text">Посмотреть ролик</span>
                        </a>-->
                    </div>
                </div>
                <div class="intro__slide" style="background: url(<?php echo $root_url_path;?>/static/img/general/bg-intro.png) no-repeat; background-size: cover;">
                    <div class="intro__container container">
                        <img class="intro__tag" src="<?php echo $root_url_path;?>/static/img/general/tag.png" alt="">
                        <h1 class="intro__title"><?php Output('index_direction2'); ?></h1>
                        <p class="intro__caption"><?php Output('index_direction_desc2'); ?></p>
                        <img class="intro__label" src="<?php echo $root_url_path;?>/static/img/general/intro-label.png" alt="">
                        <!--<a class="intro__play" href="#" data-fancybox>
                            <div class="intro__play-icon">
                                <img class="intro__play-img" src="<?php /*echo $root_url_path;*/?>/static/img/general/icons/play.svg" alt="">
                            </div><span class="intro__play-text">Посмотреть ролик</span>
                        </a>-->
                    </div>
                </div>
                <div class="intro__slide" style="background: url(<?php echo $root_url_path;?>/static/img/general/bg-intro.png) no-repeat; background-size: cover;">
                    <div class="intro__container container">
                        <img class="intro__tag" src="<?php echo $root_url_path;?>/static/img/general/tag.png" alt="">
                        <h1 class="intro__title"><?php Output('index_direction3'); ?></h1>
                        <p class="intro__caption"><?php Output('index_direction_desc3'); ?></p>
                        <img class="intro__label" src="<?php echo $root_url_path;?>/static/img/general/intro-label.png" alt="">
            <!--            <a class="intro__play" href="#" data-fancybox>
                            <div class="intro__play-icon">
                                <img class="intro__play-img" src="<?php /*echo $root_url_path;*/?>/static/img/general/icons/play.svg" alt="">
                            </div><span class="intro__play-text">Посмотреть ролик</span>
                        </a>-->
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="progressBarContainer">
                    <div class="progressBarContainer__inner" data-slick-index="0">
                        <div class="intro-card">
                            <div class="intro-card__icon">
                                <img class="intro-card__img" src="<?php echo $root_url_path;?>/static/img/general/intro-card-1.png" alt="">
                            </div>
                            <div class="intro-card__data">
                                <h4 class="intro-card__title"><?php Output('index_direction1'); ?></h4>
                                <p class="intro-card__caption"><?php Output('index_bg_direction_desc1'); ?> </p><span class="progressBar"></span>
                            </div>
                        </div>
                    </div>
                    <div class="progressBarContainer__inner" data-slick-index="1">
                        <div class="intro-card">
                            <div class="intro-card__icon">
                                <img class="intro-card__img" src="<?php echo $root_url_path;?>/static/img/general/intro-card-2.png" alt="">
                            </div>
                            <div class="intro-card__data">
                                <h4 class="intro-card__title"><?php Output('index_direction2'); ?></h4>
                                <p class="intro-card__caption"><?php Output('index_bg_direction_desc2'); ?></p><span class="progressBar"></span>
                            </div>
                        </div>
                    </div>
                    <div class="progressBarContainer__inner" data-slick-index="2">
                        <div class="intro-card">
                            <div class="intro-card__icon">
                                <img class="intro-card__img" src="<?php echo $root_url_path;?>/static/img/general/intro-card-3.png" alt="">
                            </div>
                            <div class="intro-card__data">
                                <h4 class="intro-card__title"><?php Output('index_direction3'); ?></h4>
                                <p class="intro-card__caption"><?php Output('index_bg_direction_desc3'); ?></p><span class="progressBar"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page__informer">
    <div class="informer">
        <div class="informer__container container">
            <div class="informer__swiper">
                <div class="informer__swiper-container swiper-container js-informer-swiper">
                    <div class="informer__swiper-wrapper swiper-wrapper">




                        <?php $reviews = new WP_Query(array('post_type' => 'news', 'posts_per_page' => 3));  ?>

                        <?php if ( $reviews->have_posts() ) : while ( $reviews->have_posts() ) : $reviews->the_post(); ?>


                            <div class="informer__swiper-slide swiper-slide" style="background: url(<?php   the_post_thumbnail_url(array(924, 347));  ?>) no-repeat; background-size: cover;">

                                <div class="informer-card"><span class="informer-card__heading"><?php the_title();?></span><?php the_content();?>
                                </div>
                            </div>

                        <?php endwhile; ?>
                        <?php endif; ?>



                    </div>
                </div>
                <div class="swiper-controls informer__swiper-controls">
                    <div class="swiper-controls__pagination"></div>
                </div>
            </div>




            <div class="informer__data">
                <ul class="informer__list">

                    <li class="informer__item">
                        <a class="informer__link" href="#dev" data-fancybox style="background: url(<?php echo $root_url_path;?>/static/img/2.png) no-repeat; background-size: cover;">
                            <span class="informer__title"> Индивидуальные занятия</span>
                            <span class="informer__caption">Персональные уроки по вокалу или актёрскому мастерству</span>
                        </a>
                    </li>

                    <li class="informer__item">
                        <a class="informer__link" href="#dev" data-fancybox style="background: url(<?php echo $root_url_path;?>/static/img/1.png) no-repeat; background-size: cover;">
                            <span class="informer__title">Портфолио</span>
                            <span class="informer__caption">Бесплатное портфолио для каждого ученика</span>
                        </a>
                    </li>


                    <!-- <li class="informer__item"><a class="informer__link" href="#" style="background: url(<?php echo $root_url_path;?>/static/img/1.png) no-repeat; background-size: cover;"><span class="informer__title"></span></a>
                    </li>
                    <li class="informer__item"><a class="informer__link" href="#" style="background: url(<?php echo $root_url_path;?>/static/img/2.png) no-repeat; background-size: cover;"><span class="informer__title"></span></a> -->
                    <!-- <li class="informer__item"><a class="informer__link" href="#" style="background: url(<?php echo $root_url_path;?>/static/img/Group_30.2.png) no-repeat; background-size: cover;"><span class="informer__title"></span></a>
                    </li> -->
                </ul>
            </div>


        </div>
    </div>
</div>


<div class="page__pupils">
    <div class="pupils">
        <div class="pupils__container container">
            <div class="pupils__header"><span class="pupils__heading">Кто уже с нами?</span>
            </div>
            <div class="pupils__content">
                <h2 class="pupils__title">ученики</h2>
                <ul class="pupils__actions">
                    <li class="pupils__item">
                        <a class="button button-orange pupils__button" href="#order" data-fancybox>
                            <img class="button__icon" src="<?php echo $root_url_path;?>/static/img/general/icons/pencil.svg" alt=""><span class="button__text">Оставить заявку</span>
                        </a>
                    </li>

                </ul>
            </div>
            <div class="pupils__swiper">
                <div class="pupils__swiper-container swiper-container js-pupils-swiper">
                    <div class="pupils__swiper-wrapper swiper-wrapper">


                        <?php $reviews = new WP_Query(
                                array(
                                    'post_type' => 'students',
                                    'post_status' => 'publish',
                                    'posts_per_page' => (int) OutputInt('index_student_amount'),
                                    'meta_key' => 'student_age',
                                    'orderby' => 'student_age',
                                    'order' => 'ASC'
                                ));  ?>

                        <?php if ( $reviews->have_posts() ) : while ( $reviews->have_posts() ) : $reviews->the_post(); ?>
                            <div class="pupils__swiper-slide swiper-slide">
                                <a href="<?php the_permalink();  ?>" class="students-card">
                                    <div class="students-card__overlay"></div>
                                    <div class="students-card__header">
                                            <span class="students-card__link" href="<?php the_permalink();  ?>">
                                                <h4 class="students-card__title"><?php the_title();  ?>  </h4>
                                            </span>
                                        <span class="students-card__age"><?php the_excerpt(); ?> </span>
                                    </div>
                                    <div class="students-card__content">
                                        <img class="students-card__img" src="<?php the_post_thumbnail_url('full');  ?>" alt="">
                                    </div>
                                    <div class="students-card__footer"><span class="students-card__more">Смотреть<img class="students-card__more-cursor" src="<?php echo $root_url_path?>/static/img/general/icons/arrow-orange.png" alt=""></span>
                                    </div>
                                </a>
                            </div>

                        <?php endwhile; ?>
                        <?php endif; ?>

                    </div>
                </div>
                <div class="swiper-controls pupils__swiper-controls">
                    <div class="swiper-controls__actions">
                        <button class="swiper-controls__button swiper-controls__button--prev"></button>
                        <button class="swiper-controls__button swiper-controls__button--next"></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page__rebate">
    <div class="rebate">
        <div class="rebate__container container">
            <div class="rebate__inner">
                <div class="rebate__left"><span class="rebate__title">Скидка 10%</span>
                    <h3 class="rebate__caption">для второго ребёнка </h3>
                </div>
                <div class="rebate__right">
                    <img class="rebate__img" src="<?php echo $root_url_path;?>/static/img/general/rebate-1.png" alt="">
                    <div class="rebate__data"><span class="rebate__label"><label>*</label> Акция действует на постоянной основе</span><a class="rebate__more" href="#"><span class="rebate__more-text">Подробнее</span>
                            <div class="rebate__more-icon"></div></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page__gallery">
    <div class="gallery">
        <div class="gallery__header">
            <div class="gallery__container container"><span class="gallery__heading">Как  проходят  занятия?</span>
                <h2 class="gallery__title">галерея </h2>
            </div>
        </div>

        <div class="gallery__content">
            <div class="gallery__swiper">
                <div class="gallery__swiper-container swiper-container js-gallery-swiper">

                    <div class="gallery__swiper-wrapper swiper-wrapper">

                        <?php $reviews = new WP_Query(array('post_type' => 'galery', 'posts_per_page' => -1));  ?>

                        <?php if ( $reviews->have_posts() ) : while ( $reviews->have_posts() ) : $reviews->the_post();

                            ?>
                            <div class="gallery__swiper-slide swiper-slide">
                                <a class="gallery__link" href="<?php echo the_post_thumbnail_url('full');?>" data-fancybox="gallery">
                                    <img class="gallery__img" src="<?php  echo the_post_thumbnail_url('full');?>" alt="">
                                </a>
                            </div>

                        <?php endwhile; ?>
                        <?php endif; ?>


                    </div>
                </div>
            </div>

            <div class="swiper-controls gallery__swiper-controls">
                <div class="swiper-controls__actions">
                    <button class="swiper-controls__button swiper-controls__button--prev"></button>
                    <div class="swiper-controls__pagination"></div>
                    <button class="swiper-controls__button swiper-controls__button--next"></button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page__masters">
    <div class="masters">
        <div class="masters__container container">


            <div class="masters__left"><span class="masters__heading">Кто  вдохновляет?</span>

                <h2 class="masters__title">Наставники мастерской</h2>

                <div class="masters__swiper-container swiper-container js-caption-swiper">
                    <div class="masters__swiper-wrapper swiper-wrapper">




                        <?php $reviews = new WP_Query(
                                array(
                                    'post_type' => 'educator',
                                    'posts_per_page' => -1,
                                    'post_status'	   => 'publish',
                                    'orderby' => 'post_date',
                                    'order' => 'ASC',
                                    'suppress_filters' => true
                                    )
                        );  ?>

                        <?php if ( $reviews->have_posts() ) : while ( $reviews->have_posts() ) : $reviews->the_post(); ?>

                            <?php
                            $category_name1 =  the_category()->cat_name ;
                            $tags = array("<a", ">");
                            $category_name1 = str_replace($tags, "", $category_name1);
                            $category_name1 =  the_category()->category_nicename  ;
                            ?>
                            <div class="masters__swiper-slide swiper-slide">
                                <div class="masters__data">
                                    <div class="masters__data-top">
                                        <img class="masters__quotes" src="<?php echo $root_url_path;?>/static/img/general/quotes.png" alt="">
                                        <div class="masters__data-position"><span class="masters__name">      <?php the_title();?></span><span class="masters__position">

                                            <?php echo get_field('specialization');?>
                                                     </span>
                                        </div>
                                    </div>
                                    <div class="masters__data-content">
                                        <div class="masters__text">
                                            <?php the_content();?>
                                        </div>
                                    </div>
                                </div>
                                <a class="button button-secondary masters__more" href="#dev" data-fancybox>
                                    <img class="button__icon" src="<?php echo $root_url_path;?>/static/img/general/icons/ask.png" alt=""><span class="button__text">Подробнее о педагогах</span>
                                </a>
                            </div>
                        <?php endwhile; ?>
                        <?php endif; ?>


                    </div>
                </div>
            </div>
            <div class="masters__right">
                <div class="masters__swiper">
                    <div class="masters__swiper-container swiper-container js-masters-swiper">

                        <div class="masters__swiper-wrapper swiper-wrapper">


                            <?php $reviews = new WP_Query(
                                array(
                                    'post_type' => 'educator',
                                    'posts_per_page' => -1,
                                    'post_status'	   => 'publish',
                                    'orderby' => 'post_date',
                                    'order' => 'ASC',
                                    'suppress_filters' => true
                                )
                            );  ?>

                            <?php if ( $reviews->have_posts() ) : while ( $reviews->have_posts() ) : $reviews->the_post(); ?>
                                <div class="masters__swiper-slide swiper-slide">
                                    <div class="masters-card">
                                        <div class="masters-card__left">
                                            <div class="masters-card__photo">
                                                <img class="masters-card__img" src="<?php the_post_thumbnail_url('full');  ?>" alt=""><span class="masters-card__label"> <?php echo get_field('specialization');?></span>
                                            </div>
                                        </div>

                                        <div class="masters-card__right">
                                            <div class="masters-card__header">
                                                <a class="masters-card__progress" href="#">
                                                    <img class="masters-card__progress-icon" src="<?php echo $root_url_path;?>/static/img/general/icons/cup.svg" alt=""><span class="masters-card__progress-text">Достижения</span>
                                                </a>
                                                <ul class="masters-card__list">
                                                    <li class="masters-card__item">
                                                        <a class="masters-card__link" href="<?php echo get_field( 'vk_social_link' );?>" target = "_blank">
                                                            <img class="masters-card__link-img" src="<?php echo $root_url_path;?>/static/img/general/icons/vk-c.png" alt="">
                                                        </a>
                                                    </li>
                                                    <li class="masters-card__item">
                                                        <a class="masters-card__link" href="<?php echo get_field( 'inst_social_link' );?>" target = "_blank">
                                                            <img class="masters-card__link-img" src="<?php echo $root_url_path;?>/static/img/general/icons/in-c.png" alt="">
                                                        </a>
                                                    </li>
                                                    <li class="masters-card__item">
                                                        <a class="masters-card__link" href="<?php echo get_field( 'fb_social_link' );?>" target = "_blank">
                                                            <img class="masters-card__link-img" src="<?php echo $root_url_path;?>/static/img/general/icons/fb-c.png" alt="">
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="masters-card__content"><span class="masters-card__name"><?php the_title();?></span>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                            <?php endif; ?>




                        </div>

                    </div>
                    <div class="masters__thumbs">
                        <div class="masters__inner">
                            <div class="masters__swiper-container swiper-container js-masters-thumbs-swiper">
                                <div class="masters__swiper-wrapper swiper-wrapper">

                                    <?php $educator_counter = 1?>
                                    <?php $reviews = new WP_Query(
                                        array(
                                            'post_type' => 'educator',
                                            'posts_per_page' => -1,
                                            'post_status'	   => 'publish',
                                            'orderby' => 'post_date',
                                            'order' => 'ASC',
                                            'suppress_filters' => true
                                        )
                                            );  ?>

                                    <?php if ( $reviews->have_posts() ) : while ( $reviews->have_posts() ) : $reviews->the_post(); ?>
                                        <?php   $imgURL = MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'secondary-image', NULL, 'large');?>


                                        <div class="masters__swiper-slide swiper-slide">
                                            <div class="masters-thumbs masters-thumbs--<?php echo $educator_counter;?>">
                                                <div class="masters-thumbs__photo">

                                                    <img class="masters-thumbs__img" src="<?php echo $imgURL;?>" alt="">
                                                </div>
                                                <div class="masters-thumbs__data">
                                                    <h4 class="masters-thumbs__title"><?php the_title();?></h4>
                                                </div>
                                            </div>
                                        </div>
                                    <?php $educator_counter++?>
                                    <?php endwhile; ?>
                                    <?php endif; ?>


                                </div>
                            </div>
                            <div class="swiper-controls masters__swiper-controls">
                                <div class="swiper-controls__actions">
                                    <button class="swiper-controls__button swiper-controls__button--prev"></button>
                                    <div class="swiper-controls__pagination"></div>
                                    <button class="swiper-controls__button swiper-controls__button--next"></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page__reviews">
    <div class="reviews">
        <div class="reviews__container container">
            <div class="reviews__header"><span class="reviews__heading">Хотите убедиться сами?</span>
            </div>
            <div class="reviews__content">
                <h2 class="reviews__title">Отзывы </h2>
                <a class="button button-secondary reviews__button" href="#add-reviews" data-fancybox>
                    <img class="button__icon" src="<?php echo $root_url_path;?>/static/img/general/icons/pencil.svg" alt=""><span class="button__text">Оставить отзыв</span>
                </a>
            </div>
            <div class="reviews__swiper">
                <div class="reviews__swiper">
                    <div class="reviews__swiper-container swiper-container js-reviews-swiper">
                        <div class="reviews__swiper-wrapper swiper-wrapper">


                            <?php $counter = 0;?>
                            <?php $reviews = new WP_Query(array('post_type' => 'review', 'posts_per_page' => -1));  ?>

                            <?php if ( $reviews->have_posts() ) : while ( $reviews->have_posts() ) : $reviews->the_post(); ?>

                                <?php
                                $counter ++;
                             ?>
                                <?php
                                $myExcerpt = get_the_excerpt();
                                $tags = array("<p>", "</p>");
                                $myExcerpt = str_replace($tags, "", $myExcerpt);
                                ?>

                                <div class="reviews__swiper-slide swiper-slide">
                                    <div class="reviews-card">
                                        <div class="reviews-card__photo">
                                            <img class="reviews-card__img" src="<?php the_post_thumbnail_url('full');  ?>" alt="">
                                        </div>
                                        <div class="reviews-card__content"><span class="reviews-card__date"><?php echo get_the_date( 'j F Y'); ?></span>
                                            <h4 class="reviews-card__author"><?php the_title();?></h4>
                                            <p class="reviews-card__caption"><?php echo $myExcerpt;?></p><a class="reviews-card__more" href="#review-read<?php echo $counter;?>" data-fancybox="">Читать полностью</a>
                                        </div>
                                    </div>
                                </div>


                                <div class="modal modal--review-read" id="review-read<?php echo $counter;?>">
                                    <form class="form modal__form">
                                        <div class="form__header"><span class="form__title"><?php the_title();?></span><span class="form__date"><?php echo get_the_date( 'j F Y'); ?></span>
                                        </div>
                                        <div class="form__content">
                                           <?php the_content();?>

                                        </div>
                                    </form>
                                </div>
                            <?php endwhile; ?>
                            <?php endif; ?>

                        </div>
                    </div>

                    <div class="swiper-controls reviews__swiper-controls">
                        <div class="swiper-controls__actions">
                            <button class="swiper-controls__button swiper-controls__button--prev"></button>
                            <button class="swiper-controls__button swiper-controls__button--next"></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page__sertificates">
    <div class="sertificates">
        <div class="sertificates__container container">
            <div class="sertificates__header"><span class="sertificates__heading">Что думают эксперты?</span>
                <h2 class="sertificates__title">награды </h2>
            </div>
            <div class="sertificates__content">
                <div class="sertificates__swiper">
                    <div class="sertificates__swiper-container swiper-container js-sertificates-swiper">
                        <div class="sertificates__swiper-wrapper swiper-wrapper">

                            <?php $reviews = new WP_Query(array('post_type' => 'award', 'posts_per_page' => -1));  ?>

                            <?php if ( $reviews->have_posts() ) : while ( $reviews->have_posts() ) : $reviews->the_post(); ?>

                                <div class="sertificates__swiper-slide swiper-slide">
                                    <div class="sertificates-card">
                                        <div class="sertificates-card__photo">
                                            <img class="sertificates-card__img" src="<?php the_post_thumbnail_url('full');  ?>" alt="">
                                        </div>
                                        <div class="sertificates-card__data">
                                            <div class="sertificates-card__top"><span class="sertificates-card__label">диплом</span>
                                                <a class="sertificates-card__download" download href="<?php echo get_field( 'award_download_link' );?>"><i class="fa fa-link"></i><span class="sertificates-card__download-text">Скачать</span></a>
                                            </div><span class="sertificates-card__title"><?php the_title();?></span>
                                        </div>
                                    </div>
                                </div>

                            <?php endwhile; ?>
                            <?php endif; ?>

                        </div>
                    </div>
                    <div class="swiper-controls sertificates__swiper-controls">
                        <div class="swiper-controls__actions">
                            <button class="swiper-controls__button swiper-controls__button--prev"></button>
                            <button class="swiper-controls__button swiper-controls__button--next"></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page__contacts">
    <div class="contacts">
        <div class="contacts__container container">
            <div class="contacts__header"><span class="contacts__heading">Как с нами связаться?</span>
                <h2 class="contacts__title">Контакты</h2>
            </div>
            <div class="contacts__content">
                <div class="contacts__left">
                    <ul class="info-box contacts__info-box">
                        <li class="info-box__item">
                            <div class="info-box__icon"><i class="fa fa-phone"></i>
                            </div>
                            <div class="info-box__data"><span class="info-box__label">Наш телефон:</span>
                                <h5 class="info-box__title"><a class="info-box__link" href="tel:+79104691291">+7 (910) 469-12-91</a></h5>
                            </div>
                        </li>
                        <li class="info-box__item">
                            <div class="info-box__icon">
                                <img class="info-box__img" src="<?php echo $root_url_path;?>/static/img/general/icons/map.png" alt="">
                            </div>
                            <div class="info-box__data"><span class="info-box__label">Наш адрес:</span><span class="info-box__place"><img class="info-box__plce-img" src="<?php echo $root_url_path;?>/static/img/general/icons/map-small.svg" alt=""><span class="info-box__place-text">м. Семёновская</span></span>
                                <h5 class="info-box__title"><a class="info-box__link" href="https://goo.gl/maps/WkSPheUGKV39NurH9" target="_blank">пр-т Буденного 14, Дом Культуры "Чайка"</a></h5>
                            </div>
                        </li>
                        <li class="info-box__item">
                            <div class="info-box__icon"><i class="fa fa-envelope"></i>
                            </div>
                            <div class="info-box__data"><span class="info-box__label">Нашa почта:</span>
                                <h5 class="info-box__title"><a class="info-box__link" href="mailto:info@artist-m.ru">info@artist-m.ru</a></h5>
                            </div>
                        </li>
                    </ul>
                    <div class="contacts__map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1121.952202732579!2d37.7209225!3d55.7775321!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b5356a1f6783e3%3A0x1225fa904a65618d!2z0JTQvtC8INCa0YPQu9GM0YLRg9GA0Ysg0KfQsNC50LrQsA!5e0!3m2!1sru!2sua!4v1565121403216!5m2!1sru!2sua"
                                width="341" height="341" frameborder="0" style="border:0;" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="contacts__right">
                    <form class="form contacts__form"  id ="second_form">
                        <div class="form__header"><span class="form__title">Остались вопросы?</span><span class="form__caption">Мы свяжемся с вами в ближайшее время и ответим на них!</span>
                        </div>
                        <div class="form__content">
                            <div class="form__field">
                                <div class="form__icon">
                                    <img class="form__img" src="<?php echo $root_url_path;?>/static/img/general/icons/user.svg" alt="">
                                </div>
                                <input class="form__input" type="text" placeholder="Ваше имя" name="user_name" required>
                            </div>
                            <div class="form__field">
                                <div class="form__icon">
                                    <img class="form__img" src="<?php echo $root_url_path;?>/static/img/general/icons/mobile.svg" alt="">
                                </div>
                                <input class="form__input" type="tel" placeholder="Номер телефона" name="user_number" required>
                            </div>
                        </div>
                        <div class="form__footer">
                            <button class="form__send"  id="form2-button" type="submit" onclick = "return Form2ClickCallBack();">Отправить заявку
                                <img class="form__send-icon" src="<?php echo $root_url_path;?>/static/img/general/icons/plane.svg" alt="">
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<?php get_footer();