<?php

global $root_url_path;

get_header(); ?>

    <div class="main page__main">
        <div class="main__header">
            <div class="main__container container">
                <h3 class="main__title">Актёрское мастерство</h3>
                <ul class="breadcrumbs main__breadcrumbs">
                    <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/">Главная</a>
                    </li>
                    <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Актёрское мастерство</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main__content" style="z-index:-1;position: relative;">
            <div class="terms main__terms">
                <div class="terms__container container">
                    <div class="terms__body">
                        <div class="terms__section">
                            <div class="terms__content">
                                <p><strong><?php OutputActing('first_tb'); ?> </strong> <?php OutputActing('first_par'); ?></p>
                                <div class="terms__grid">
                                    <div class="terms__column">
                                    	<a href="<?php OutputActingImage("image1"); ?>" data-fancybox="at">
                                        	<img class="terms__img" src="<?php OutputActingImage("image1"); ?>" alt="">
                                        </a>
                                    </div>
                                    <div class="terms__column">
                                    	<a href="<?php OutputActingImage("image2"); ?>" data-fancybox="at">
                                        	<img class="terms__img" src="<?php OutputActingImage("image2"); ?>" alt="">
                                        </a>
                                    </div>
                                </div>
                                <p><?php OutputActing('second_par'); ?></p>
                            </div>
                        </div>
                        <div class="terms__section">
                            <div class="terms__head">
                                <h3 class="terms__title"><?php OutputActing('first_tittle'); ?></h3>
                            </div>
                            <div class="terms__content">
                                <p><?php OutputActing('third_par1'); ?></p>
                                <p><?php OutputActing('third_par2'); ?></p>
                            </div>
                        </div>
                        <div class="terms__section">
                            <div class="terms__head">
                                <h3 class="terms__title"><?php OutputActing('second_tittle'); ?></h3>
                            </div>
                            <div class="terms__content">
                                <?php if(false)
                                    {?>
                                <div class="video terms__video">
                                    <a class="video__link" href="<?php echo $root_url_path;?>/static/img/general/vd-1.png" data-fancybox="">
                                        <img class="video__img" src="<?php echo $root_url_path;?>/static/img/general/vd-1.png" alt="">
                                        <div class="video__actions">
                                            <img class="video__icon" src="<?php echo $root_url_path;?>/static/img/general/pl-ic.png" alt=""><span class="video__text">Посмотреть ролик</span>
                                        </div>
                                    </a>
                                </div>
                                        <?php }?>
                                <p><?php OutputActing('fourth_par1'); ?> <a href="<?php OutputActing('link_text'); ?>"><?php OutputActing('name_text'); ?></a><?php OutputActing('fourth_par2'); ?></p>
                                <ul>
                                    <li style="padding:0;">
                                        <?php

                                        ?>
                                        <ul>

                                            <?php OutputMarkedList(get_theme_mod('action_mark_list')); ?>

                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php get_footer();
