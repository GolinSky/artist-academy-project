<?php

global $root_url_path;

get_header(); ?>

    <div class="main page__main">
        <div class="main__header">
            <div class="main__container container">
                <h3 class="main__title">Вокал</h3>
                <ul class="breadcrumbs main__breadcrumbs">
                    <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/">Главная</a>
                    </li>
                    <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Вокал</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main__content" style="z-index:-1;position: relative;">
            <div class="terms main__terms">
                <div class="terms__container container">
                    <div class="terms__body">
                        <div class="terms__section">
                            <div class="terms__content">
                                <p><strong><?php OutputVocals('first_tb'); ?> </strong> <?php OutputVocals('first_par'); ?></p>
                                <div class="terms__grid">
                                    <div class="terms__column">
                                    	<a href="<?php OutputVocalsImage("image1"); ?>" data-fancybox="at">
                                        	<img class="terms__img" src="<?php OutputVocalsImage("image1"); ?>" alt="">
                                        </a>
                                    </div>
                                    <div class="terms__column">
                                    	<a href="<?php OutputVocalsImage("image2"); ?>" data-fancybox="at">
                                        	<img class="terms__img" src="<?php OutputVocalsImage("image2"); ?>" alt="">
                                        </a>
                                    </div>
                                </div>
                                <p><?php OutputVocals('second_par'); ?></p>
                            </div>
                        </div>
                        <div class="terms__section">
                            <div class="terms__head">
                                <h3 class="terms__title"><?php OutputVocals('first_tittle'); ?></h3>
                            </div>
                            <div class="terms__content">
                                <p><?php OutputVocals('third_par1'); ?></p>
                                <p><?php OutputVocals('third_par2'); ?></p>
                            </div>
                        </div>
                        <div class="terms__section">
                            <div class="terms__head">
                                <h3 class="terms__title"><?php OutputVocals('second_tittle'); ?></h3>
                            </div>
                            <div class="terms__content">
                                <?php if(false)
                                    {?>
                                <div class="video terms__video">
                                    <a class="video__link" href="<?php echo $root_url_path;?>/static/img/general/vd-1.png" data-fancybox="">
                                        <img class="video__img" src="<?php echo $root_url_path;?>/static/img/general/vd-1.png" alt="">
                                        <div class="video__actions">
                                            <img class="video__icon" src="<?php echo $root_url_path;?>/static/img/general/pl-ic.png" alt=""><span class="video__text">Посмотреть ролик</span>
                                        </div>
                                    </a>
                                </div>
                                        <?php }?>
                                <p><?php OutputVocals('fourth_par1'); ?> <a href="<?php OutputVocals('link_text'); ?>"><?php OutputVocals('name_text'); ?></a><?php OutputVocals('fourth_par2'); ?></p>
                                <ul>
                                    <li style="padding:0;">
                                        <?php

                                        ?>
                                        <ul>

                                            <?php OutputMarkedList(get_theme_mod('vocal_mark_list')); ?>

                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php get_footer();
