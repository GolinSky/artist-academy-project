<?php
global $root_url_path;
get_header(); ?>

    <div class="main page__main">
        <div class="main__header">
            <div class="main__container container">
                <h3 class="main__title">Контакты</h3>
                <ul class="breadcrumbs main__breadcrumbs">
                    <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/">Главная</a>
                    </li>
                    <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Контакты</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main__content">
            <div class="place main__place">
                <div class="place__container container">
                    <div class="place__content">
                        <div class="place__section">
                            <div class="place__header opened">
                                <div class="place__data">
                                    <img class="place__icon" src="<?php echo $root_url_path;?>/static/img/contacts/map-dark.svg" alt="">
                                    <h3 class="place__title">Филиал на <span class="place__label">семёновской</span></h3>
                                </div>
                                <div class="place__arrow">
                                    <img class="place__arrow-img" src="<?php echo $root_url_path;?>/static/img/contacts/arrow-down.svg" alt="">
                                </div>
                            </div>
                            <div class="place__info" style="display: block;">
                                <ul class="info-box place__info-box">
                                    <li class="info-box__item">
                                        <div class="info-box__icon"><i class="fa fa-phone"></i>
                                        </div>
                                        <div class="info-box__data"><span class="info-box__label">Наш телефон:</span>
                                            <h5 class="info-box__title"><a class="info-box__link" href="tel:+79104691291">+7 (910) 469-12-91</a></h5>
                                        </div>
                                    </li>
                                    <li class="info-box__item">
                                        <div class="info-box__icon">
                                            <img class="info-box__img" src="<?php echo $root_url_path;?>/static/img/contacts/map.png" alt="">
                                        </div>
                                        <div class="info-box__data"><span class="info-box__label">Наш адрес:</span>
                                            <h5 class="info-box__title"><a class="info-box__link" href="https://goo.gl/maps/WkSPheUGKV39NurH9" target="_blank">пр-т Буденного 14, Дом Культуры "Чайка"</a></h5>
                                        </div>
                                    </li>
                                    <li class="info-box__item">
                                        <div class="info-box__icon"><i class="fa fa-envelope"></i>
                                        </div>
                                        <div class="info-box__data"><span class="info-box__label">Наш почта:</span>
                                            <h5 class="info-box__title"><a class="info-box__link" href="mailto:info@artist-m.ru">info@artist-m.ru</a></h5>
                                        </div>
                                    </li>
                                </ul>
                                <div class="place__map">
                                    <iframe src="https://yandex.ua/map-widget/v1/-/CGwku0n4" width="100%" height="395" frameborder="0"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php get_footer();
