<!DOCTYPE html>
<html class="no-js" lang="ru">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>default title</title>
        <meta content="" name="description">
        <meta content="" name="keywords">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="telephone=no" name="format-detection">
        <meta name="HandheldFriendly" content="true">

        <!--[if (gt IE 9)|!(IE)]><!-->
        <link href="<?php echo get_stylesheet_directory_uri()?>/static/css/main.min.css" rel="stylesheet" type="text/css">
        <!--<![endif]-->
        <meta property="og:title" content="#{data.title}">
        <meta property="og:title" content="">
        <meta property="og:url" content="">
        <meta property="og:description" content="">
        <meta property="og:image" content="">
        <meta property="og:image:type" content="image/jpeg">
        <meta property="og:image:width" content="500">
        <meta property="og:image:height" content="300">
        <meta property="twitter:description" content="">
        <link rel="image_src" href="">
        <link rel="icon" type="image/x-icon" href="favicon.ico">
        <script>
            (function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)
        </script>
    </head>

    <body class="page">
        <nav class="pushy pushy-right">
            <div class="pushy-content">
                <ul>
                    <li class="pushy-submenu">Направления<i class="fa fa-angle-down"></i>
                        <ul>
                            <li class="pushy-link"><a href="#">Актерское мастерство</a>
                            </li>
                            <li class="pushy-link"><a href="#">Хореография</a>
                            </li>
                            <li class="pushy-link"><a href="#">Вокал</a>
                            </li>
                        </ul>
                    </li>
                    <li class="pushy-link"><a href="#">Ученики</a>
                    </li>
                    <li class="pushy-link"><a href="#">Педагоги</a>
                    </li>
                    <li class="pushy-link"><a href="#">Отзывы</a>
                    </li>
                    <li class="pushy-link"><a href="#">Цены</a>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="page__overlay site-overlay"></div>
        
        <div class="lines page__lines">
            <div class="lines__container container">
                <ul class="lines__list">
                    <li class="lines__item">
                        <img class="lines__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/vertical-lines.png" alt="">
                    </li>
                    <li class="lines__item">
                        <img class="lines__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/vertical-lines.png" alt="">
                    </li>
                    <li class="lines__item">
                        <img class="lines__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/vertical-lines.png" alt="">
                    </li>
                    <li class="lines__item">
                        <img class="lines__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/vertical-lines.png" alt="">
                    </li>
                    <li class="lines__item">
                        <img class="lines__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/vertical-lines.png" alt="">
                    </li>
                </ul>
            </div>
        </div>
        <div class="page__header">
            <div class="header">
                <div class="header__container container">
                    <a class="logo header__logo" href="/">
                        <img class="logo__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/logo.png" alt="">
                    </a>
                    <div class="header__data">
                        <div class="navbar header__navbar">
                            <ul class="socials navbar__socials">
                                <li class="socials__item">
                                    <a class="socials__link" href="#">
                                        <img class="socials__icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/vk.svg" alt="">
                                    </a>
                                </li>
                                <li class="socials__item">
                                    <a class="socials__link" href="#">
                                        <img class="socials__icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/inst.svg" alt="">
                                    </a>
                                </li>
                                <li class="socials__item">
                                    <a class="socials__link" href="#">
                                        <img class="socials__icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/fb.svg" alt="">
                                    </a>
                                </li>
                                <li class="socials__item">
                                    <a class="socials__link" href="#">
                                        <img class="socials__icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/ok.svg" alt="">
                                    </a>
                                </li>
                                <li class="socials__item socials__item--accent"><a class="socials__link" href="#"><i class="fa fa-youtube-play"></i>
                      <div class="socials__link-text">Идет онлайн-эфир!</div></a>
                                </li>
                            </ul>
                            <div class="navbar__data">
                                <ul class="contacts-box navbar__contacts-box">
                                    <li class="contacts-box__item"><a class="contacts-box__link" href="tel:+79104691291"><span class="contacts-box__icon"><img class="contacts-box__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/phone.svg" alt=""></span><span class="contacts-box__text">+7 (910) 469-12-91</span></a>
                                    </li>
                                    <li class="contacts-box__item"><a class="contacts-box__link" href="mailto:info@artist-m.ru"><span class="contacts-box__icon"><img class="contacts-box__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/envelope.svg" alt=""></span><span class="contacts-box__text">info@artist-m.ru</span></a>
                                    </li>
                                </ul>
                                <a class="button button-secondary navbar__login" href="#login" data-fancybox>
                                    <img class="button__icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/lock.svg" alt=""><span class="button__text">Войти в кабинет</span>
                                </a><a class="navbar__mobile-btn" href="#"><i class="fa fa-bars"></i></a>
                            </div>
                        </div>
                        <div class="header__content">
                            <ul class="menu header__menu">
                                <li class="menu__item"><a class="menu__link" href="#">Направления</a>
                                    <ul class="menu__dropdown">
                                        <li class="menu__dropdown-item"><a class="menu__dropdown-link" href="#">droplnk</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu__item"><a class="menu__link" href="#">Ученики</a>
                                </li>
                                <li class="menu__item"><a class="menu__link" href="#">Педагоги</a>
                                </li>
                                <li class="menu__item"><a class="menu__link" href="#">Отзывы</a>
                                </li>
                                <li class="menu__item menu__item--accent"><a class="menu__link" href="#">Цены</a>
                                </li>
                                <li class="menu__item"><a class="menu__link" href="#">Контакты</a>
                                </li>
                            </ul>
                            <div class="header__actions">
                                <a class="button header__button header__button--writing" href="#">
                                    <img class="button__icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/calendar.svg" alt=""><span class="button__text">Раписание занятий</span>
                                </a>
                                <a class="button button-default header__button header__button--order" href="#order" data-fancybox>
                                    <img class="button__icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/pencil.svg" alt=""><span class="button__text">Оставить заявку</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main main--secondary">
            <div class="main__header">
                <div class="main__container container">
                    <h3 class="main__title">расписание занятий</h3>
                    <ul class="breadcrumbs main__breadcrumbs">
                        <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/">Главная</a>
                        </li>
                        <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Расписание</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="main__content">
                <div class="sked main__sked">
                    <div class="sked__container container">
                        <div class="sked__header">
                            <div class="selection sked__selection">
                                <div class="selection__grid">
                                    <div class="selection__row selection__row--age">
                                        <ul class="selection__list">
                                            <li class="selection__item selection__item--red color">
                                                <label class="selection__label">
                                                    <input type="radio" name="radio" class="selection__radio" /> <span class="selection__text">3-5 лет</span>
                                                </label>
                                            </li>
                                            <li class="selection__item selection__item--orange color">
                                                <label class="selection__label">
                                                    <input type="radio" name="radio" class="selection__radio" /> <span class="selection__text">6-8 лет</span>
                                                </label>
                                            </li>
                                            <li class="selection__item selection__item--gold color">
                                                <label class="selection__label">
                                                    <input type="radio" name="radio" class="selection__radio" /> <span class="selection__text">9-12 лет</span>
                                                </label>
                                            </li>
                                            <li class="selection__item selection__item--purple color">
                                                <label class="selection__label">
                                                    <input type="radio" name="radio" class="selection__radio" /> <span class="selection__text">13-15 лет</span>
                                                </label>
                                            </li>
                                            <li class="selection__item color">
                                                <label class="selection__label">
                                                    <input type="radio" name="radio" class="selection__radio" /> <span class="selection__text">16+ лет</span>
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="selection__row selection__row--type">
                                        <ul class="selection__list">
                                            <li class="selection__item">
                                                <label class="selection__label">
                                                    <input type="radio" name="radio" class="selection__radio" />
                                                    <div class="selection__icon">
                                                        <img class="selection__icon-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/moon.svg" alt="">
                                                    </div><span class="selection__text">Начинающие</span>
                                                </label>
                                            </li>
                                            <li class="selection__item">
                                                <label class="selection__label">
                                                    <input type="radio" name="radio" class="selection__radio" />
                                                    <div class="selection__icon">
                                                        <img class="selection__icon-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/star.svg" alt="">
                                                    </div><span class="selection__text">Профессионалы</span>
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="selection__row selection__row--quantity">
                                        <ul class="selection__list">
                                            <li class="selection__item color">
                                                <label class="selection__label">
                                                    <input type="radio" name="radio" class="selection__radio" /><span class="selection__number">1</span><span class="selection__text">1 раз в неделю</span>
                                                </label>
                                            </li>
                                            <li class="selection__item color">
                                                <label class="selection__label">
                                                    <input type="radio" name="radio" class="selection__radio" /><span class="selection__number">2</span><span class="selection__text"> 2 разa в неделю</span>
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sked__content">
                            <div class="sked__grid">
                                <div class="sked__column">
                                    <div class="sked-card">
                                        <div class="sked-card__header"><span class="sked-card__title">Понедельник</span>
                                        </div>
                                        <div class="sked-card__content">
                                            <div class="sked-card__section sked-card__section--red">
                                                <div class="sked-card__line"></div>
                                                <div class="sked-card__row"><span class="sked-card__row-time">18:00 - 21:00</span>
                                                    <ul class="sked-card__row-list">
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-1.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-2.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-3.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                    </ul><span class="sked-card__row-price">от 10 000 <i class="fa fa-rub"></i></span>
                                                </div>
                                                <div class="sked-card__line"></div>
                                            </div>
                                            <div class="sked-card__section sked-card__section--orange">
                                                <div class="sked-card__line"></div>
                                                <div class="sked-card__row"><span class="sked-card__row-time">18:00 - 21:00</span>
                                                    <ul class="sked-card__row-list">
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-1.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-2.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-3.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                    </ul><span class="sked-card__row-price">от 10 000 <i class="fa fa-rub"></i></span>
                                                </div>
                                                <div class="sked-card__line"></div>
                                            </div>
                                            <div class="sked-card__section sked-card__section--purple">
                                                <div class="sked-card__line"></div>
                                                <div class="sked-card__row"><span class="sked-card__row-time">18:00 - 21:00</span>
                                                    <ul class="sked-card__row-list">
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-1.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-2.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-3.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                    </ul><span class="sked-card__row-price">от 10 000 <i class="fa fa-rub"></i></span>
                                                </div>
                                                <div class="sked-card__line"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sked__column">
                                    <div class="sked-card">
                                        <div class="sked-card__header"><span class="sked-card__title">вторник</span>
                                        </div>
                                        <div class="sked-card__content">
                                            <div class="sked-card__section sked-card__section--red">
                                                <div class="sked-card__line"></div>
                                                <div class="sked-card__row"><span class="sked-card__row-time">18:00 - 21:00</span>
                                                    <ul class="sked-card__row-list">
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-1.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-2.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-3.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                    </ul><span class="sked-card__row-price">от 10 000 <i class="fa fa-rub"></i></span>
                                                </div>
                                                <div class="sked-card__line"></div>
                                            </div>
                                            <div class="sked-card__section sked-card__section--orange">
                                                <div class="sked-card__line"></div>
                                                <div class="sked-card__row"><span class="sked-card__row-time">18:00 - 21:00</span>
                                                    <ul class="sked-card__row-list">
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-1.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-2.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-3.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                    </ul><span class="sked-card__row-price">от 10 000 <i class="fa fa-rub"></i></span>
                                                </div>
                                                <div class="sked-card__line"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sked__column">
                                    <div class="sked-card">
                                        <div class="sked-card__header"><span class="sked-card__title">среда</span>
                                        </div>
                                        <div class="sked-card__content">
                                            <div class="sked-card__section sked-card__section--red">
                                                <div class="sked-card__line"></div>
                                                <div class="sked-card__row"><span class="sked-card__row-time">18:00 - 21:00</span>
                                                    <ul class="sked-card__row-list">
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-1.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-2.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-3.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                    </ul><span class="sked-card__row-price">от 10 000 <i class="fa fa-rub"></i></span>
                                                </div>
                                                <div class="sked-card__line"></div>
                                            </div>
                                            <div class="sked-card__section sked-card__section--gray">
                                                <div class="sked-card__line"></div>
                                                <div class="sked-card__row"><span class="sked-card__row-time">18:00 - 21:00</span>
                                                    <ul class="sked-card__row-list">
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-1.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-2.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-3.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                    </ul><span class="sked-card__row-price">от 10 000 <i class="fa fa-rub"></i></span>
                                                </div>
                                                <div class="sked-card__line"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sked__column">
                                    <div class="sked-card">
                                        <div class="sked-card__header"><span class="sked-card__title">четверг</span>
                                        </div>
                                        <div class="sked-card__content">
                                            <div class="sked-card__section sked-card__section--red">
                                                <div class="sked-card__line"></div>
                                                <div class="sked-card__row"><span class="sked-card__row-time">18:00 - 21:00</span>
                                                    <ul class="sked-card__row-list">
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-1.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-2.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-3.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                    </ul><span class="sked-card__row-price">от 10 000 <i class="fa fa-rub"></i></span>
                                                </div>
                                                <div class="sked-card__line"></div>
                                            </div>
                                            <div class="sked-card__section sked-card__section--orange">
                                                <div class="sked-card__line"></div>
                                                <div class="sked-card__row"><span class="sked-card__row-time">18:00 - 21:00</span>
                                                    <ul class="sked-card__row-list">
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-1.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-2.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-3.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                    </ul><span class="sked-card__row-price">от 10 000 <i class="fa fa-rub"></i></span>
                                                </div>
                                                <div class="sked-card__line"></div>
                                            </div>
                                            <div class="sked-card__section sked-card__section--gold">
                                                <div class="sked-card__line"></div>
                                                <div class="sked-card__row"><span class="sked-card__row-time">18:00 - 21:00</span>
                                                    <ul class="sked-card__row-list">
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-1.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-2.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-3.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                    </ul><span class="sked-card__row-price">от 10 000 <i class="fa fa-rub"></i></span>
                                                </div>
                                                <div class="sked-card__line"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sked__column">
                                    <div class="sked-card">
                                        <div class="sked-card__header"><span class="sked-card__title">пятница</span>
                                        </div>
                                        <div class="sked-card__content">
                                            <div class="sked-card__section sked-card__section--red">
                                                <div class="sked-card__line"></div>
                                                <div class="sked-card__row"><span class="sked-card__row-time">18:00 - 21:00</span>
                                                    <ul class="sked-card__row-list">
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-1.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-2.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-3.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                    </ul><span class="sked-card__row-price">от 10 000 <i class="fa fa-rub"></i></span>
                                                </div>
                                                <div class="sked-card__line"></div>
                                            </div>
                                            <div class="sked-card__section sked-card__section--orange">
                                                <div class="sked-card__line"></div>
                                                <div class="sked-card__row"><span class="sked-card__row-time">18:00 - 21:00</span>
                                                    <ul class="sked-card__row-list">
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-1.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-2.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-3.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                    </ul><span class="sked-card__row-price">от 10 000 <i class="fa fa-rub"></i></span>
                                                </div>
                                                <div class="sked-card__line"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sked__column">
                                    <div class="sked-card">
                                        <div class="sked-card__header"><span class="sked-card__title">суббота</span>
                                        </div>
                                        <div class="sked-card__content">
                                            <div class="sked-card__section sked-card__section--orange">
                                                <div class="sked-card__line"></div>
                                                <div class="sked-card__row"><span class="sked-card__row-time">18:00 - 21:00</span>
                                                    <ul class="sked-card__row-list">
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-1.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-2.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                        <li class="sked-card__row-item">
                                                            <div class="sked-card__row-photo">
                                                                <img class="sked-card__row-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/chl-3.png" alt="">
                                                            </div>
                                                            <div class="sked-card__row-dropdown"><span class="sked-card__row-name">Владислав Моматенко</span><span class="sked-card__row-position">Актерское мастерство</span>
                                                            </div>
                                                        </li>
                                                    </ul><span class="sked-card__row-price">от 10 000  <i class="fa fa-rub"></i></span>
                                                </div>
                                                <div class="sked-card__line"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sked__footer">
                            <div class="sked__informer"><span class="sked__informer-title">Внимание! </span>
                                <p class="sked__informer-text">Открытие новых групп происходит в течение всего учебного года. Если Вас не устраивают дни и время занятий - запишитесь у администратора. Как только группа по новому расписанию будет сформирована, Ваш ребенок сможет начать
                                    занятия.</p>
                            </div>
                            <div class="order sked__order">
                                <h3 class="order__title">Остались вопросы?   <span> Звоните!</span></h3>
                                <form class="order__form" action="#">
                                    <div class="order__form-field">
                                        <input class="order__form-input" type="text" placeholder="Ваше имя">
                                    </div>
                                    <div class="order__form-field">
                                        <input class="order__form-input" type="text" placeholder="Номер телефона">
                                    </div>
                                    <button class="button button-secondary order__form-send">Оставить заявку</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="page__footer">
            <footer class="footer">
                <div class="footer__container container">
                    <div class="footer__top">
                        <div class="subs footer__subs">
                            <div class="subs__logo">
                                <img class="subs__logo-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/footer-logo.png" alt="">
                            </div>
                            <div class="subs__data">
                                <h3 class="subs__title">НОВОСТИ И СПЕЦИАЛЬНЫЕ ПРЕДЛОЖЕНИЯ</h3>
                                <form class="subs__form" action="#">
                                    <div class="subs__field">
                                        <img class="subs__icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/envelope-orange.png" alt="">
                                        <input class="subs__input" type="text" placeholder="Введите адрес электронной почты">
                                        <button class="button subs__send">Подписаться</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="footer__content">
                        <div class="footer__grid">
                            <div class="footer__column"><span class="footer__title">Направления</span>
                                <ul class="footer__list">
                                    <li class="footer__item"><a class="footer__link" href="#">Актерское мастерство</a>
                                    </li>
                                    <li class="footer__item"><a class="footer__link" href="#">Хореография</a>
                                    </li>
                                    <li class="footer__item"><a class="footer__link" href="#">Вокал</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="footer__column"><span class="footer__title">О нас</span>
                                <ul class="footer__list">
                                    <li class="footer__item"><a class="footer__link" href="#">Ученики</a>
                                    </li>
                                    <li class="footer__item"><a class="footer__link" href="#">Педагоги</a>
                                    </li>
                                    <li class="footer__item"><a class="footer__link" href="#">Отзывы</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="footer__column"><span class="footer__title">Информация</span>
                                <ul class="footer__list">
                                    <li class="footer__item"><a class="footer__link" href="#">Расписание</a>
                                    </li>
                                    <li class="footer__item"><a class="footer__link" href="#">Новости</a>
                                    </li>
                                    <li class="footer__item"><a class="footer__link" href="#">Контакты</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="footer__column"><span class="footer__title">Прайс-лист</span>
                                <ul class="footer__list">
                                    <li class="footer__item"><a class="footer__link" href="#">Цены</a>
                                    </li>
                                    <li class="footer__item"><a class="footer__link" href="#">О поступлении</a>
                                    </li>
                                    <li class="footer__item"><a class="footer__link" href="#">Акции</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="footer__column"><span class="footer__title">Контакты и социальные сети</span>
                                <div class="footer__inner">
                                    <ul class="info-box footer__info-box">
                                        <li class="info-box__item">
                                            <div class="info-box__icon"><i class="fa fa-phone"></i>
                                            </div>
                                            <div class="info-box__data"><span class="info-box__label">Наш телефон:</span><a class="info-box__link" href="tel:+79104691291">+7 (910) 469-12-91</a>
                                            </div>
                                        </li>
                                        <li class="info-box__item">
                                            <div class="info-box__icon"><i class="fa fa-envelope"></i>
                                            </div>
                                            <div class="info-box__data"><span class="info-box__label">Наш почта:</span><a class="info-box__link" href="mailto:info@artist-m.ru">info@artist-m.ru</a>
                                            </div>
                                        </li>
                                    </ul>
                                    <ul class="socials footer__socials">
                                        <li class="socials__item"><a class="socials__link" href="#"><i class="fa fa-vk"></i></a>
                                        </li>
                                        <li class="socials__item"><a class="socials__link" href="#"><i class="fa fa-instagram"></i></a>
                                        </li>
                                        <li class="socials__item"><a class="socials__link" href="#"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li class="socials__item"><a class="socials__link" href="#"><i class="fa fa-odnoklassniki"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="copyright footer__copyright"><span class="copyright__text">ООО "Мастерская Артист" </span><span class="copyright__text">© 2019,  Все права защищены</span>
                        <div class="copyright__hashtag"><span class="copyright__hashtag-label">#</span><span class="copyright__hashtag-text">мастерскаяартист</span>
                        </div>
                    </div>
                </div>
            </footer>
            <div class="modals page__modals">
                <div class="modal modal--login" id="login">
                    <form class="form modal__form">
                        <div class="form__header"><span class="form__title">Добро пожаловать!</span>
                        </div>
                        <div class="form__content">
                            <div class="form__field">
                                <div class="form__icon">
                                    <img class="form__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/envelope-wh.svg" alt="">
                                </div>
                                <input class="form__input" type="email" placeholder="Введите почту">
                            </div>
                            <div class="form__field">
                                <div class="form__icon">
                                    <img class="form__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/lock-full.svg" alt="">
                                </div>
                                <input class="form__input" id="password" type="password" placeholder="Введите пароль">
                                <button class="form__show-pass"></button>
                            </div>
                        </div>
                        <div class="form__footer">
                            <button class="form__send">
                                <img class="form__send-icon form__send-icon--left" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/lock.svg" alt=""><span class="form__send-text">Отправить заявку</span>
                            </button><a class="button button-default form__reg" href="#reg" data-fancybox>Зарегистрироваться</a>
                            <a class="form__restore" href="#restore" data-fancybox>
                                <img class="form__restore-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/info.png" alt=""><span class="form__restore-text">Забыли пароль?</span>
                            </a>
                        </div>
                    </form>
                </div>
                <div class="modal modal--reg" id="reg">
                    <form class="form modal__form">
                        <div class="form__header"><span class="form__title">Регистрация</span>
                        </div>
                        <div class="form__content">
                            <div class="form__field">
                                <div class="form__icon">
                                    <img class="form__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/lock-bg.png" alt="">
                                </div>
                                <input class="form__input" type="email" placeholder="Введите пин-код ученика">
                            </div>
                        </div>
                        <div class="form__footer"><a class="form__send" href="#reg2" data-fancybox><span class="form__send-text">Продолжить регистрацию</span></a>
                        </div>
                    </form>
                </div>
                <div class="modal modal--reg-second" id="reg2">
                    <form class="form modal__form">
                        <div class="form__header"><span class="form__label">Здравствуйте,</span><span class="form__title form__title--small">Виктория кожевникова</span>
                        </div>
                        <div class="form__content">
                            <div class="form__field">
                                <div class="form__icon">
                                    <img class="form__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/envelope-wh.svg" alt="">
                                </div>
                                <input class="form__input" type="email" placeholder="Введите почту">
                            </div>
                            <div class="form__field">
                                <div class="form__icon">
                                    <img class="form__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/lock-full.svg" alt="">
                                </div>
                                <input class="form__input" type="password" placeholder="Введите пароль">
                            </div>
                            <div class="form__field">
                                <div class="form__icon">
                                    <img class="form__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/lock-full.svg" alt="">
                                </div>
                                <input class="form__input" type="password" placeholder="Подтвердите пароль">
                            </div>
                        </div>
                        <div class="form__footer">
                            <button class="form__send"><span class="form__send-text">Зарегистрироваться</span>
                            </button>
                        </div>
                    </form>
                </div>
                <div class="modal modal--restore" id="restore">
                    <form class="form modal__form">
                        <div class="form__header"><span class="form__label">Мы вышлем письмо вам на почту!</span><span class="form__title form__title--small">восстановление пароля</span>
                        </div>
                        <div class="form__content">
                            <div class="form__field">
                                <div class="form__icon">
                                    <img class="form__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/envelope-wh.svg" alt="">
                                </div>
                                <input class="form__input" type="email" placeholder="Введите почту">
                            </div>
                        </div>
                        <div class="form__footer"><a class="button button-secondary form__reg" href="#restore-second" data-fancybox>Восстановить пароль</a>
                        </div>
                    </form>
                </div>
                <div class="modal modal--restore-second" id="restore-second">
                    <form class="form modal__form">
                        <div class="form__header"><span class="form__label">Введите новый пароль и запомните его :)</span><span class="form__title form__title--small">восстановление пароля</span>
                        </div>
                        <div class="form__content">
                            <div class="form__field">
                                <div class="form__icon">
                                    <img class="form__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/lock-full.svg" alt="">
                                </div>
                                <input class="form__input" type="password" placeholder="Введите новый пароль">
                            </div>
                            <div class="form__field">
                                <div class="form__icon">
                                    <img class="form__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/lock-full.svg" alt="">
                                </div>
                                <input class="form__input" type="password" placeholder="Подтвердите пароль">
                            </div>
                        </div>
                        <div class="form__footer">
                            <button class="button button-secondary form__reg">Обновить пароль</button>
                        </div>
                    </form>
                </div>
                <div class="modal modal--add-reviews" id="add-reviews">
                    <form class="form modal__form">
                        <div class="form__header"><span class="form__title">форма отзыва</span>
                        </div>
                        <div class="form__content">
                            <div class="form__field">
                                <div class="form__icon">
                                    <img class="form__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/user.svg" alt="">
                                </div>
                                <input class="form__input" type="text" placeholder="Имя и фамилия">
                                <div class="form__field-label">*</div>
                            </div>
                            <div class="form__field">
                                <div class="form__icon">
                                    <img class="form__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/envelope-wh.svg" alt="">
                                </div>
                                <input class="form__input" type="email" placeholder="Введите почту">
                                <div class="form__field-label">*</div>
                            </div>
                            <div class="form__field">
                                <div class="form__icon">
                                    <img class="form__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/gallery.svg" alt="">
                                </div>
                                <input class="form__input" type="file" placeholder="Выберите фотографию">
                                <img class="form__field-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/file.svg" alt="">
                            </div>
                            <div class="form__field">
                                <textarea class="form__textarea" name="" placeholder="Ваше сообщение"></textarea>
                            </div>
                        </div>
                        <div class="form__footer">
                            <button class="button button-secondary form__reg">Отправить отзыв</button>
                        </div>
                    </form>
                </div>
                <div class="modal modal--order" id="order">
                    <div class="modal__inner">
                        <div class="modal__swiper">
                            <div class="modal__swiper-container swiper-container js-order-swiper">
                                <div class="modal__swiper-wrapper swiper-wrapper">
                                    <div class="modal__swiper-slide swiper-slide">
                                        <div class="modal-card">
                                            <div class="modal-card__photo">
                                                <img class="modal-card__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/order-1.png" alt="">
                                            </div>
                                            <div class="modal-card__data">
                                                <img class="modal-card__data-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/logo-purple.svg" alt=""><span class="modal-card__data-title">Мастерская артист</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal__swiper-slide swiper-slide">
                                        <div class="modal-card">
                                            <div class="modal-card__photo">
                                                <img class="modal-card__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/order-1.png" alt="">
                                            </div>
                                            <div class="modal-card__data">
                                                <img class="modal-card__data-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/logo-purple.svg" alt=""><span class="modal-card__data-title">Мастерская артист</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal__swiper-slide swiper-slide">
                                        <div class="modal-card">
                                            <div class="modal-card__photo">
                                                <img class="modal-card__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/order-1.png" alt="">
                                            </div>
                                            <div class="modal-card__data">
                                                <img class="modal-card__data-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/logo-purple.svg" alt=""><span class="modal-card__data-title">Мастерская артист</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-controls modal__swiper-controls">
                                <div class="swiper-controls__pagination"></div>
                            </div>
                        </div>
                        <form class="form modal__form">
                            <div class="form__header"><span class="form__title">Форма заявки</span>
                            </div>
                            <div class="form__content">
                                <div class="form__field">
                                    <div class="form__icon">
                                        <img class="form__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/user.svg" alt="">
                                    </div>
                                    <input class="form__input" type="text" placeholder="Ваше имя">
                                </div>
                                <div class="form__field">
                                    <div class="form__icon">
                                        <img class="form__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/mobile.svg" alt="">
                                    </div>
                                    <input class="form__input" type="tel" placeholder="Номер телефона">
                                </div>
                                <div class="rebate form__rebate">
                                    <div class="rebate__inner">
                                        <div class="rebate__left"><span class="rebate__title">Скидка 10%</span>
                                            <h3 class="rebate__caption">для второго ребенка </h3>
                                        </div>
                                        <div class="rebate__right">
                                            <img class="rebate__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/rebate-2.png" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form__footer">
                                <button class="form__send"><span class="form__send-text">Отправить заявку</span>
                                    <img class="form__send-icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/plane.svg" alt="">
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <script src="<?php echo get_stylesheet_directory_uri()?>/static/js/main.min.js"></script>
        <script src="<?php echo get_stylesheet_directory_uri()?>/static/js/separate-js/common.js"></script>
    </body>

</html>