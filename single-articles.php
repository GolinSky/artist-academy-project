<?php
/**
 * The template for displaying all single posts and attachments
 *

 */
global $root_url_path;

get_header();


// Start the loop.
while ( have_posts() ) :
    the_post();
 $prev_id = get_previous_post()->ID;
 $next_id = get_next_post()->ID;
    // Include the single post content template.
    get_template_part( 'template-parts/article-detail', 'article-detail' );

/*    get_previous_post()->ID*/
?>

    <div class="article__aside">
        <div class="intrest article__intrest">
            <div class="intrest__header"><span class="intrest__title">Интересные статьи:</span>
                <a class="intrest__more" href="<?php get_page_url_in_menu('article');?>" title="Ко всем статьям">
                    <img class="intrest__more-img" src="<?php echo $root_url_path;?>/static/img/general/icons/intrest.svg" alt=""><span class="intrest__more-text">Ко всем статьям</span>
                </a>
            </div>
            <div class="intrest__content">

            <?php $reviews = new WP_Query(
            array(
                'post_type' => 'articles',
                'posts_per_page' => 3,
                'post_status'	   => 'publish',
                'orderby' => 'article_rating',
                'order' => 'ASC',
                'suppress_filters' => true
            )
        );  ?>

    <?php if ( $reviews->have_posts() ) : while ( $reviews->have_posts() ) : $reviews->the_post(); ?>
                <div class="intrest__section"><a class="intrest-card" href="<?php the_permalink();?>"><span class="intrest-card__title"><?php the_title();?></span><span class="intrest-card__date"><img class="intrest-card__date-img" src="<?php echo $root_url_path ?>/static/img/general/icons/calendar-gray.svg" alt=""><span class="intrest-card__date-text"><?php echo get_the_date( 'j.m.Y'); ?></span></span></a>
                </div>

    <?php endwhile; ?>
    <?php endif; ?>
            </div>
        </div>
    </div>
    </div>


    <div class="article__other">
        <div class="other">
            <div class="other__inner">
                <div class="other__header"><span class="other__title">Другие новости</span>
                    <div class="swiper-controls other__swiper-controls">
                        <div class="swiper-controls__actions">
                            <button class="swiper-controls__button swiper-controls__button--prev"></button>
                            <button class="swiper-controls__button swiper-controls__button--next"></button>
                        </div>
                    </div>
                </div>
                <div class="other__content">
                    <div class="other__swiper">
                        <div class="other__swiper-container swiper-container js-other-swiper">
                            <div class="other__swiper-wrapper swiper-wrapper">

                                <div class="other__swiper-slide swiper-slide">
                                    <div class="news-card">
                                        <div class="news-card__photo">
                                            <div class="news-card__photo-gradient"></div>
                                            <img class="news-card__img" src="<?php echo  the_post_thumbnail_url(); ?>" alt="">
                                        </div>
                                        <div class="news-card__data">
                                            <div class="news-card__date">
                                                <img class="news-card__icon" src="<?php echo $root_url_path;?>/static/img/general/icons/calendar-gray.svg" alt=""><span class="news-card__date-text"><?php echo get_the_date( 'j.m.Y'); ?></span>
                                            </div><a class="news-card__title" href="<?php the_permalink();?>"><?php the_title(); ?></a><a class="news-card__button" href="<?php the_permalink();?>">Читать далее</a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
    </div>


<?php
    // End of the loop.
endwhile;


get_footer();