<?php

    function cache_uri()
    {
        global $student_post_count;
        if($student_post_count!= -1 )
            $student_post_count = 8;

        global $root_url_path;
        $root_url_path = get_stylesheet_directory_uri();
        global $url;
        $url="https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        global $query1;

    }
    add_action( 'init', 'cache_uri' );

    add_theme_support( 'post-thumbnails' ); // для всех типов постов

    show_admin_bar( true );

    //add new post types
    require_once locate_template( '/functions/post/students_post.php' );
    require_once locate_template( '/functions/post/educator_post.php' );
    require_once locate_template( '/functions/post/review_post.php' );
    require_once locate_template( '/functions/post/news_post.php' );
    require_once locate_template( '/functions/post/award_post.php' );
    require_once locate_template( '/functions/post/galery_post.php' );
    require_once locate_template( '/functions/post/articles_post.php' );
    require_once locate_template( '/functions/post/language_preset_post.php' );
    require_once locate_template( '/functions/post/shedule_post.php' );
    require_once locate_template( '/functions/post/album_post.php' );
    require_once locate_template( '/functions/post/direction_post.php' );

    require_once locate_template( '/functions/sort/sort_post.php' );
    require_once locate_template( '/functions/page/page-helper.php' );
    require_once locate_template( '/functions/validation/validation_helper.php' );
    require_once locate_template( '/functions/editors/text-editor.php' );
    require_once locate_template( '/functions/meta/user-meta.php' );

    require_once locate_template('/functions/editors/admin-cleaner.php');

    add_image_size( 'article-size', 773, 434, true); // 220 pixels wide by 180 pixels tall, soft proportional crop mode

    function article_size( $sizes ) {
        return array_merge( $sizes, array(
            'article-size' => __( 'Article Thumbnail' ),
        ) );
    }

    function save_cookie($query_key, $query_data)
    {
        setcookie($query_key, $query_data, 3 * DAYS_IN_SECONDS, COOKIEPATH, COOKIE_DOMAIN );
    }

    function save_serialized_cookie($query_key, $query_data)
    {
        setcookie($query_key, serialize($query_data), 3 * DAYS_IN_SECONDS, COOKIEPATH, COOKIE_DOMAIN );
    }

    function get_serialized_cookie($query_key, $default_value)
    {
        return isset($_COOKIE[$query_key])
            ?unserialize($_COOKIE[$query_key])
            :$default_value;
    }

    function get_cookie($query_key, $default_value)
    {
        return isset($_COOKIE[$query_key])
            ?$_COOKIE[$query_key]
            :$default_value;
    }

    function clear_cookie($query_key)
    {
        unset($_COOKIE[$query_key]);
        setcookie($query_key, '', time() - (15 * 60));
    }



