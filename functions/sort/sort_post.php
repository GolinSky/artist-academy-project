<?php // add this function to functions.php this is your ajax action
add_action('wp_ajax_my_action', 'wp_ajax_filter_menu');
add_action('wp_ajax_nopriv_my_action', 'wp_ajax_filter_menu');

add_action('wp_ajax_group_action', 'get_filter_groups');
add_action('wp_ajax_nopriv_group_action', 'get_filter_groups');

function extract_numbers($string)
{
    preg_match_all('/([\d]+)/', $string, $match);
    return $match;
}
global $student_query;
function split_days($string)
{
    $match = explode('|', $string);
    $last_i = count($match) - 1;
    $remove = false;
    $index = 0;
    for($i = 0;$i<count($match)-1;$i++)
    {
        if($match[$i] == $match[$last_i])
        {
            $index = $i;
            $remove = true;
            break;
        }
    }
    if($remove)
    {
    /*    $match[$last_i] = '';
        $match[$index] = '';*/

        unset($match[$last_i]);
        unset($match[$index]);
        sort($match);
    }

    return $match;
}

function post_query($query_name, $default_value)
{
    $query_value = $_POST[$query_name];
    echo '\'' . get_query_value($query_value, $default_value) . '\'';
}

function return_post_query($query_name, $default_value)
{
    $query_value = $_POST[$query_name];

    return get_query_value($query_value, $default_value);
}

function get_query($query_name, $default_value)
{
    $query_value = $_GET[$query_name];
    echo '\'' . get_query_value($query_value, $default_value) . '\'';
}

function get_query_value($query_value, $default_value)
{
    if ($query_value && !empty($query_value)) {
        $value = $query_value;
    } else {
        $value = $default_value;
    }
    return $value;
}

function get_query_int_value($query_name, $default_value)
{
    $query_value = $_GET[$query_name];
    return query_int_value($query_value, $default_value);
}

function post_query_int_value($query_name, $default_value)
{
    $query_value = $_POST[$query_name];
    return query_int_value($query_value, $default_value);
}

function query_int_value($query_value, $default_value)
{
    return $query_value && !empty($query_value)
        ? $query_value
        : $default_value;
}

function wp_ajax_filter_menu()
{
    global $root_url_path;
    $use_cache = (bool)$_POST['use_cashe'];
    $hair_query = $_POST['hair_query'];

    $gender_query = $_POST['gender_query'];


    $sort_query = return_post_query('sort_query', 'title');
    $orderBy = 'meta_value_num';

    $amount_query = post_query_int_value('amount_query', 8);

    $age_test = $_POST['age_query'];

    if($use_cache)
    {
        $age_test = get_cookie('age_query', $age_test);
        $_POST['age_query'] = $age_test;
        $gender_query = get_cookie('gender_query', $gender_query);
        $_POST['gender_query'] = $gender_query;
        $sort_query = get_cookie('sort_query', $sort_query);
        $_POST['sort_query'] = $sort_query;
        $orderBy = get_cookie('orderBy', $orderBy);
        $_POST['orderBy'] = $orderBy;
    }
    else
    {
        save_cookie('age_query', $age_test);
        save_cookie('gender_query', $gender_query);
        save_cookie('sort_query', $sort_query);
        save_cookie('orderBy', $orderBy);
    }
    $sort_radio_value = $sort_query;
    if ($sort_query == 'title') {
        $orderBy = 'title';
        $sort_query = 'student_age';
    }

    $gender_radio_value = $gender_query;
    if ($gender_query == 'all')
    {
        $gender_query = array('boy:Мальчик', 'girl:Девочка');
    }

    if ($age_test && !empty($age_test))
    {
        $age_array = extract_numbers($age_test)[0];
        $min_age_value = $age_array[0];
        $max_age_value = $age_array[1];
        for ($i = 1; $i < count($age_array); $i++)
        {
            if ($i % 2 != 0) // get max
            {
                if ($max_age_value < $age_array[$i])
                {
                    $max_age_value = $age_array[$i];
                }
            }
            else // get min
            {
                if ($min_age_value > $age_array[$i])
                {
                    $min_age_value = $age_array[$i];
                }
            }
        }
        $final_age_filter = array($min_age_value, $max_age_value);
    }
    else
    {
        $final_age_filter = 'array(18,150)';
        $final_age_filter = extract_numbers($final_age_filter)[0];
    }

    if($use_cache)
    {
        //get cache
        $amount_query = get_cookie('amount_query', $amount_query);
        $_POST['amount_query'] = $amount_query;
        $hair_query = get_cookie('hair_query', $hair_query);
        $_POST['hair_query'] = $hair_query;
    }
    else
    {
        //save cache
        save_cookie('amount_query', $amount_query);
        save_cookie('hair_query', $hair_query);
    }

    $main_query = array(
        'post_type' => 'students',
        'posts_per_page' => $amount_query,
        'post_status' => 'publish',
        'meta_query' => array(
            'relation' => 'AND', // both of below conditions must match
            'age_query' => array(
                'key' => 'student_age',
                'order' => 'desc',
                'value' => $final_age_filter,
                'type' => 'numeric',
                'compare' => 'BETWEEN'
            ),
            'gender_query' => array(
                'key' => 'gender_type',
                'value' => $gender_query,
                'compare' => 'IN'
            ),
            'hair_query' => array(
                'key' => 'hair_color',
                'value' => $hair_query,
                'compare' => 'IN'
            )
        ),
        'meta_key' => $sort_query,
        'orderby' => $orderBy,
        'order' => 'ASC'
    );
    $filteroption = new WP_Query($main_query);

    if ($filteroption->have_posts()) :
        while ($filteroption->have_posts())
            : $filteroption->the_post();
            ?>
            <div class="students__column column">
                <a  class="students-card" onclick="send_data('<?php the_permalink(); ?>')" href="#">
                    <div class="students-card__overlay"></div>
                    <div class="students-card__header">
                                            <span class="students-card__link" href="" )">
                                                <h4 class="students-card__title"><?php the_title(); ?>  </h4>
                                            </span>
                        <span class="students-card__age"><?php the_excerpt(); ?> </span>
                    </div>
                    <div class="students-card__content">
                        <img class="students-card__img" src="<?php the_post_thumbnail_url('full'); ?>" alt="">
                    </div>
                    <div class="students-card__footer"><span class="students-card__more">Смотреть<img
                                    class="students-card__more-cursor"
                                    src="<?php echo $root_url_path ?>/static/img/general/icons/arrow-orange.png" alt=""></span>
                    </div>
                </a>
            </div>


        <?php
        endwhile;
        wp_reset_query(); ?>

        <script>

            setUpGenderRadio( "<?php echo $gender_radio_value; ?>");
            setUpSortingRadio("<?php echo $sort_radio_value; ?>");
            setUpHairSelection("<?php echo $hair_query; ?>");
            setUpAgeCheckBox("<?php echo $age_test; ?>");

            function setUpAgeCheckBox(value) {
                var inputs =$(".students__box-dropdown-chekbox input[type=checkbox]");
                console.log('setUpAgeCheckBox: '+inputs.length);
                var splitValues = value.split('array');
                for(var i = 0;i<inputs.length;i++)
                {
                    for(var j = 0;j<splitValues.length;j++)
                    {
                        var item = 'array'+splitValues[j];
                       if(inputs[i].value == item)
                       {
                           inputs[i].checked = true;
                           inputs[i].closest('li').classList.add('checked');
                           inputs[i].closest('div').classList.add('checked');

                       }
                    }
                }
            }

            function setUpHairSelection(selected_value) {
                document.getElementById("hair_selection").value = selected_value;
            }
            
            function change_post_by_age(value) {
                var age_query = value;
                jQuery.ajax({
                    async: true,
                    url: '<?php echo admin_url('admin-ajax.php');?>',
                    data: {
                        'action': 'my_action',
                        'age_query': age_query,
                        'hair_query': <?php post_query('hair_query', 'array(Блондин, Рыжий, Шатен, Русый, Брюнет)'); ?>,
                        'gender_query': <?php post_query('gender_query', 'all');  ?>,
                        'sort_query': <?php post_query('sort_query', 'title'); ?>,
                        'amount_query': <?php echo $amount_query; ?>,

                    },
                    type: 'POST',
                    success: function (data) {
                        jQuery("#load_html").html(data);
                    }
                });

            }

            function change_post_by_gender(value) {
                var gender_query = value;
                jQuery.ajax({
                    async: true,
                    url: '<?php echo admin_url('admin-ajax.php');?>',
                    data: {
                        'action': 'my_action',
                        'gender_query': gender_query,
                        'hair_query': <?php post_query('hair_query', 'array(Блондин, Рыжий, Шатен, Русый, Брюнет)'); ?>,
                        'age_query': <?php post_query('age_query', 'array(0,150)'); ?>,
                        'sort_query': <?php post_query('sort_query', 'title'); ?>,
                        'amount_query': <?php echo $amount_query; ?>
                    },
                    type: 'POST',
                    success: function (data) {
                        jQuery("#load_html").html(data);
                    }
                });
            }

            function change_post_by_hair(value) {
                var hair_query = value;
                jQuery.ajax({
                    async: true,
                    url: '<?php echo admin_url('admin-ajax.php');?>',
                    data: {
                        'action': 'my_action',
                        'gender_query': <?php  post_query('gender_query', 'all'); ?>,
                        'hair_query': hair_query,
                        'age_query': <?php post_query('age_query', 'array(0,150)'); ?>,
                        'sort_query': <?php post_query('sort_query', 'title'); ?>,
                        'amount_query': <?php echo $amount_query; ?>
                    },
                    type: 'POST',
                    success: function (data) {
                        jQuery("#load_html").html(data);
                    }
                });
            }

            function change_post_by_sorting(value) {
                var sort_query = value;
                jQuery.ajax({
                    async: true,
                    url: '<?php echo admin_url('admin-ajax.php');?>',
                    data: {
                        'action': 'my_action',
                        'gender_query': <?php post_query('gender_query', 'all') ?>,
                        'hair_query': <?php post_query('hair_query', 'array(Блондин, Рыжий, Шатен, Русый, Брюнет)'); ?>,
                        'age_query': <?php post_query('age_query', 'array(0,150)'); ?>,
                        'amount_query': <?php echo $amount_query; ?>,
                        'sort_query': sort_query
                    },
                    type: 'POST',
                    success: function (data) {
                        jQuery("#load_html").html(data);
                    }
                });
            }
            function send_data(link) {


                console.log(<?php post_query('amount_query', 'all') ?>);
                console.log(<?php post_query('gender_query', 'all') ?>);
                console.log(<?php post_query('hair_query', 'array(Блондин, Рыжий, Шатен, Русый, Брюнет)'); ?>);
                console.log(<?php post_query('age_query', 'array(0,150)'); ?>);
                console.log(<?php post_query('sort_query', 'title'); ?>);
                jQuery.ajax({
                    async: true,
                    url: '<?php echo admin_url('admin-ajax.php');?>',
                    data: {
                        'action': 'my_action',
                        'amount_query': <?php echo $amount_query; ?>,
                        'gender_query': <?php post_query('gender_query', 'all') ?>,
                        'hair_query': <?php post_query('hair_query', 'array(Блондин, Рыжий, Шатен, Русый, Брюнет)'); ?>,
                        'age_query': <?php post_query('age_query', 'array(0,150)'); ?>,
                        'sort_query': <?php post_query('sort_query', 'title'); ?>
                    },
                    type: 'POST',
                    success: function (data) {
                       // save_cookie(data);
                        window.location.replace(link);
                    }
                });
            }

            function send_data_get(link) {

                console.log(<?php post_query('amount_query', 'all') ?>);
                console.log(<?php post_query('gender_query', 'all') ?>);
                console.log(<?php post_query('hair_query', 'array(Блондин, Рыжий, Шатен, Русый, Брюнет)'); ?>);
                console.log(<?php post_query('age_query', 'array(0,150)'); ?>);
                console.log(<?php post_query('sort_query', 'title'); ?>);
                jQuery.ajax({
                    async: true,
                    url: '<?php echo admin_url('admin-ajax.php');?>',
                    data: {
                        'action': 'my_action',
                        'amount_query': <?php echo $amount_query; ?>,
                        'gender_query': <?php post_query('gender_query', 'all') ?>,
                        'hair_query': <?php post_query('hair_query', 'array(Блондин, Рыжий, Шатен, Русый, Брюнет)'); ?>,
                        'age_query': <?php post_query('age_query', 'array(0,150)'); ?>,
                        'sort_query': <?php post_query('sort_query', 'title'); ?>
                    },
                    type: 'GET',
                    success: function (data) {
                    }
                });
                window.location.replace(link);

            }

            function change_post_by_amount(value) {
                var amount_query = value;
                jQuery.ajax({
                    async: true,
                    url: '<?php echo admin_url('admin-ajax.php');?>',
                    data: {
                        'action': 'my_action',
                        'amount_query': amount_query,
                        'gender_query': <?php post_query('gender_query', 'all') ?>,
                        'hair_query': <?php post_query('hair_query', 'array(Блондин, Рыжий, Шатен, Русый, Брюнет)'); ?>,
                        'age_query': <?php post_query('age_query', 'array(0,150)'); ?>,
                        'sort_query': <?php post_query('sort_query', 'title'); ?>
                    },
                    type: 'POST',
                    success: function (data) {
                        jQuery("#load_html").html(data);
                    }
                });
            }

            function gender_filter_click(value) {
                if (value == '<?php echo $gender_query?>') {
                    value = 'all';
                    setUpGenderRadio(value);
                }
                change_post_by_gender(value);
            }

            function setUpGenderRadio(radioValue)
            {
                console.log("setUpGenderRadio: "+radioValue);
                switch (radioValue) {
                    case 'all':
                    {
                        document.getElementById("girl_radio").classList.remove("checked");
                        document.getElementById("boy_radio").classList.remove('checked');
                        break;
                    }
                    case 'boy:Мальчик':
                    {
                        document.getElementById("boy_radio").classList.add('checked');
                        break;
                    }
                    case 'girl:Девочка':
                    {
                        document.getElementById("girl_radio").classList.add("checked");
                        break;
                    }
                }
            }

            function setUpSortingRadio(radioValue)
            {
                console.log("setUpSortingRadio: "+radioValue);
                switch (radioValue) {

                    case 'student_age':
                    {
                        document.getElementById("age_radio").classList.add('checked');
                        break;
                    }
                    case 'rating':
                    {
                        document.getElementById("rating_radio").classList.add("checked");
                        break;
                    }
                }
            }

            function age_filter_click(value, checked) {
                var inputs = $(".students__box-dropdown-chekbox input");
                var allValues = '';
                for (var i = 0; i < inputs.length; i++) {
                    if (inputs[i].checked) {
                        allValues += inputs[i].value;
                    }
                }
                if (allValues == '') {
                    allValues = 'array(0,150)';
                }
                change_post_by_age(allValues);

            }

            function hair_filter_click(value) {
                change_post_by_hair(value);
            }

            function sort_filter_click(value) {
                change_post_by_sorting(value);
            }

            function count_filter_click() {
                change_post_by_amount(-1);//make all post view
            }
            
            function set_count)() {
                document.getElementById("load_more_div").style.display = 'none';
            }

            function change_post_by_amount(value) {
                var amount_query = value;
                jQuery.ajax({
                    async: true,
                    url: '<?php echo admin_url('admin-ajax.php');?>',
                    data: {
                        'action': 'my_action',
                        'amount_query': amount_query,
                        'gender_query': <?php post_query('gender_query', 'all') ?>,
                        'hair_query': <?php post_query('hair_query', 'array(Блондин, Рыжий, Шатен, Русый, Брюнет)'); ?>,
                        'age_query': <?php post_query('age_query', 'array(0,150)'); ?>,
                        'sort_query': <?php post_query('sort_query', 'title'); ?>

                    },
                    type: 'POST',
                    success: function (data) {
                        jQuery("#load_html").html(data);
                    }
                });
            }


        </script>
    <?php
    else:
        echo '<div class="main__container container"><h3 class="main__title">Записи не найдены!</h3></div>';
        //return false;
    endif;
    die;
}

function get_groups()
{
    global $root_url_path;
    $reviews = new WP_Query(
                                array(
                                    'post_type' => 'groups',
                                    'posts_per_page' => -1,
                                    'post_status'	   => 'publish',
                                    'orderby' => 'post_date',
                                    'order' => 'ASC',
                                    'suppress_filters' => true
                                )
                            );
                            if ( $reviews->have_posts() ) : while ( $reviews->have_posts() ) : $reviews->the_post(); ?>

                                <div class="leasons__row">
                                    <div class="leasons__header">
                                        <div class="leasons__grid">
                                            <div class="leasons__column">
                                                <h3 class="leasons__title">Группа “<?php the_title();?>”</h3>
                                            </div>
                                            <div class="leasons__column"><span class="leasons__wrap leasons__wrap--age"><?php echo get_field('age_category');?></span>
                                            </div>
                                            <div class="leasons__column"><span class="leasons__wrap leasons__wrap--days">

                                            <ul class="leasons__days-list">
                                                <?php
                                                $days = get_field('days_of_the_week');

                                                for($i = 0;$i<count($days);$i++)
                                                {
                                                    echo '<li class="leasons__days-item"><span class="leasons__days-text"> '. $days[$i] .'</span></li>';
                                                }


                                                ?>

                                            </ul>
                                                    </span>
                                            </div>
                                            <div class="leasons__column"><span class="leasons__wrap leasons__wrap--price">от <?php echo get_field('group_price'); ?><i class="fa fa-rub"></i></span>
                                                <div class="leasons__arrow"><i class="fa fa-angle-down"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="leasons__dropdown">
                                        <div class="leasons__dropdown-inner">
                                            <div class="leasons__dropdown-left">
                                                <div class="work-card leasons__work-card"><span class="work-card__title">Время занятий:</span><span class="work-card__time"><?php echo get_field('group_time_start'); ?> -<?php echo get_field('group_time_end'); ?></span>
                                                    <img class="work-card__img" src="<?php echo $root_url_path;?>/static/img/general/icons/clock-small.svg" alt="">
                                                </div>
                                            </div>
                                            <div class="leasons__dropdown-right">
                                                <div class="leasons__dropdown-grid">
                                                    <?php
                                                    $educators = get_field('coaches');
                                                    for($i = 0;$i<count($educators);$i++)
                                                    {

                                                        $id = $educators[$i]->ID;
                                                        $spec = get_field("specialization", $id);
                                                        $imgURL = MultiPostThumbnails::get_post_thumbnail_url('educator', 'secondary-image', $id, 'large');

                                                        echo '<div class="leasons__dropdown-column">
                                                        <div class="coach-card-small leasons__coach-card-small">
                                                            <div class="coach-card-small__photo">
                                                                <img class="coach-card-small__img" src="' . $imgURL .'" alt="" height="41" width="41">
                                                            </div>
                                                                <div class="coach-card-small__data"><span class="coach-card-small__title">'. $educators[$i]->post_title .'</span><span class="coach-card-small__position">'. $spec .'</span>
                                                            </div>
                                                        </div>
                                                    </div>';
                                                    }

                                                    ?>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="order leasons__order">
                                            <h3 class="order__title">Остались вопросы?   <span> Звоните!</span></h3>
                                            <form class="order__form" action="#">
                                                <div class="order__form-field">
                                                    <input class="order__form-input" type="text" placeholder="Ваше имя">
                                                </div>
                                                <div class="order__form-field">
                                                    <input class="order__form-input" type="text" placeholder="Номер телефона">
                                                </div>
                                                <button class="button button-secondary order__form-send">Оставить заявку</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            <?php endwhile; ?>
                            <?php endif;
}

function echo_days_elements($days)
{
    if(count($days) != 0)
    {
        for($i = 0;$i<count($days);$i++)
        {
            echo $days[$i] . '|';
        }
    }
}

function get_filter_groups()
{
    global $root_url_path;
    $days_query = split_days($_POST['day_query']);
    $sequence_query = split_days($_POST['sequence_query']);
    ?>
    <script>
        function sort_by_day(value) {
            var day_query =  "<?php echo_days_elements($days_query); ?>" + value;
            jQuery.ajax({
                async:true,
                url: '<?php echo admin_url( 'admin-ajax.php' );?>',
                data: {
                    'action' : 'group_action',
                    'day_query': day_query,
                    'sequence_query': "<?php echo_days_elements($sequence_query); ?>",
                },
                type: 'POST',
                success: function(data) {
                    jQuery("#load_container").html(data);
                }
            });
        }
        function sort_by_sequence(value) {
            var sequence_query = "<?php echo_days_elements($sequence_query); ?>" + value;
            jQuery.ajax({
                async:true,
                url: '<?php echo admin_url( 'admin-ajax.php' );?>',
                data: {
                    'action' : 'group_action',
                    'sequence_query': sequence_query,
                    'day_query': "<?php echo_days_elements($days_query); ?>"
                },
                type: 'POST',
                success: function(data) {
                    jQuery("#load_container").html(data);
                }
            });
        }
    </script>
  <?php
    $reviews = new WP_Query(
            array(
                'post_type' => 'groups',
                'posts_per_page' => -1,
                'post_status' => 'publish',
                'orderby' => 'post_date',
                'order' => 'ASC',
                'suppress_filters' => true
            ));
    $count_posts = wp_count_posts( 'groups' )->publish;
    $current_post_count = 0;
    $target_query_id[] = array();
    $nothing_found = false;
    if ( $reviews->have_posts() ) : while ( $reviews->have_posts() ) : $reviews->the_post(); ?>

        <?php
        $find_equals_days = false;
        $find_equals_age = false;
        $count_days = count($days_query);
        $count_price = count($sequence_query);
        $days = get_field('days_of_the_week');

        if($count_days != 0)
        {
            if($count_days == 1)
            {
                if($days_query[0] == '')
                {
                    $find_equals_days = true;
                }
            }
            for($i = 0;$i<count($days);$i++)
            {
                for($j = 0;$j<count($days_query);$j++)
                {
                    if($days[$i] == $days_query[$j])
                    {
                        $find_equals_days = true;
                        break;
                    }
                }
                if($find_equals_days)
                {
                    break;
                }
            }
        }
        else
        {
            $find_equals_days = true;
        }

        $price = get_field('age_category');
        if($count_price != 0)
        {
            if($count_price == 1)
            {
                if($sequence_query[0] == '')
                {
                    $find_equals_age = true;
                }
            }
            for($i = 0;$i<count($sequence_query);$i++)
            {
                if($price == $sequence_query[$i])
                {
                    $find_equals_age = true;
                    break;
                }
            }
        }
        else
        {
            $find_equals_age = true;
        }

        $count_posts--;
        if(!$find_equals_age || !$find_equals_days)
        {
            if($find_equals_age || $find_equals_days)
            {
                array_push($target_query_id, get_the_ID());
            }

            if($count_posts == 0 && $current_post_count == 0)
            {
                echo '<div class="main__container container"><h3 class="main__title">По вашему запросу ничего не найдено!</h3></div>';
                echo '<div class="main__container container"><center><h4 class="leasons__heading">Возможно вы искали это</h4></center></div><br>';
                $nothing_found = true;
            }
            continue;
        }
        else
        {
            $current_post_count++;
        }

        ?>


        <div class="leasons__row">
            <div class="leasons__header">
                <div class="leasons__grid">
                    <div class="leasons__column">
                        <h3 class="leasons__title">Группа “<?php the_title();?>”</h3>
                    </div>
                    <div class="leasons__column"><span class="leasons__wrap leasons__wrap--age"><?php echo get_field('age_category');?></span>
                    </div>
                    <div class="leasons__column"><span class="leasons__wrap leasons__wrap--days">

                                            <ul class="leasons__days-list">
                                                <?php

                                                for($i = 0;$i<count($days);$i++)
                                                {
                                                    echo '<li class="leasons__days-item"><span class="leasons__days-text"> '. $days[$i] .'</span></li>';
                                                }

                                                ?>

                                            </ul>
                                                    </span>
                    </div>
                    <div class="leasons__column"><span class="leasons__wrap leasons__wrap--price">от <?php echo get_field('group_price'); ?><i class="fa fa-rub"></i></span>
                        <div class="leasons__arrow"><i class="fa fa-angle-down"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="leasons__dropdown">
                <div class="leasons__dropdown-inner">
                    <div class="leasons__dropdown-left">
                        <div class="work-card leasons__work-card"><span class="work-card__title">Время занятий:</span><span class="work-card__time"><?php echo get_field('group_time_start'); ?> -<?php echo get_field('group_time_end'); ?></span>
                            <img class="work-card__img" src="<?php echo $root_url_path;?>/static/img/general/icons/clock-small.svg" alt="">
                        </div>
                    </div>
                    <div class="leasons__dropdown-right">
                        <div class="leasons__dropdown-grid">
                            <?php
                            $educators = get_field('coaches');
                            for($i = 0;$i<count($educators);$i++)
                            {

                                $id = $educators[$i]->ID;
                                $spec = get_field("specialization", $id);
                                $imgURL = MultiPostThumbnails::get_post_thumbnail_url('educator', 'secondary-image', $id, 'large');

                                echo '<div class="leasons__dropdown-column">
                                                        <div class="coach-card-small leasons__coach-card-small">
                                                            <div class="coach-card-small__photo">
                                                                <img class="coach-card-small__img" src="' . $imgURL .'" alt="" height="41" width="41">
                                                            </div>
                                                                <div class="coach-card-small__data"><span class="coach-card-small__title">'. $educators[$i]->post_title .'</span><span class="coach-card-small__position">'. $spec .'</span>
                                                            </div>
                                                        </div>
                                                    </div>';
                            }

                            ?>

                        </div>
                    </div>
                </div>
                <div class="order leasons__order">
                    <h3 class="order__title">Остались вопросы?   <span> Звоните!</span></h3>
                    <form class="order__form" action="#">
                        <div class="order__form-field">
                            <input class="order__form-input" type="text" placeholder="Ваше имя">
                        </div>
                        <div class="order__form-field">
                            <input class="order__form-input" type="text" placeholder="Номер телефона">
                        </div>
                        <button class="button button-secondary order__form-send">Оставить заявку</button>
                    </form>
                </div>
            </div>
        </div>

    <?php endwhile;
    if($nothing_found)
    {
        while ( $reviews->have_posts() ) : $reviews->the_post();?>

            <?php
            $days = get_field('days_of_the_week');

            $find_equals = false;
            foreach ($target_query_id as $item)
            {
                if(get_the_ID() == $item)
                {
                    $find_equals = true;
                    break;
                }
            }
            if(!$find_equals) continue;

            ?>

            <div class="leasons__row">
                <div class="leasons__header">
                    <div class="leasons__grid">
                        <div class="leasons__column">
                            <h3 class="leasons__title">Группа “<?php the_title();?>”</h3>
                        </div>
                        <div class="leasons__column"><span class="leasons__wrap leasons__wrap--age"><?php echo get_field('age_category');?></span>
                        </div>
                        <div class="leasons__column"><span class="leasons__wrap leasons__wrap--days">

                                            <ul class="leasons__days-list">
                                                <?php

                                                for($i = 0;$i<count($days);$i++)
                                                {
                                                    echo '<li class="leasons__days-item"><span class="leasons__days-text"> '. $days[$i] .'</span></li>';
                                                }

                                                ?>

                                            </ul>
                                                    </span>
                        </div>
                        <div class="leasons__column"><span class="leasons__wrap leasons__wrap--price">от <?php echo get_field('group_price'); ?><i class="fa fa-rub"></i></span>
                            <div class="leasons__arrow"><i class="fa fa-angle-down"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="leasons__dropdown">
                    <div class="leasons__dropdown-inner">
                        <div class="leasons__dropdown-left">
                            <div class="work-card leasons__work-card"><span class="work-card__title">Время занятий:</span><span class="work-card__time"><?php echo get_field('group_time_start'); ?> -<?php echo get_field('group_time_end'); ?></span>
                                <img class="work-card__img" src="<?php echo $root_url_path;?>/static/img/general/icons/clock-small.svg" alt="">
                            </div>
                        </div>
                        <div class="leasons__dropdown-right">
                            <div class="leasons__dropdown-grid">
                                <?php
                                $educators = get_field('coaches');
                                for($i = 0;$i<count($educators);$i++)
                                {

                                    $id = $educators[$i]->ID;
                                    $spec = get_field("specialization", $id);
                                    $imgURL = MultiPostThumbnails::get_post_thumbnail_url('educator', 'secondary-image', $id, 'large');

                                    echo '<div class="leasons__dropdown-column">
                                                        <div class="coach-card-small leasons__coach-card-small">
                                                            <div class="coach-card-small__photo">
                                                                <img class="coach-card-small__img" src="' . $imgURL .'" alt="" height="41" width="41">
                                                            </div>
                                                                <div class="coach-card-small__data"><span class="coach-card-small__title">'. $educators[$i]->post_title .'</span><span class="coach-card-small__position">'. $spec .'</span>
                                                            </div>
                                                        </div>
                                                    </div>';
                                }

                                ?>

                            </div>
                        </div>
                    </div>
                    <div class="order leasons__order">
                        <h3 class="order__title">Остались вопросы?   <span> Звоните!</span></h3>
                        <form class="order__form" action="#">
                            <div class="order__form-field">
                                <input class="order__form-input" type="text" placeholder="Ваше имя">
                            </div>
                            <div class="order__form-field">
                                <input class="order__form-input" type="text" placeholder="Номер телефона">
                            </div>
                            <button class="button button-secondary order__form-send">Оставить заявку</button>
                        </form>
                    </div>
                </div>
            </div>

        <?php endwhile;

    }

        endif;
       die;
}




