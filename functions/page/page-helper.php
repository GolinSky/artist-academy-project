<?php

    function get_page_url_in_menu($slug, $current_url)
    {
        $target_url = get_permalink(get_page_by_path($slug));
        if($current_url != $target_url)
        {
            echo $target_url;
        }
        else
        {
            echo '#';
        }
    }

    function get_closed_url_in_menu($slug, $current_url)
    {
        if(!current_user_can('administrator'))
        {
            echo '#';
            return;
        }

        $target_url = get_permalink(get_page_by_path($slug));
        if($current_url != $target_url)
        {
            echo $target_url;
        }
        else
        {
            echo '#';
        }
    }

    function get_student_url_in_menu($current_url)
    {
        $target_url = get_permalink(get_page_by_path('student'));
        if($current_url != $target_url)
        {
            clear_cookie('amount_query');
            clear_cookie('age_query');
            clear_cookie('gender_query');
            clear_cookie('hair_query');
            clear_cookie('sort_query');
            clear_cookie('orderBy');
            echo $target_url;
        }
        else
        {
            echo '#';
        }
    }

    function get_url_by_page_slug($slug)
    {
        echo get_permalink(get_page_by_path($slug));
    }

    function load_galery_by_acf($field_name, $post_id)
    {
        $student_galery_mini = get_field_object($field_name, $post_id);
        $arr = $student_galery_mini['value'];
        preg_match_all('/<img[^>]+>/i',$arr, $result);

        $img = array();

        $img_counter = 0;

        foreach( $result[0] as $img_tag)
        {
            preg_match_all('/(src)=("[^"]*")/i',$img_tag, $img[$img_counter]);
            $img_counter++;
        }
        return $img;
    }

    if (class_exists('MultiPostThumbnails')) {

        new MultiPostThumbnails(array(
            'label' => 'Secondary Image',
            'id' => 'secondary-image',
            'post_type' => 'educator'
        ) );

    }

    add_filter( 'excerpt_length', function(){
        return 14;
    } );
    add_filter('excerpt_more', function($more) {
        return '...';
    });

