
// A $( document ).ready() block.
$( document ).ready(function() {
    var allInputs = $( ":input" );
    console.log( "ready! ->" + allInputs.length);

    allInputs.each(function(index)
    {
        $(this).prop('invalid', false);

    });
});

function Form1Click(path) {
   // alert('Form1Click method called');
    var name = $("#first-form input[name=applicant_nrequiredame]").val();
    var number = $("#first-form input[name=applicant_numrequiredber]").val();
    var target = 'Форма Заявки';

   // alert('Form1Click method in progress ' . name);
   // alert('Form1Click method in progress ' . number);

    $.ajax({
        url: path + '/functions/form/formNumberNameHandler.php',
        method: 'get',


        data: {
            name: name,
            number: number,
            target: target
        },
        success: function (response) {
          //  alert(response);
            //$('#submit-ajax').html(response);
        }
    });
  //  alert('Form1Click method finished');
}

function Form1ClickCallBack( path  ) {


    var valid = $("#first-form input");
    for(var i = 0;i<valid.length;i++)
    {
        if(!valid[i].checkValidity())
        {
            valid[i].closest('div').classList.add('invalid');
            return false;
        }
        else
        {
            valid[i].closest('div').classList.remove('invalid');
        }
    }

    Form1Click(path);
    $("#first-form")[0].reset();
    $.fancybox.close();
    return false;



}



