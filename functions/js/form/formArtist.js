

function ArtistFormAjax(path, student_name)
{
    var name = $("#artist_form input[name=artist_name]").val();
    var mail = $("#artist_form input[name=artist_mail]").val();
    var number = $("#artist_form input[name=artist_phone]").val();

    var target = 'Форма приглашения';

    $.ajax({
        url: path + '/functions/form/formArtistHandler.php',
        method: 'get',

        data: {
            name: name,
            mail: mail,
            number: number,
            student_name: student_name,
            target: target
        },
        success: function (response) {
            //  alert(response);
            //$('#submit-ajax').html(response);
        }
    });
      console.log('ArtistFormAjax method finished');
}



function OnClickArtistForm( path , student_name )
{
    var valid = $("#artist_form input");
    for(var i = 0;i<valid.length;i++)
    {
        if(!valid[i].checkValidity())
        {
            valid[i].closest('div').classList.add('invalid');
            return false;
        }
        else
        {
            valid[i].closest('div').classList.remove('invalid');
        }
    }

    ArtistFormAjax(path, student_name);
    $("#artist_form")[0].reset();
    $.fancybox.close();
    return false;
}