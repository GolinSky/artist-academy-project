<?php
function check_for_spam($checked_value)
{
    $spam_list = array(
        'porn',
        'http',
        '<',
        '>',
        'sex'
    );

    foreach ($checked_value as $request_datum) {
        foreach ($spam_list as $spam_preset) {
            if (strpos($request_datum, $spam_preset) !== false) {
                return false;
            }
        }
    }
    return true;
}