<?php

    class DataBaseController
    {
        public $name_table = 'wp_students'; // by default if Init Method'll not be called

        function __construct()
        {
            $this->Init();
        }

        //init name of table
        function Init()
        {
            global $wpdb;
            $this->name_table = $wpdb->prefix . 'students';
        }

        public function InsertDataBase(array $values, array $types)
        {
            global $wpdb;
            $wpdb->insert(
                $this->name_table,
                $values,
                $types
            );
            echo "\ninsert_custom_table";
        }

        public function UpdateDataBase()
        {
            echo 'UpdateDataBase';
        }

        function DeleteFromDataBase()
        {

        }

        function Get_by_name( $name )
        {
            global $wpdb;
            $results = $wpdb->get_results( $wpdb->prepare('SELECT * FROM '.$this->name_table.' WHERE name = %s', $name) );
            return $results;
        }

        function Get_by_id( $id )
        {
            global $wpdb;
            $row = $wpdb->get_row( $wpdb->prepare('SELECT * FROM '.$this->name_table.' WHERE id = %d', $id) );
            return $row;
        }

        function Get_all_table()
        {
            global $wpdb;
            $row = $wpdb->get_results( 'SELECT * FROM '.$this->name_table );
            return $row;
        }

    }