    <?php

    function create_student_tables()
    {


        global $wpdb;


        $table_name = $wpdb->prefix . 'students';


        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") == $table_name)
        {
            //echo '<br>database already exist';
            return;
        }
        $sql = "CREATE TABLE $table_name (
			id int(11) NOT NULL AUTO_INCREMENT,
			name varchar(255) DEFAULT NULL,
			surname varchar(255) DEFAULT NULL,
			email varchar(255) DEFAULT NULL,
            child_name varchar(255) DEFAULT NULL,
			child_surname varchar(255) DEFAULT NULL,
            child_age int(3) DEFAULT NULL,
			UNIQUE KEY id (id)
	);";
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
        // префикс текущего сайта
        echo $wpdb->prefix . 'students ';
    }
    create_student_tables();

    function insert_custom_table()
    {
        global $wpdb;
        $wpdb->insert(
            $wpdb->prefix . 'students',
            array(
                'id' => 2,
                'name' => 'Александр',
                'surname' => 'Голинский',
                'child_name' => 'Андрей',
                'child_surname' => 'Перекитный',
                'child_age' => 12
            ),
            array(
                '%d',
                '%s',
                '%s',
                '%s',
                '%s',
                '%d'
            )
        );
        echo "\ninsert_custom_table";
    }