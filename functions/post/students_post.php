<?php
    add_action( 'init', 'students_register_post_type_init' ); // Использовать функцию только внутри хука init

    function students_register_post_type_init() {
        $labels = array(
            'name' => 'Студент',
            'singular_name' => 'Студент', // админ панель Добавить->Функцию
            'add_new' => 'Добавить Студента',
            'add_new_item' => 'Добавить нового Студента', // заголовок тега <title>
            'edit_item' => 'Редактировать Студента',
            'new_item' => 'Новый Студент',
            'all_items' => 'Студенты',
            'view_item' => 'Просмотр Студентов на сайте',
            'search_items' => 'Искать Студентов',
            'not_found' =>  'Студенты не найдено.',
            'not_found_in_trash' => 'В корзине нет Студентов.',
            'menu_name' => 'Студенты' // ссылка в меню в админке
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'show_ui' => true, // показывать интерфейс в админке
            'has_archive' => true,
            'show_in_menu' => 'custom-post-type-slug',
            //'menu_icon' => get_stylesheet_directory_uri() .'/img/function_icon.png', // иконка в меню
            'menu_position' => 20, // порядок в меню
            'supports' => array( 'title', 'editor', 'comments', 'author', 'thumbnail')
        );
        register_post_type('students', $args);
    }

    add_filter( 'post_updated_messages', 'students_post_type_messages' );

    function students_post_type_messages( $messages ) {
        global $post, $post_ID;

        $messages['students'] = array( // functions - название созданного нами типа записей
            0 => '', // Данный индекс не используется.
            1 => sprintf( 'Функция обновлена. <a href="%s">Просмотр</a>', esc_url( get_permalink($post_ID) ) ),
            2 => 'Параметр обновлён.',
            3 => 'Параметр удалён.',
            4 => 'Функция обновлена',
            5 => isset($_GET['revision']) ? sprintf( 'Функция восстановлена из редакции: %s', wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
            6 => sprintf( 'Функция опубликована на сайте. <a href="%s">Просмотр</a>', esc_url( get_permalink($post_ID) ) ),
            7 => 'Функция сохранена.',
            8 => sprintf( 'Отправлено на проверку. <a target="_blank" href="%s">Просмотр</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
            9 => sprintf( 'Запланировано на публикацию: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Просмотр</a>', date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
            10 => sprintf( 'Черновик обновлён. <a target="_blank" href="%s">Просмотр</a>', esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
        );

        return $messages;
    }

    function students_post_type_help_tab() {

        $screen = get_current_screen();

        // Прекращаем выполнение функции, если находимся на страницах других типов постов
        if ( 'students' != $screen->post_type )
            return;

        // Массив параметров для первой вкладки
        $args = array(
            'id'      => 'tab_1',
            'title'   => 'Обзор',
            'content' => '<h3>Обзор</h3><p>Содержимое первой вкладки.</p>'
        );

        // Добавляем вкладку
        $screen->add_help_tab( $args );

        // Массив параметров для второй вкладки
        $args = array(
            'id'      => 'tab_2',
            'title'   => 'Доступные действия',
            'content' => '<h3>Доступные действия с типом постов &laquo;' . $screen->post_type . '&raquo;</h3><p>Содержимое второй вкладки</p>'
        );

        // Добавляем вторую вкладку
        $screen->add_help_tab( $args );

    }

    add_action('admin_head', 'students_post_type_help_tab');