<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');

if (!check_for_spam($_REQUEST)) {
    return;
}

//Получаем данные из глобальной переменной $_GET, так как мы передаем данные методом GET
$name = $_GET['name']; // Вытаскиваем имя в переменную

$number = $_GET['number']; // Вытаскиваем почту в переменную
$target = $_GET['target'];
$message = "Форма обратной связи [$target]: \nИмя: $name \nНомер телефона $number \nLogs: $log "; // Формируем сообщение, отправляемое на почту
$to = get_option('admin_email'); // Задаем получателя письма
$from = "noreply-site.web.cofp.ru"; // От кого пришло письмо
$subject = "Письмо с сайта https://artist-m.ru/"; // Задаем тему письма
$headers = "From: $from\r\nReply-To: $to\r\nContent-type: text/html; charset=utf-8\r\n"; // Формируем заголовок письма (при неправильном формировании может ломаться кодировка и т.д.)
wp_mail('info@artist-m.ru', $subject, $message);
mail($to, $subject, $message, $headers);
