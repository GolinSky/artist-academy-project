

<?php
    require_once ($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');
    if (!check_for_spam($_REQUEST)) {
        return;
    }
    //Получаем данные из глобальной переменной $_GET, так как мы передаем данные методом GET
    $user_mail= $_GET['user_email'];
    $target =  $_GET['target'];
    $message = "Форма обратной связи [$target]: \nПочта: $user_mail"; // Формируем сообщение, отправляемое на почту
    $to =   get_option('admin_email'); // Задаем получателя письма
    $from = "Мастерская \"Артист\""; // От кого пришло письмо
    $subject = "Письмо с сайта https://artist-m.ru/"; // Задаем тему письма
    $headers = "From: $from\r\nReply-To: $to\r\nContent-type: text/html; charset=utf-8\r\n"; // Формируем заголовок письма (при неправильном формировании может ломаться кодировка и т.д.)
    wp_mail( 'info@artist-m.ru', $subject, $message);
    if (mail($to, $subject, $message, $headers))
    { // При помощи функции mail, отправляем сообщение, проверяя отправилось оно или нет
        echo "<p>Сообщение успешно отправлено</p>"; // Отправка успешна
        echo '<script>window.close()</script>';

        exit;

    }
    else {
        echo "<p>Что-то пошло не так, как планировалось</p>"; // Письмо не отправилось
        echo '<script>window.close()</script>';

        exit;
    }
