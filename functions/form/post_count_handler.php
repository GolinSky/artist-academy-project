<?php
    if (!check_for_spam($_REQUEST)) {
        return;
    }
    require_once ($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');
    $count = $_POST['count'];
    global $student_post_count;
    $student_post_count = $count;