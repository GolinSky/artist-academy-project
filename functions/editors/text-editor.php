<?php
use Editors\AdminEditor;

add_action('customize_register', function($customizer)
{
    require('AdminEditor.php');
    //choreography
    $admin_editor = new AdminEditor($customizer, 'contact_panel', 'choreography_section');
    $admin_editor->add_panel('Настройки страниц',10);
    $admin_editor->add_section('Хореография', 11);
    $admin_editor->add_control_setting('first_tb', 'text', 'Вступительное слово');
    $admin_editor->add_control_setting('first_par', 'textarea', 'Первый параграф');
    $admin_editor->add_control_image('Первое изображение', 'image1');
    $admin_editor->add_control_image('Второе изображение', 'image2');
    $admin_editor->add_control_setting('second_par', 'textarea', 'Второй параграф');
    $admin_editor->add_control_setting('first_tittle', 'text', 'Заголовок 1');
    $admin_editor->add_control_setting('third_par1', 'textarea', 'Третий параграф(часть 1)');
    $admin_editor->add_control_setting('third_par2', 'textarea', 'Третий параграф(часть 2)');
    $admin_editor->add_control_setting('second_tittle', 'text', 'Заголовок 2');
    $admin_editor->add_control_setting('fourth_par1', 'textarea', 'Четвертый параграф(часть 1)');
    $admin_editor->add_control_setting('link_text', 'url', 'Ссылка' );
    $admin_editor->add_control_setting('name_text', 'text', 'Текст' );
    $admin_editor->add_control_setting('fourth_par2', 'textarea', 'Четвертый параграф(часть 2)');
    $admin_editor->add_control_setting('mark_list', 'textarea', 'Маркированный список');

    //new section
    $admin_editor = new AdminEditor($customizer, 'contact_panel', 'vocals_section');
    $admin_editor->add_section('Вокал', 12);
    $admin_editor->add_control_setting('vocal_first_tb', 'text', 'Вступительное слово');
    $admin_editor->add_control_setting('vocal_first_par', 'textarea', 'Первый параграф');
    $admin_editor->add_control_image('Первое изображение', 'vocal_image1');
    $admin_editor->add_control_image('Второе изображение', 'vocal_image2');
    $admin_editor->add_control_setting('vocal_second_par', 'textarea', 'Второй параграф');
    $admin_editor->add_control_setting('vocal_first_tittle', 'text', 'Заголовок 1');
    $admin_editor->add_control_setting('vocal_third_par1', 'textarea', 'Третий параграф(часть 1)');
    $admin_editor->add_control_setting('vocal_third_par2', 'textarea', 'Третий параграф(часть 2)');
    $admin_editor->add_control_setting('vocal_second_tittle', 'text', 'Заголовок 2');
    $admin_editor->add_control_setting('vocal_fourth_par1', 'textarea', 'Четвертый параграф(часть 1)');
    $admin_editor->add_control_setting('vocal_link_text', 'url', 'Ссылка' );
    $admin_editor->add_control_setting('vocal_name_text', 'text', 'Текст' );
    $admin_editor->add_control_setting('vocal_fourth_par2', 'textarea', 'Четвертый параграф(часть 2)');
    $admin_editor->add_control_setting('vocal_mark_list', 'textarea', 'Маркированный список');

    //new section
    $admin_editor = new AdminEditor($customizer, 'contact_panel', 'action_section');
    $admin_editor->add_section('Актерское Мастерство', 13);
    $admin_editor->add_control_setting('action_first_tb', 'text', 'Вступительное слово');
    $admin_editor->add_control_setting('action_first_par', 'textarea', 'Первый параграф');
    $admin_editor->add_control_image('Первое изображение', 'action_image1');
    $admin_editor->add_control_image('Второе изображение', 'action_image2');
    $admin_editor->add_control_setting('action_second_par', 'textarea', 'Второй параграф');
    $admin_editor->add_control_setting('action_first_tittle', 'text', 'Заголовок 1');
    $admin_editor->add_control_setting('action_third_par1', 'textarea', 'Третий параграф(часть 1)');
    $admin_editor->add_control_setting('action_third_par2', 'textarea', 'Третий параграф(часть 2)');
    $admin_editor->add_control_setting('action_second_tittle', 'text', 'Заголовок 2');
    $admin_editor->add_control_setting('action_fourth_par1', 'textarea', 'Четвертый параграф(часть 1)');
    $admin_editor->add_control_setting('action_link_text', 'url', 'Ссылка' );
    $admin_editor->add_control_setting('action_name_text', 'text', 'Текст' );
    $admin_editor->add_control_setting('action_fourth_par2', 'textarea', 'Четвертый параграф(часть 2)');
    $admin_editor->add_control_setting('action_mark_list', 'textarea', 'Маркированный список');

    //main
    //new section
    $admin_editor = new AdminEditor($customizer, 'contact_panel', 'index_section');
    $admin_editor->add_section('Главная Страница', 13);

    $admin_editor->add_control_setting('index_direction1', 'text', 'Направление 1');
    $admin_editor->add_control_setting('index_direction_desc1', 'textarea', 'Описание направления 1');
    $admin_editor->add_control_setting('index_bg_direction_desc1', 'textarea', 'Описание направления 1(Краткое Описание)');

    $admin_editor->add_control_image('Фон направление 1', 'direction_bg1');

    $admin_editor->add_control_setting('index_direction2', 'text', 'Направление 2');
    $admin_editor->add_control_setting('index_direction_desc2', 'textarea', 'Описание направления 2');
    $admin_editor->add_control_setting('index_bg_direction_desc2', 'textarea', 'Описание направления 2(Краткое Описание)');

    $admin_editor->add_control_image('Фон направление 2', 'direction_bg2');

    $admin_editor->add_control_setting('index_direction3', 'text', 'Направление 3');
    $admin_editor->add_control_setting('index_direction_desc3', 'textarea', 'Описание направления 3');
    $admin_editor->add_control_setting('index_bg_direction_desc3', 'textarea', 'Описание направления 3(Краткое Описание)');
    $admin_editor->add_control_image('Фон направление 3', 'direction_bg3');

    $admin_editor->add_control_setting('index_student_amount', 'number', 'Кол-во выводимых студентов');




});

function Output($name)
{
    echo get_theme_mod($name);
}

function OutputInt($name)
{
    return (int)get_theme_mod($name);
}


function OutputVocals($name)
{
    Output('vocal_' . $name);
}

function OutputActing($name)
{
    Output('action_' . $name);
}

function OutputImage($name)
{
    echo get_theme_mod($name);
}

function OutputVocalsImage($name)
{
    OutputImage('vocal_' .$name);
}

function OutputActingImage($name)
{
    OutputImage('action_' .$name);
}

function OutputMarkedList($object)
{
    $split_list = explode("\n", $object);
    if(count($split_list) > 0)
    {
        for ($i = 0; $i < count($split_list); $i++)
        {
            if($split_list[$i] != '')
            {
                echo '<li><p>'. $split_list[$i] . '</p></li>';
            }
        }
    }
}






