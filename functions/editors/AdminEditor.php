<?php
namespace Editors;

use WP_Customize_Image_Control;

class AdminEditor
{
    protected $root_panel = '';
    protected $root_section= '';
    protected $customizer;
    protected $transport = 'refresh';//  postMessage

    //getters
    public function get_root_section()
    {
        return $this->root_section;
    }

    public function __construct($customizer, $root_panel, $root_section)
    {
        $this->customizer = $customizer;
        $this->root_panel = $root_panel;
        $this->root_section = $root_section;
    }

    public function add_panel($panel_tittle, $panel_priority)
    {
        $this->customizer->add_panel(
            $this->root_panel,
            array(
                'priority'       => $panel_priority,
                'theme_supports' => '',
                'title'          => __($panel_tittle, 'artist_theme'),
                'description'    => __($panel_tittle, 'artist_theme'),
            )
        );
    }
    public function add_section($section_tittle, $section_priority)
    {
        $this->customizer->add_section(
            $this->root_section,
            array(
                'title' => $section_tittle,
                'description' => $section_tittle,
                'priority' => $section_priority,
                'panel' => $this->root_panel,

            )
        );
    }

    public function add_control_setting($settings_name, $control_type, $control_label)
    {
        $this->customizer->add_setting(
            $settings_name,
            array(
                'default' => '^ ^',
                'transport' => $this->transport,
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options'
            )
        );
        $this->customizer->add_control(
            $settings_name,
            array(
                'label' => $control_label,
                'section' => $this->root_section,
                'type' => $control_type,
                'settings' => $settings_name
            )
        );
    }

    public function add_control_image($settings_label, $settings_name)
    {
        $this->customizer->add_setting(
            $settings_name,
            array(
            'default'        => '',
            'transport' => $this->transport
            ));
        $this->customizer->add_control(new WP_Customize_Image_Control(
            $this->customizer,
            $settings_name, array(
            'label'   => $settings_label,
            'section' => $this->root_section,
            'settings'   => $settings_name,
        )
        ) );
    }
}