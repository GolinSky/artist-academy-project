<?php

global $root_url_path;
/**
 * Template Name: Галерея
 */
get_header(); ?>

    <div class="main page__main">
        <div class="main__header">
            <div class="main__container container">
                <h3 class="main__title">Галерея</h3>
                <ul class="breadcrumbs main__breadcrumbs">
                    <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/">Главная</a>
                    </li>
                    <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Галерея</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main__content">
            <div class="album">
                <div class="album__container container">
                    <div class="album__content">
                        <div class="album__grid">

                            <!--get post  album-->

                            <?php
                            $reviews = new WP_Query(
                                array(
                                    'post_type' => 'album',
                                    'posts_per_page' => -1,
                                    'post_status'	   => 'publish',
                                    'orderby' => 'post_date',
                                    'order' => 'ASC',
                                    'suppress_filters' => true
                                )
                            );
                            if ( $reviews->have_posts() ) : while ( $reviews->have_posts() ) : $reviews->the_post(); ?>
                            <div class="album__column">
                                <a class="album-card" href="<?php the_permalink(); ?>">
                                    <div class="album-card__photo">
                                        <img class="album-card__img" src="<?php echo get_the_post_thumbnail_url(); ?>" alt=""><span class="album-card__gradient"></span>
                                    </div>
                                    <div class="album-card__data"><span class="album-card__title"><?php the_title(); ?></span>
                                        <div class="album-card__arrow"><i class="fa fa-angle-right"></i>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <?php endwhile; ?>
                            <?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php get_footer();
