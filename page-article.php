<?php /* Template Name: Article Template */ ?>
<?php
global $root_url_path;
get_header(); ?>

    <div class="main page__main">
        <div class="main__header">
            <div class="main__container container">
                <h3 class="main__title">Статьи сайта</h3>
                <ul class="breadcrumbs main__breadcrumbs">
                    <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/">Главная</a>
                    </li>
                    <li class="breadcrumbs__item"><a class="breadcrumbs__link">Статьи</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main__content">
            <div class="news main__news">
                <div class="news__container container">
                    <div class="news__content">
                        <div class="news__grid">

                            <?php $reviews = new WP_Query(
                                array(
                                    'post_type' => 'articles',
                                    'posts_per_page' => -1,
                                    'post_status'	   => 'publish',
                                    'orderby' => 'post_date',
                                    'order' => 'ASC',
                                    'suppress_filters' => true
                                )
                            );  ?>

                            <?php if ( $reviews->have_posts() ) : while ( $reviews->have_posts() ) : $reviews->the_post(); ?>

                            <div class="news__column">
                                <div class="news-card">
                                    <div class="news-card__photo">
                                        <div class="news-card__photo-gradient"></div>
                                        <img class="news-card__img" src="<?php the_post_thumbnail_url('full');  ?>" alt="">
                                    </div>
                                    <div class="news-card__data">
                                        <div class="news-card__date">
                                            <img class="news-card__icon" src="<?php echo $root_url_path?>/static/img/general/icons/calendar-gray.svg" alt=""><span class="news-card__date-text"><?php echo get_the_date( 'j.m.Y'); ?><!--21.11.2019--></span>
                                        </div><a class="news-card__title" href="<?php the_permalink(); ?>"><?php the_title();?></a><a class="news-card__button" href="<?php the_permalink(); ?>">Читать далее</a>
                                    </div>
                                </div>
                            </div>
                             <?php endwhile; ?>
                            <?php endif; ?>

                        </div>
                   <!--     <button class="button button-large news__more"><span class="button__text">Показать больше</span>
                        </button>-->
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php get_footer();
