<!DOCTYPE html>
<html class="no-js" lang="ru">
<?php global $root_url_path; ?>
<?php global $url;
$url="https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php echo get_bloginfo('name');?> </title>
    <meta content="" name="description">
    <meta content="" name="keywords">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="telephone=no" name="format-detection">
    <meta name="HandheldFriendly" content="true">

    <!--[if (gt IE 9)|!(IE)]><!-->
    <link href="<?php echo $root_url_path?>/static/css/main.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $root_url_path?>/style.css" rel="stylesheet" type="text/css">

    <!--<![endif]-->
    <meta property="og:title" content="#{data.title}">
    <meta property="og:title" content="">
    <meta property="og:url" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="500">
    <meta property="og:image:height" content="300">
    <meta property="twitter:description" content="">
    <link rel="icon" type="image/x-icon" href="<?php echo get_stylesheet_directory_uri()?>/favicon.ico">
   <!-- <script type="text/javascript" src="<?php /*echo $root_url_path*/?>/static/js/jquery.min.js"></script>-->

    <script>
        (function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-148588827-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-148588827-1');
    </script>
    <script type="text/javascript">



        function Form3ClickCallBack()
        {
            var valid = $("#third_form input");

            for(var i = 0;i<valid.length;i++)
            {
                if(!valid[i].checkValidity())
                {
                    valid[i].closest('div').classList.add('invalid');
                    return false;
                }
                else
                {
                    valid[i].closest('div').classList.remove('invalid');
                }
            }

            $.fancybox.close();
            return true;
            $("#third_form")[0].reset();
            $.fancybox.close();

        }

        function Form2ClickCallBack()
        {
            var valid = $("#second_form input");
            for(var i = 0;i<valid.length;i++)
            {
                if(!valid[i].checkValidity())
                {
                    valid[i].closest('div').classList.add('invalid');
                    return false;
                }
                else
                {
                    valid[i].closest('div').classList.remove('invalid');
                }
            }

            Form2Click();
            $("#second_form")[0].reset();
            $.fancybox.close();
            return false;
        }


        function Form4ClickCallBack() {

            var valid = $("#fourth-form input");
            for(var i = 0;i<valid.length;i++)
            {
                if(!valid[i].checkValidity())
                {
                    valid[i].closest('div').classList.add('invalid');
                    return false;
                }
                else
                {
                    valid[i].closest('div').classList.remove('invalid');
                }
            }

            Form4Click();
            $("#fourth-form")[0].reset();
            $.fancybox.close();
            return false;
        }

        function Form4Click() {

            var user_email = $("#fourth-form input[name=user_email]").val();
            var target = 'Форма Рассылки';
            $.ajax({
                url: "<?php echo $root_url_path?>/functions/form/formMailing.php",
                method: 'get',
                data: {
                    user_email: user_email,
                    target: target
                },
                success: function (response) {
                    //$('#submit-ajax').html(response);
                }
            });
        }



        function Form2Click() {

            var name = $("#second_form input[name=user_name]").val();
            var number = $("#second_form input[name=user_number]").val();
            var target = 'Остались вопросы';

            $.ajax({
                url: "<?php echo $root_url_path?>/functions/form/formNumberNameHandler.php",
                method: 'get',
                data: {
                    name: name,
                    number: number,
                    target: target

                },
                success: function (response) {
                    //alert('response: '.response);
                }
            });

        }


        function Form3Click() {
            var name = $("#third_form input[name=name_requiredsurname]").val();
            var user_email = $("#third_form input[name=userequiredr_email]").val();
            var user_review = $("#third_form textarea[name=message]").val();
            var fileToUpload = $("#third_form input[name=fileToUpload]").val();
            var target_subject = 'Форма отзыва';


            $.ajax({
                url: "<?php echo $root_url_path?>/functions/form/formFileHandler.php",
                method: 'POST',
                data:
                    {
                        name: name,
                        user_email: user_email,
                        user_review: user_review,
                        fileToUpload:fileToUpload,
                        target_subject: target_subject
                    },

                cache: false,
                processData: false,
                success: function (response) {
                    console.log(response);
                }
            });
        }
    </script>

</head>
<body class="page">

<nav class="pushy pushy-right">
    <div class="pushy-content">
        <ul>
            <li class="pushy-submenu">Направления<i class="fa fa-angle-down"></i>
                <ul>
                    <?php
                    if(!current_user_can('administrator'))
                    {
                        echo '<li class="pushy-link"><a href="#dev" data-fancybox >Актерское мастерство</a></li>';

                    }
                    else
                    {
                        echo '<li class="pushy-link"><a href="' .  get_page_url_in_menu('acting', $url) . '">Актерское мастерство</a></li>';
                    }
                    ?>
                    <li class="pushy-link"><a href="<?php get_page_url_in_menu('choreography', $url); ?>" >Хореография</a>
                    </li>
                    <li class="pushy-link"><a href="#dev" data-fancybox>Вокал</a>
                    </li>
                </ul>
            </li>
            <li class="pushy-link"><a href="<?php get_student_url_in_menu($url); ?>">Ученики</a>
            </li>
            <li class="pushy-link"><a href="#dev" data-fancybox>Наставники</a>
            </li>
            <li class="pushy-link"><a href="<?php get_page_url_in_menu('feedback', $url); ?>" >Отзывы</a>
            </li>
            <li class="pushy-link"><a href="<?php get_page_url_in_menu('price', $url); ?>">Цены </a>
            </li>
            <li class="pushy-link"><a href="<?php get_page_url_in_menu('contacts', $url); ?>">Контакты </a>
            </li>
            <li class="pushy-link pushy-link--socials">
                <ul>
                    <li class="pushy-link">
                        <a href="#dev" data-fancybox> <i class="fa fa-vk"></i>
                        </a>
                    </li>
                    <li class="pushy-link"><a href="#dev" data-fancybox><i class="fa fa-instagram"></i></a>
                    </li>
                    <li class="pushy-link"><a href="#dev" data-fancybox><i class="fa fa-facebook"></i></a>
                    </li>
                    <li class="pushy-link"><a href="#dev" data-fancybox><i class="fa fa-odnoklassniki"></i></a>
                    </li>
                    <li class="pushy-link"><a href="#dev" data-fancybox><i class="fa fa-youtube-play"></i></a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
<div class="page__overlay site-overlay"></div>


<div class="lines page__lines">
    <div class="lines__container container">
        <ul class="lines__list">
            <li class="lines__item">
                <img class="lines__img" src="<?php echo $root_url_path?>/static/img/general/vertical-lines.png" alt="">
            </li>
            <li class="lines__item">
                <img class="lines__img" src="<?php echo $root_url_path?>/static/img/general/vertical-lines.png" alt="">
            </li>
            <li class="lines__item">
                <img class="lines__img" src="<?php echo $root_url_path?>/static/img/general/vertical-lines.png" alt="">
            </li>
            <li class="lines__item">
                <img class="lines__img" src="<?php echo $root_url_path?>/static/img/general/vertical-lines.png" alt="">
            </li>
            <li class="lines__item">
                <img class="lines__img" src="<?php echo $root_url_path?>/static/img/general/vertical-lines.png" alt="">
            </li>
        </ul>
    </div>
</div>
<div class="page__header">
    <div class="header">
        <div class="header__container container">
            <a class="logo header__logo" href="<?php
            if(is_home())
            {
                echo '#';
            }
            else
            {
               echo get_home_url();
            }
            ?>">
                <img class="logo__img" src="<?php echo $root_url_path?>/static/img/general/icons/logo.svg" alt="">
            </a>
            <div class="header__data">
                <div class="navbar header__navbar">
                    <ul class="socials navbar__socials">
                        <li class="socials__item">
                            <a class="socials__link" href="https://vk.com/artist_m" target="_blank">
                                <img class="socials__icon" src="<?php echo $root_url_path?>/static/img/general/icons/vk.svg" alt="">
                            </a>
                        </li>
                        <li class="socials__item">
                            <a class="socials__link" href="https://www.instagram.com/mast_artist/" target="_blank">
                                <img class="socials__icon" src="<?php echo $root_url_path?>/static/img/general/icons/inst.svg" alt="">
                            </a>
                        </li>
                        <li class="socials__item">
                            <a class="socials__link" href="https://www.facebook.com/mast.artist/" target="_blank">
                                <img class="socials__icon" src="<?php echo $root_url_path?>/static/img/general/icons/fb.svg" alt="">
                            </a>
                        </li>
                        <li class="socials__item">
                            <a class="socials__link" href="https://ok.ru/artist.m" target="_blank">
                                <img class="socials__icon" src="<?php echo $root_url_path?>/static/img/general/icons/ok.svg" alt="">
                            </a>
                        </li>
                        <li class="socials__item"><a class="socials__link" href="https://www.youtube.com/channel/UCRl-zLap2AizJW6aRBf40jg" target="_blank">
                                <!-- <i class="fa fa-youtube-play"></i> -->
                                <img class="socials__icon" src="<?php echo $root_url_path?>/static/img/general/icons/you-n.svg" alt="">
                                <!--  <div class="socials__link-text">Идет онлайн-эфир!</div> --></a>
                        </li>
                    </ul>
                    <div class="navbar__data">
                        <ul class="contacts-box navbar__contacts-box">
                            <li class="contacts-box__item"><a class="contacts-box__link" href="tel:+79104691291"><span class="contacts-box__icon"><img class="contacts-box__img" src="<?php echo $root_url_path?>/static/img/general/icons/phone.svg" alt=""></span><span class="contacts-box__text">+7 (910) 469-12-91</span></a>
                            </li>
                            <li class="contacts-box__item"><a class="contacts-box__link" href="mailto:info@artist-m.ru"><span class="contacts-box__icon"><img class="contacts-box__img" src="<?php echo $root_url_path?>/static/img/general/icons/envelope.svg" alt=""></span><span class="contacts-box__text">info@artist-m.ru</span></a>
                            </li>
                        </ul>
                        <!--<a class="button button-secondary navbar__login" href="#dev" data-fancybox>
                            <img class="button__icon" src="<?php /*echo $root_url_path*/?>/static/img/general/icons/lock.svg" alt=""><span class="button__text">Войти в кабинет</span>
                        </a>--><a class="navbar__mobile-btn menu-btn" href="#"><i class="fa fa-bars"></i></a>
                    </div>
                </div>
                <div class="header__content">
                    <ul class="menu header__menu">
                        <li class="menu__item menu__item--dropdown"><a class="menu__link" href="#dev" data-fancybox>Направления<i class="fa fa-angle-down"></i></a>
                            <ul class="menu__dropdown">
                                <li class="menu__dropdown-item"><a class="menu__dropdown-link" href="<?php get_closed_url_in_menu('acting', $url); ?>">Актёрское мастерство</a>
                                </li>
                                <li class="menu__dropdown-item"><a class="menu__dropdown-link" href="<?php get_closed_url_in_menu('choreography', $url); ?>">Хореография</a>
                                </li>
                                <li class="menu__dropdown-item"><a class="menu__dropdown-link" href="<?php get_closed_url_in_menu('vocals', $url); ?>">Вокал</a>
                                </li>
                            </ul>
                        </li>
                        <li class="menu__item"><a class="menu__link"  href="<?php get_student_url_in_menu($url); ?>">Ученики</a>
                        </li>
                        <li class="menu__item"><a class="menu__link" href="#dev" data-fancybox >Наставники</a>
                        </li>
                        <li class="menu__item"><a class="menu__link" href="<?php get_page_url_in_menu('feedback', $url); ?>" >Отзывы</a>
                        </li>
                        <li class="menu__item"><a class="menu__link" href="<?php
                            get_page_url_in_menu('price', $url);
                            ?>" >Цены</a>
                        </li>
                        <li class="menu__item"><a class="menu__link" href="<?php get_page_url_in_menu('contacts', $url); ?>">Контакты</a>
                        </li>
                    </ul>
                    <div class="header__actions">
                        <a class="button header__button header__button--writing" href="#dev" data-fancybox>
                            <img class="button__icon" src="<?php echo $root_url_path?>/static/img/general/icons/calendar.svg" alt=""><span class="button__text">Раписание занятий</span>
                        </a>
                        <a class="button button-default header__button header__button--order" href="#order" data-fancybox>
                            <img class="button__icon" src="<?php echo $root_url_path?>/static/img/general/icons/pencil.svg" alt=""><span class="button__text">Оставить заявку</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


























