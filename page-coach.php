<?php
global $root_url_path;
get_header(); ?>

    <div class="main page__main">
        <div class="main__header">
            <div class="main__container container">
                <h3 class="main__title">НАСТАВНИКИ МАСТЕРСКОЙ</h3>
                <ul class="breadcrumbs main__breadcrumbs">
                    <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/">Главная</a>
                    </li>
                    <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Наставники</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main__content">
            <div class="coach main__coach">
                <div class="coach__top">
                    <div class="coach__menu">
                        <div class="coach__container container">
                            <ul class="coach__list">


                                <?php $educator_counter = 1;
                                $reviews = new WP_Query(
                                    array(
                                        'post_type' => 'educator',
                                        'posts_per_page' => -1,
                                        'post_status'	   => 'publish',
                                        'orderby' => 'post_date',
                                        'order' => 'ASC',
                                        'suppress_filters' => true
                                    )
                                );
                                if ( $reviews->have_posts() ) : while ( $reviews->have_posts() ) : $reviews->the_post(); ?>
                                    <?php
                                    if(get_the_title(get_the_ID()) == "Виталий Васильев") : continue; endif;
                                    ?>
                                    <?php $imgURL = MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'secondary-image', NULL, 'large');?>

                                    <li class="coach__item">
                                        <a class="coach-card-small js-scroll-to" href="#<?= $educator_counter;?>">
                                            <div class="coach-card-small__photo">
                                                <img class="coach-card-small__img" src="<?= $imgURL;?>" alt="" height="41" width="41">
                                            </div>
                                            <div class="coach-card-small__data"><span class="coach-card-small__title"><?php the_title();?></span><span class="coach-card-small__position"><?= get_field('specialization');?></span>
                                            </div>
                                        </a>
                                    </li>

                                    <?php $educator_counter++?>
                                <?php endwhile; ?>
                                <?php endif; ?>

                            </ul>
                        </div>
                    </div>
                    <div class="coach__wrp">
                        <div class="coach__container container">
                            <ul class="coach__list">
                                <?php $educator_counter = 1;
                                $reviews = new WP_Query(
                                    array(
                                        'post_type' => 'educator',
                                        'posts_per_page' => -1,
                                        'post_status'	   => 'publish',
                                        'orderby' => 'post_date',
                                        'order' => 'ASC',
                                        'suppress_filters' => true
                                    )
                                );
                                if ( $reviews->have_posts() ) : while ( $reviews->have_posts() ) : $reviews->the_post(); ?>
                                <?php
                                if(get_the_title(get_the_ID()) == "Виталий Васильев") : continue; endif;
                                ?>
                                <?php $imgURL = MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'secondary-image', NULL, 'large');?>


                                <li class="coach__item">
                                    <a class="coach-card-small js-scroll-to" href="#1">
                                        <div class="coach-card-small__photo">
                                            <img class="coach-card-small__img" src="<?= $imgURL;?>" alt="" height="41" width="41">
                                        </div>
                                        <div class="coach-card-small__data"><span class="coach-card-small__title"><?php the_title();?></span><span class="coach-card-small__position"><?= get_field('specialization');?></span>
                                        </div>
                                    </a>
                                </li>

                                    <?php $educator_counter++?>
                                <?php endwhile; ?>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="coach__content">
                    <div class="coach__container container">
                        <div class="coach__tabs">

                            <?php $educator_counter = 1;
                            $reviews = new WP_Query(
                                array(
                                    'post_type' => 'educator',
                                    'posts_per_page' => -1,
                                    'post_status'	   => 'publish',
                                    'orderby' => 'post_date',
                                    'order' => 'ASC',
                                    'suppress_filters' => true
                                )
                            );
                            if ( $reviews->have_posts() ) : while ( $reviews->have_posts() ) : $reviews->the_post(); ?>
                            <?php
                            if(get_the_title(get_the_ID()) == "Виталий Васильев") : continue; endif;
                            ?>

                            <div class="coach__tab" id="<?= $educator_counter; ?>">
                                <div class="coach-card">
                                    <div class="coach-card__left">
                                        <div class="coach-card__photo">
                                            <img class="coach-card__img" src="<?= get_field('main_image'); ?>" alt="">
                                        </div>
                                        <div class="coach-card__info">
                                            <h2 class="coach-card__title"><?php the_title();?></h2><span class="coach-card__position"><?=  get_field('specialization');?></span><span class="coach-card__slogan"><?=  get_field('education');?></span>
                                            <ul class="coach-card__list">
                                                <li class="coach-card__item">
                                                    <a class="coach-card__link" href="<?=  get_field( 'vk_social_link' );?>">
                                                        <img class="coach-card__link-img" src="<?=  $root_url_path;?>/static/img/coach/vk-c.png" alt="">
                                                    </a>
                                                </li>
                                                <li class="coach-card__item">
                                                    <a class="coach-card__link" href="<?=  get_field( 'inst_social_link' );?>">
                                                        <img class="coach-card__link-img" src="<?=  $root_url_path;?>/static/img/coach/in-c.png" alt="">
                                                    </a>
                                                </li>
                                                <li class="coach-card__item">
                                                    <a class="coach-card__link" href="<?=  get_field( 'fb_social_link' );?>">
                                                        <img class="coach-card__link-img" src="<?=  $root_url_path;?>/static/img/coach/fb-c.png" alt="">
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="coach-card__right">
                                        <h2 class="coach-card__heading">Достижения</h2>
                                        <div class="coach-card__scrollbar scrollbar-inner js-coach-card-scroll">
                                            <ul class="coach-card__actions">

                                                <?php
                                                $education_list = get_field( 'achievements' );
                                                $split_list = explode("\n", $education_list);
                                                if(count($split_list) > 0)
                                                {
                                                    for ($i = 0; $i < count($split_list); $i++)
                                                    {
                                                        if($split_list[$i] != '')
                                                        {
                                                            echo '<li class="coach-card__actions-item"><span class="coach-card__actions-text"> '. $split_list[$i] . '</span></li>';
                                                        }
                                                    }
                                                }
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <?php $educator_counter++?>
                            <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



<?php get_footer();?>
    <script>
        var postion = $('.coach__content').offset().top,
            height = $('.coach__content').height();
        $(document).on('scroll', function (){
            var scroll = $(document).scrollTop();
            if(scroll  > postion && scroll < (postion + height) ) {
                $('.coach__wrp').addClass('scrolled')
            }else {
                $('.coach__wrp').removeClass('scrolled')
            }
        })
    </script>
