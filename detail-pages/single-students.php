
<!DOCTYPE html>
<html class="no-js" lang="ru">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>default title</title>
    <meta content="" name="description">
    <meta content="" name="keywords">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="telephone=no" name="format-detection">
    <meta name="HandheldFriendly" content="true">

    <!--[if (gt IE 9)|!(IE)]><!-->
    <link href="<?php echo get_stylesheet_directory_uri()?>/static/css/main.min.css" rel="stylesheet" type="text/css">
    <!--<![endif]-->
    <meta property="og:title" content="#{data.title}">
    <meta property="og:title" content="">
    <meta property="og:url" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="500">
    <meta property="og:image:height" content="300">
    <meta property="twitter:description" content="">
    <link rel="image_src" href="">
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    <script>
        (function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)
    </script>
</head>

<body class="page">
<div class="lines page__lines">
    <div class="lines__container container">
        <ul class="lines__list">
            <li class="lines__item">
                <img class="lines__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/vertical-lines.png" alt="">
            </li>
            <li class="lines__item">
                <img class="lines__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/vertical-lines.png" alt="">
            </li>
            <li class="lines__item">
                <img class="lines__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/vertical-lines.png" alt="">
            </li>
            <li class="lines__item">
                <img class="lines__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/vertical-lines.png" alt="">
            </li>
            <li class="lines__item">
                <img class="lines__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/vertical-lines.png" alt="">
            </li>
        </ul>
    </div>
</div>
<div class="page__header">
    <div class="header">
        <div class="header__container container">
            <a class="logo header__logo" href="/">
                <img class="logo__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/logo.svg" alt="">
            </a>
            <div class="header__data">
                <div class="navbar header__navbar">
                    <ul class="socials navbar__socials">
                        <li class="socials__item">
                            <a class="socials__link" href="#">
                                <img class="socials__icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/vk.svg" alt="">
                            </a>
                        </li>
                        <li class="socials__item">
                            <a class="socials__link" href="#">
                                <img class="socials__icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/inst.svg" alt="">
                            </a>
                        </li>
                        <li class="socials__item">
                            <a class="socials__link" href="#">
                                <img class="socials__icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/fb.svg" alt="">
                            </a>
                        </li>
                        <li class="socials__item">
                            <a class="socials__link" href="#">
                                <img class="socials__icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/ok.svg" alt="">
                            </a>
                        </li>
                        <li class="socials__item socials__item--accent">
                            <a class="socials__link" href="#">
                                <img class="socials__icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/you-n.svg" alt="">
                                <div class="socials__link-text">Идет онлайн-эфир!</div>
                            </a>
                        </li>
                    </ul>
                    <div class="navbar__data">
                        <ul class="contacts-box navbar__contacts-box">
                            <li class="contacts-box__item"><a class="contacts-box__link" href="tel:+79104691291"><span class="contacts-box__icon"><img class="contacts-box__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/phone.svg" alt=""></span><span class="contacts-box__text">+7 (910) 469-12-91</span></a>
                            </li>
                            <li class="contacts-box__item"><a class="contacts-box__link" href="mailto:info@artist-m.ru"><span class="contacts-box__icon"><img class="contacts-box__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/envelope.svg" alt=""></span><span class="contacts-box__text">info@artist-m.ru</span></a>
                            </li>
                        </ul>
                        <a class="button button-secondary navbar__login" href="#dev" data-fancybox>
                            <img class="button__icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/lock.svg" alt=""><span class="button__text">Войти в кабинет</span>
                        </a><a class="navbar__mobile-btn menu-btn" href="#"><i class="fa fa-bars"></i></a>
                    </div>
                </div>
                <div class="header__content">
                    <ul class="menu header__menu">
                        <li class="menu__item menu__item--dropdown"><a class="menu__link" href="#">Направления<i class="fa fa-angle-down"></i></a>
                            <ul class="menu__dropdown">
                                <li class="menu__dropdown-item"><a class="menu__dropdown-link" href="#">Актерское мастерство</a>
                                </li>
                                <li class="menu__dropdown-item"><a class="menu__dropdown-link" href="#">Хореография</a>
                                </li>
                                <li class="menu__dropdown-item"><a class="menu__dropdown-link" href="#">Вокал</a>
                                </li>
                            </ul>
                        </li>
                        <li class="menu__item"><a class="menu__link" href="#">Ученики</a>
                        </li>
                        <li class="menu__item"><a class="menu__link" href="#">Педагоги</a>
                        </li>
                        <li class="menu__item"><a class="menu__link" href="#">Отзывы</a>
                        </li>
                        <li class="menu__item menu__item--accent"><a class="menu__link" href="#">Цены</a>
                        </li>
                        <li class="menu__item"><a class="menu__link" href="#">Контакты</a>
                        </li>
                    </ul>
                    <div class="header__actions">
                        <a class="button header__button header__button--writing" href="#">
                            <img class="button__icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/calendar.svg" alt=""><span class="button__text">Раписание занятий</span>
                        </a>
                        <a class="button button-default header__button header__button--order" href="#order" data-fancybox>
                            <img class="button__icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/pencil.svg" alt=""><span class="button__text">Оставить заявку</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main">
    <div class="main__header">
        <div class="main__container container">
            <h3 class="main__title">Диана тимофеева</h3>
            <ul class="breadcrumbs main__breadcrumbs">
                <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/">Главная</a>
                </li>
                <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Ученики  </a>
                </li>
                <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Диана Тимофеева</a>
                </li>
            </ul>
            <div class="detail main__detail">
                <div class="detail__swiper">
                    <div class="detail__swiper-container swiper-container js-detail-swiper">
                        <div class="detail__swiper-wrapper swiper-wrapper">
                            <div class="detail__swiper-slide swiper-slide">
                                <div class="detail-card detail-card--video">
                                    <a class="detail-card__photo" href="https://www.youtube.com/watch?v=2njmekeVxLQ" data-fancybox="detail">
                                        <img class="detail-card__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/detail-card-1.jpg" alt="">
                                    </a>
                                    <div class="detail-card__data">
                                        <div class="detail-card__play">
                                            <div class="detail-card__play-icon">
                                                <img class="detail-card__play-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/play-big.svg" alt="">
                                            </div>
                                            <div class="detail-card__play-link"><span class="detail-card__play-text">Посмотреть ролик</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="detail__swiper-slide swiper-slide">
                                <div class="detail-card">
                                    <a class="detail-card__photo" href="<?php echo get_stylesheet_directory_uri()?>/static/img/general/detail-card-2.jpg" data-fancybox="detail">
                                        <img class="detail-card__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/detail-card-2.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="detail__swiper-slide swiper-slide">
                                <div class="detail-card">
                                    <a class="detail-card__photo" href="<?php echo get_stylesheet_directory_uri()?>/static/img/general/detail-card-3.jpg" data-fancybox="detail">
                                        <img class="detail-card__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/detail-card-3.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="detail__swiper-slide swiper-slide">
                                <div class="detail-card">
                                    <a class="detail-card__photo" href="<?php echo get_stylesheet_directory_uri()?>/static/img/general/detail-card-4.jpg" data-fancybox="detail">
                                        <img class="detail-card__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/detail-card-4.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="detail__swiper-slide swiper-slide">
                                <div class="detail-card">
                                    <a class="detail-card__photo" href="<?php echo get_stylesheet_directory_uri()?>/static/img/general/detail-card-1.jpg" data-fancybox="detail">
                                        <img class="detail-card__img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/detail-card-1.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-controls detail__swiper-controls">
                    <div class="swiper-controls__actions">
                        <button class="swiper-controls__button swiper-controls__button--prev"></button>
                        <button class="swiper-controls__button swiper-controls__button--next"></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main__content">
        <div class="detail main__detail">
            <div class="detail__container container">
                <div class="detail__content">
                    <div class="detail__feedback"><span class="detail__feedback-title">Понравился Артист?</span>
                        <form class="detail__feedback-form">
                            <div class="detail__feedback-field">
                                <span class="detail__feedback-error-text">Неверно</span>
                                <input class="detail__feedback-input" type="text" placeholder="Ваше имя">
                            </div>
                            <div class="detail__feedback-field">
                                <span class="detail__feedback-error-text">Неверно</span>
                                <input class="detail__feedback-input" type="email" placeholder="Электронная почта">
                            </div>
                            <div class="detail__feedback-field">
                                <span class="detail__feedback-error-text">Неверно</span>
                                <input class="detail__feedback-input" type="tel" placeholder="Номер телефона">
                            </div>
                            <button class="button detail__feedback-button">Пригласить на кастинг</button>
                        </form>
                    </div>
                </div>
                <div class="detail__footer">
                    <ul class="detail__chars-list">
                        <li class="detail__chars-item">
                            <div class="detail__chars-icon">
                                <img class="detail__chars-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/chars-1.png" alt="">
                            </div>
                            <div class="detail__chars-data"><span class="detail__chars-title">Возраст:</span><span class="detail__chars-text">11 лет</span>
                            </div>
                        </li>
                        <li class="detail__chars-item">
                            <div class="detail__chars-icon">
                                <img class="detail__chars-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/chars-2.png" alt="">
                            </div>
                            <div class="detail__chars-data"><span class="detail__chars-title">Рост:</span><span class="detail__chars-text">142 см</span>
                            </div>
                        </li>
                        <li class="detail__chars-item">
                            <div class="detail__chars-icon">
                                <img class="detail__chars-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/chars-3.png" alt="">
                            </div>
                            <div class="detail__chars-data"><span class="detail__chars-title">Цвет волос:</span><span class="detail__chars-text">серо-голубой</span>
                            </div>
                        </li>
                        <li class="detail__chars-item">
                            <div class="detail__chars-icon">
                                <img class="detail__chars-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/chars-4.png" alt="">
                            </div>
                            <div class="detail__chars-data"><span class="detail__chars-title">Цвет волос:</span><span class="detail__chars-text">шатен</span>
                            </div>
                        </li>
                    </ul>
                    <div class="detail__grid">
                        <div class="detail__column">
                            <div class="detail__section">
                                <div class="detail__section-header">
                                    <img class="detail__section-icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/datail-1.png" alt=""><span class="detail__section-title">Образование:</span><i class="fa fa-angle-down"></i>
                                </div>
                                <div class="detail__section-content">
                                    <ul class="detail__section-list">
                                        <li class="detail__section-item"><span class="detail__section-text">Мастерская "Артист"</span>
                                        </li>
                                        <li class="detail__section-item"><span class="detail__section-text">ДМШ #27 им. С.В. Рахманинова по классу фортепиано, 4 класс</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="detail__section">
                                <div class="detail__section-header">
                                    <img class="detail__section-icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/datail-5.png" alt=""><span class="detail__section-title">Конкурсы</span><i class="fa fa-angle-down"></i>
                                </div>
                                <div class="detail__section-content">
                                    <ul class="detail__section-list">
                                        <li class="detail__section-item"><span class="detail__section-text">2015 г. </span>
                                        </li>
                                        <li class="detail__section-item detail__section-item--secondary"><span class="detail__section-text">XII Открытый фестиваль детского творчества "Юное дарование-2015", лауреат II степени в номинации "Эстрадный вокал", г. Москва</span>
                                        </li>
                                        <li class="detail__section-item detail__section-item--secondary"><span class="detail__section-text">Международный фестиваль-конкурс "Вдохновение", лауреат II степени в номинации "Эстрадный вокал", г. Москва</span>
                                        </li>
                                        <li class="detail__section-item detail__section-item--secondary"><span class="detail__section-text">II Международный интернет конкурс "Белый КиТ", лауреат III степени в номинации "Эстрадный вокал. Соло", г. Москва</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="detail__column">
                            <div class="detail__section">
                                <div class="detail__section-header">
                                    <img class="detail__section-icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/datail-2.png" alt=""><span class="detail__section-title">Навыки:</span><i class="fa fa-angle-down"></i>
                                </div>
                                <div class="detail__section-content">
                                    <ul class="detail__section-list">
                                        <li class="detail__section-item"><span class="detail__section-text">Вокал</span>
                                        </li>
                                        <li class="detail__section-item"><span class="detail__section-text">Хореография</span>
                                        </li>
                                        <li class="detail__section-item"><span class="detail__section-text">Плавание</span>
                                        </li>
                                        <li class="detail__section-item"><span class="detail__section-text">Дефиле</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="detail__section">
                                <div class="detail__section-header">
                                    <img class="detail__section-icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/datail-6.png" alt=""><span class="detail__section-title">Знание языков</span><i class="fa fa-angle-down"></i>
                                </div>
                                <div class="detail__section-content">
                                    <ul class="detail__section-list">
                                        <li class="detail__section-item detail__section-item--language">
                                            <img class="detail__section-item-img" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/datail-7.png" alt=""><span class="detail__section-text">Английский</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="detail__column">
                            <div class="detail__section">
                                <div class="detail__section-header">
                                    <img class="detail__section-icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/datail-3.png" alt=""><span class="detail__section-title">Фильмография:</span><i class="fa fa-angle-down"></i>
                                </div>
                                <div class="detail__section-content">
                                    <ul class="detail__section-list">
                                        <li class="detail__section-item"><span class="detail__section-text">Нет опыта</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="detail__column">
                            <div class="detail__section">
                                <div class="detail__section-header">
                                    <img class="detail__section-icon" src="<?php echo get_stylesheet_directory_uri()?>/static/img/general/icons/datail-4.png" alt=""><span class="detail__section-title">Театр:</span><i class="fa fa-angle-down"></i>
                                </div>
                                <div class="detail__section-content">
                                    <ul class="detail__section-list">
                                        <li class="detail__section-item"><span class="detail__section-text">Нет опыта</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





</html>