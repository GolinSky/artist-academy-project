
<?php
    global $root_url_path;
    get_header();
    $uri = $_SERVER['REQUEST_URI'];
?>

        <div class="main page__main">
            <div class="main__header">
                <div class="main__container container">
                    <h3 class="main__title">Ученики мастерской</h3>
                    <ul class="breadcrumbs main__breadcrumbs">
                        <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/">Главная</a>
                        </li>
                        <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Ученики</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="main__content">
                <div class="students main__students">
                    <div class="students__container container">
                        <div class="students__header">
                            <form class="students__form" action="#" id ="radio_form">
                                <div class="students__grid grid">
                                    <div class="students__column column">
                                        <div class="students__wrap">
                                            <ul class="students__list">
                                                <li class="students__item" id = "boy_radio">
                                                    <label class="students__label">
                                                        <input type="radio" name="radio"  class="students__radio" onclick="gender_filter_click('boy:Мальчик')" />Мальчик
                                                    </label>
                                                </li>
                                                <li class="students__item"  id = "girl_radio">
                                                    <label class="students__label">
                                                        <input type="radio" name="radio" class="students__radio" onclick="gender_filter_click('girl:Девочка')" />Девочка
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="students__column column">
                                        <div class="students__wrap">
                                            <div class="students__box"><span class="students__box-text">Цвет волос:  <?php


                                            ?></span>
                                                <select class="students__box-select" data-placeholder="Не выбрано" onchange="hair_filter_click(this.value)" id="hair_selection">
                                                    <div class="scrollbar-inner">
                                                        <option value="array(Блондин, Рыжий, Шатен, Русый, Брюнет)">Любой</option>
                                                        <option value="Блондин">Блондин</option>
                                                        <option value="Рыжий">Рыжий</option>
                                                        <option value="Шатен">Шатен</option>
                                                        <option value="Русый">Русый</option>
                                                        <option value="Брюнет">Брюнет</option>
                                                    </div>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="students__column column">
                                        <div class="students__wrap">
                                            <div class="students__box" id = "select_student_age"><span class="students__box-text">Возраст ребенка: </span>
                                                <div class="students__dropbox">
                                                    <div class="students__box-age"><span class="students__box-placeholder">#</span><i class="fa fa-chevron-down"></i>
                                                    </div>
                                            <form id="age_form_selector">
                                                    <ul class="students__box-dropdown">
                                                        <li class="students__box-dropdown-item">
                                                            <label>
                                                                <input id ="age_checkbox" class="students__box-dropdown-chekbox" type="checkbox"value="array(0,3)" onchange="age_filter_click('array(0,3)',this.checked)"><span class="students__box-dropdown-label">0 - 3</span>
                                                            </label>
                                                        </li>

                                                        <li class="students__box-dropdown-item">
                                                            <label>
                                                                <input id ="age_checkbox" class="students__box-dropdown-chekbox" type="checkbox" value="array(4,6)" onchange="age_filter_click('array(4,6)',this.checked)"><span class="students__box-dropdown-label">4 - 6</span>
                                                            </label>
                                                        </li>
                                                        <li class="students__box-dropdown-item">
                                                            <label>
                                                                <input id ="age_checkbox" class="students__box-dropdown-chekbox" type="checkbox" value="array(7,10)" onchange="age_filter_click('array(7,10)',this.checked)"><span class="students__box-dropdown-label" >7 - 10</span>
                                                            </label>
                                                        </li>
                                                        <li class="students__box-dropdown-item">
                                                            <label>
                                                                <input id ="age_checkbox" class="students__box-dropdown-chekbox" type="checkbox" value="array(11,14)" onchange="age_filter_click('array(11,14)',this.checked)"><span class="students__box-dropdown-label">11 - 14</span>
                                                            </label>
                                                        </li>
                                                        <li class="students__box-dropdown-item">
                                                            <label>
                                                                <input id ="age_checkbox" class="students__box-dropdown-chekbox" type="checkbox" value="array(15,17)" onchange="age_filter_click('array(15,17)',this.checked)"><span class="students__box-dropdown-label">15 - 17</span>
                                                            </label>
                                                        </li>
                                                        <li class="students__box-dropdown-item">
                                                            <label>
                                                                <input id ="age_checkbox" class="students__box-dropdown-chekbox" type="checkbox" value="array(18,150)" onchange="age_filter_click('array(18,150)',this.checked)"><span class="students__box-dropdown-label">18+</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                            </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="students__column column">
                                        <div class="students__wrap">
                                            <ul class="students__list">
                                                <li class="students__item" id="age_radio">
                                                    <label class="students__label">
                                                        <input type="radio" name="radio" class="students__radio" onclick="sort_filter_click('student_age')"    />По возрасту
                                                    </label>
                                                </li>
                                                <li class="students__item" id="rating_radio">
                                                    <label class="students__label">
                                                        <input type="radio" name="radio" class="students__radio" onclick="sort_filter_click('rating')" />По рейтингу
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </form>

                        </div>

                        <div class="students__content">
                            <div class="students__grid grid" id="load_html"></div>
                        </div>
                        <?php
                            if($_GET['amount_query'] != -1)
                            {
                        ?>
                        <div class="students__footer" id="load_more_div">
                            <button class="button button-large students__button"  onclick="count_filter_click()">Показать больше </button>
                        </div>
                      <?php } ?>
                    </div>
                </div>
            </div>
        </div>
   <?php get_footer();?>

   <script>
    document.addEventListener("DOMContentLoaded", change_post_at_begins);

    function change_post_at_begins()
    {
        console.log(<?php get_query('amount_query', 'all') ?>);
        console.log(<?php get_query('gender_query', 'all') ?>);
        console.log(<?php get_query('hair_query', 'array(Блондин, Рыжий, Шатен, Русый, Брюнет)'); ?>);
        console.log(<?php get_query('age_query', 'array(0,150)'); ?>);
        console.log(<?php get_query('sort_query', 'title'); ?>);

        jQuery.ajax({
            async:true,
            url: '<?php echo admin_url( 'admin-ajax.php' );?>',
            data: {
                'action' : 'my_action',
                'amount_query': <?php echo get_query_int_value('amount_query', 8);?>,
                'gender_query': <?php get_query('gender_query', 'all'); ?>,
                'hair_query': <?php get_query('hair_query', 'array(Блондин, Рыжий, Шатен, Русый, Брюнет)'); ?>,
                'age_query': <?php get_query('age_query', 'array(0,150)'); ?>,
                'sort_query': <?php get_query('sort_query', 'title'); ?>,
                'use_cashe':true
            },
            type: 'POST',
            success: function(data) {
                jQuery("#load_html").html(data);
            }
        });
    }
</script>
