<div class="main page__main">
    <div class="main__header">
        <div class="main__container container">
            <h3 class="main__title"><?php the_title(); ?></h3>
            <ul class="breadcrumbs main__breadcrumbs">
                <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/">Главная</a>
                </li>
                <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#"><?php the_title(); ?></a>
                </li>
            </ul>
        </div>
    </div>
<style>
    .terms__content_direction
    {
        padding: 1.875rem 0;
    }


    .terms__content_direction > h3
    {
        color: #E46C46 !important;
        text-align: center !important;
        font-family: 'park' ;
        font-weight: 400 !important;
        padding: 3vh 0 3vh 0  !important;
    }

    .terms__content_direction > h2
    {
        color: #E46C46 !important;
        text-align: center !important;
        font-family: 'park' ;
        font-weight: 400 !important;
        padding: 3vh 0 3vh 0  !important;
    }

    .terms__content_direction > h1
    {
        color: #E46C46 !important;
        text-align: center !important;
        font-family: 'park' ;
        font-weight: 400 !important;
        padding: 3vh 0 3vh 0  !important;
    }

    .terms__content_direction > h4
    {
        color: #E46C46 !important;
        text-align: center !important;
        font-family: 'park' ;
        font-weight: 400 !important;
        padding: 3vh 0 3vh 0  !important;
    }

    .terms__content_direction
    {
        color: #8f8da6;
    }
    .terms__content_direction > ul
    {
        margin: 0;
        padding: 0;
        list-style: none;
    }

    .terms__content_direction >li
    {
        position: relative;
        padding-left: 1.25rem;
        margin-bottom: 1rem;
        line-height: 1;
    }

    .terms__content_direction ul li:before {
        content: '';
        position: absolute;
        left: 0;
        top: 0.375rem;
        width: 5px;
        height: 5px;
        border: 1.5px solid #6138D5;
        -webkit-transform: rotate(-45deg);
        transform: rotate(-45deg);
    }
</style>

    <div class="main__content" style="z-index:-1;position: relative;">
        <div class="terms main__terms">
            <div class="terms__container container">
                <div class="terms__body">
                    <div class="terms__section">
                        <div class="terms__content_direction">
<?php
global $root_url_path;
$content = get_the_content(get_the_ID());


$img_ex = '<img[^>]*src *= *["\']([^\'"]+)["\'][^>]*>';
$content = preg_replace_callback( "~(?:<a[^>]+>\s*)$img_ex|($img_ex)~", function($mm){
    // пропускаем, если картинка уже со ссылкой
    if( empty($mm[2]) )
        return $mm[0];

    //return '<a href="'. $mm[3] .'" data-fancybox="at" >'. $mm[2] .'</a>';
    return '<div class="terms__column"><a href="'. $mm[3] .'" data-fancybox="at" ><img class="terms__img" src="'. $mm[3] .'" alt=""></a></div>';

}, $content );
echo $content;


?>

                </div>
            </div>
        </div>
    </div>
</div>
    </div>
</div>
<!--<a href="<?php /*OutputImage("image1"); */?>" data-fancybox="at">
    <img class="terms__img" src="<?php /*OutputImage("image1"); */?>" alt="">
</a>-->

<!--<script src="<?php /*echo $root_url_path*/?>/functions/js/img-replacer.js"></script>
<script> replace_class(); </script>-->