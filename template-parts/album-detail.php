
<?php global $root_url_path;
$mini_galery = load_galery_by_acf('album_gallery_mini', get_the_ID());
$original_galery = load_galery_by_acf('album_gallery', get_the_ID());
?>

<div class="main page__main">
    <div class="main__header">
        <div class="main__container container">
            <h3 class="main__title">Галерея</h3>
            <ul class="breadcrumbs main__breadcrumbs">
                <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/">Главная</a>
                </li>
                <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="<?php get_url_by_page_slug('gallery'); ?>">Галерея</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="main__content">
        <div class="album-detail">
            <div class="album-detail__photo">
                <img class="album-detail__img" src="<?php echo get_field('album_background'); ?>" alt="">
                <div class="album-detail__gradient"></div>
            </div>
            <div class="album-detail__container container">
                <div class="album-detail__header">
                    <h3 class="album-detail__title"><?php the_title();?></h3>
                    <div class="badge album-detail__badge">
                        <img class="badge__icon" src="<?php echo $root_url_path; ?>/static/img/general/calendar-gray.svg" alt=""><span class="badge__text"><?php echo get_the_date( 'j F Y'); ?><!--21 ноября 2019 г.--></span>
                    </div><span class="album-detail__caption"><?php echo get_field('album_description'); ?> </span>
                </div>
                <div class="album-detail__content">
                    <div class="album-detail__grid">

                        <?php


                        for ($i = 0;$i < count($original_galery);$i++)
                        {
                            echo '<div class="album-detail__column">
                                    <a class="album-detail-card" href=' . $original_galery[$i][2][0]  . ' data-fancybox="dtl">
                                        <div class="album-detail-card__photo">
                                            <img class="album-detail-card__img" src=' . $mini_galery[$i][2][0]  . ' alt="">
                                        </div>
                                    </a>
                                </div>';
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





