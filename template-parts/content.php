<?php
/**
 * The template part for displaying content
 *

 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
	

		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
	</header><!-- .entry-header -->



	<div class="entry-content">
		
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		
	
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
