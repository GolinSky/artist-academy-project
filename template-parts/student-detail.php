
<?php global $root_url_path; ?>

<?php // Get the current value.
$count = (int) get_field('rating');

// Increase it.
$count++;

// Update with new value.
update_field('rating', $count);?>

<div class="main">
    <div class="main__header">
        <div class="main__container container">
            <h3 class="main__title"><?php the_title();?></h3>
            <ul class="breadcrumbs main__breadcrumbs">
                <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="<?php echo get_home_url();?>">Главная </a>
                </li>
                <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="<?php
                    $target_url = get_permalink(get_page_by_path( 'student' ));
                    echo $target_url;
                    ?>">Ученики  </a>
                </li>
                <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#"><?php the_title();?></a>
                </li>
            </ul>
            <div class="detail main__detail">
                <div class="detail__swiper">
                    <div class="detail__swiper-container swiper-container js-detail-swiper">
                        <div class="detail__swiper-wrapper swiper-wrapper">
                            <?php

                            $video_link = get_field('video_link');
                            if($video_link == '') {


                                ?>
                                <div class="detail__swiper-slide swiper-slide">
                                    <div class="detail-card detail-card--video">
                                        <a class="detail-card__photo" href="<?php echo $video_link; ?>"
                                           data-fancybox="detail">
                                            <img class="detail-card__img"
                                                 src="<?php echo $root_url_path ?>/static/img/general/detail-card-1.jpg"
                                                 alt="">
                                        </a>
                                        <div class="detail-card__data">
                                            <div class="detail-card__play">
                                                <div class="detail-card__play-icon">
                                                    <img class="detail-card__play-img"
                                                         src="<?php echo $root_url_path ?>/static/img/general/icons/play-big.svg"
                                                         alt="">
                                                </div>
                                                <div class="detail-card__play-link"><span
                                                            class="detail-card__play-text">Посмотреть ролик</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                            <?php
                            $mini_galery = load_galery_by_acf('student_media_mini', get_the_ID());
                            $original_galery = load_galery_by_acf('student_media', get_the_ID());

                            for ($i = 0;$i < count($mini_galery);$i++)
                            {
                                echo '<div class="detail__swiper-slide swiper-slide">
                                <div class="detail-card">
                                    <a class="detail-card__photo" href=' . $original_galery[$i][2][0]  . ' data-fancybox="detail">
                                        <img class="detail-card__img" src=' . $mini_galery[$i][2][0]. ' alt="">
                                    </a>
                                </div>
                            </div>';
                            }
                            ?>
                        </div>
                    </div>
                </div>

                <?php   if($video_link != '' or count($mini_galery) > 0) { ?>
                    <div class="swiper-controls detail__swiper-controls">
                        <div class="swiper-controls__actions">
                            <button class="swiper-controls__button swiper-controls__button--prev"></button>
                            <button class="swiper-controls__button swiper-controls__button--next"></button>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
    <div class="main__content">
        <div class="detail main__detail">
            <div class="detail__container container">
                <div class="detail__content">
                    <div class="detail__feedback"><span class="detail__feedback-title">Понравился Артист?</span>
                        <form class="detail__feedback-form" id = "artist_form">
                            <div class="detail__feedback-field">
                                <span class="detail__feedback-error-text">Неверно</span>
                                <input class="detail__feedback-input" type="text" placeholder="Ваше имя" name="artist_name" required>
                            </div>
                            <div class="detail__feedback-field">
                                <span class="detail__feedback-error-text">Неверно</span>
                                <input class="detail__feedback-input" type="email" placeholder="Электронная почта" name="artist_mail" required>
                            </div>
                            <div class="detail__feedback-field">
                                <span class="detail__feedback-error-text">Неверно</span>
                                <input class="detail__feedback-input" type="tel" placeholder="Номер телефона" name="artist_phone" required>
                            </div>
                            <button class="button detail__feedback-button" onclick = "return OnClickArtistForm('<?php echo $root_url_path . '\',\'' . get_the_title();?>')" href = "javascript:void(0);">Пригласить на кастинг</button>
                        </form>
                    </div>
                </div>
                <div class="detail__footer">
                    <ul class="detail__chars-list">
                        <li class="detail__chars-item">
                            <div class="detail__chars-icon">
                                <img class="detail__chars-img" src="<?php echo $root_url_path?>/static/img/general/icons/chars-1.png" alt="">
                            </div>
                            <div class="detail__chars-data"><span class="detail__chars-title">Возраст:</span><span class="detail__chars-text"><?php the_excerpt(); ?> </span>
                            </div>
                        </li>
                        <li class="detail__chars-item">
                            <div class="detail__chars-icon">
                                <img class="detail__chars-img" src="<?php echo $root_url_path?>/static/img/general/icons/chars-2.png" alt="">
                            </div>
                            <div class="detail__chars-data"><span class="detail__chars-title">Рост:</span><span class="detail__chars-text"><?php echo get_field( 'height' );?> см</span>
                            </div>
                        </li>
                        <li class="detail__chars-item">
                            <div class="detail__chars-icon">
                                <img class="detail__chars-img" src="<?php echo $root_url_path?>/static/img/general/icons/chars-3.png" alt="">
                            </div>
                            <div class="detail__chars-data"><span class="detail__chars-title">Цвет глаз:</span><span class="detail__chars-text"><?php echo get_field( 'eye_color' );?> </span>
                            </div>
                        </li>
                        <li class="detail__chars-item">
                            <div class="detail__chars-icon">
                                <img class="detail__chars-img" src="<?php echo $root_url_path?>/static/img/general/icons/chars-4.png" alt="">
                            </div>
                            <div class="detail__chars-data"><span class="detail__chars-title">Цвет волос:</span><span class="detail__chars-text"><?php echo get_field( 'hair_color' );?></span>
                            </div>
                        </li>
                    </ul>
                    <div class="detail__grid">

                        <div class="detail__column">
                            <div class="detail__section">
                                <div class="detail__section-header">
                                    <img class="detail__section-icon" src="<?php echo $root_url_path?>/static/img/general/icons/datail-1.png" alt=""><span class="detail__section-title">Образование:</span><i class="fa fa-angle-down"></i>
                                </div>
                                <div class="detail__section-content">
                                    <ul class="detail__section-list">
                                        <?php
                                        $education_list =  get_field( 'education' );
                                        $split_list = explode("\n", $education_list);
                                        if(count($split_list) > 0)
                                        {
                                            for ($i = 0; $i < count($split_list); $i++)
                                            {
                                                if($split_list[$i] != '')
                                                    echo '<li class="detail__section-item"><span class="detail__section-text">'. $split_list[$i] . '</span></li>';
                                            }
                                        }


                                        ?>


                                    </ul>
                                </div>
                            </div>

                            <div class="detail__section">
                                <div class="detail__section-header">
                                    <img class="detail__section-icon" src="<?php echo $root_url_path?>/static/img/general/icons/datail-5.png" alt=""><span class="detail__section-title">Конкурсы</span><i class="fa fa-angle-down"></i>
                                </div>
                                <div class="detail__section-content">
                                    <ul class="detail__section-list">

                                        <?php
                                        $education_list =  get_field( 'competition' );
                                        $split_list = explode("\n", $education_list);
                                        if(count($split_list) > 0)
                                        {
                                            for ($i = 0; $i < count($split_list); $i++)
                                            {
                                                if($split_list[$i] != '')
                                                {
                                                    if($i == 0)
                                                    {
                                                        echo '<li class="detail__section-item"><span class="detail__section-text">'. $split_list[$i] . '</span></li>';
                                                    }
                                                    else
                                                    {
                                                        echo '<li class="detail__section-item detail__section-item--secondary"><span class="detail__section-text">'. $split_list[$i] . '</span></li>';
                                                    }
                                                }
                                            }
                                        }
                                        ?>



                                    </ul>
                                </div>
                            </div>




                        </div>


                        <div class="detail__column">
                            <div class="detail__section">
                                <div class="detail__section-header">
                                    <img class="detail__section-icon" src="<?php echo $root_url_path?>/static/img/general/icons/datail-2.png" alt=""><span class="detail__section-title">Навыки:</span><i class="fa fa-angle-down"></i>
                                </div>
                                <div class="detail__section-content">
                                    <ul class="detail__section-list">

                                        <?php
                                        $education_list =  get_field( 'skills' );
                                        $split_list = explode("\n", $education_list);
                                        if(count($split_list) > 0)
                                        {
                                            for ($i = 0; $i < count($split_list); $i++)
                                            {
                                                if($split_list[$i] != '')
                                                {
                                                    echo '<li class="detail__section-item"><span class="detail__section-text">'. $split_list[$i] . '</span></li>';
                                                }
                                            }
                                        }
                                        ?>



                                    </ul>
                                </div>
                            </div>

                            <?php
                            $field = get_field_object('language_skills', get_the_ID());
                            $education_list =  $field['value'][0];
                            if(count($field['value'] ) > 0 &&  $education_list->post_type === 'language')
                            {
                                $path =  $root_url_path . '/static/img/general/icons/datail-6.png';
                                echo '  <div class="detail__section">
                                        <div class="detail__section-header">
                                            <img class="detail__section-icon" src="'.$path.'" alt=""><span class="detail__section-title">Знание языков</span><i class="fa fa-angle-down"></i>
                                        </div>
                                        <div class="detail__section-content">
                                            <ul class="detail__section-list">';


                                                $education_list =  $field['value'][0];


                                                   if(count($field['value'] ) > 0 )
                                                   {

                                                       for ($i = 0; $i < count($field['value']); $i++)
                                                       {

                                                           $education_list =  $field['value'][$i];
                                                            if($education_list->post_type === 'language')
                                                            {
                                                                echo ' <img class="detail__section-item-img" src="'
                                                                    . get_the_post_thumbnail_url($education_list->ID,'thumbnail' )
                                                                    .'" alt=""><span class="detail__section-text">'
                                                                    . $education_list->post_title . '</span><br>';
                                                            }


                                                       }
                                                   }
                                                ?>
                                            </ul>
                                        </div>
                                    </div>

                            <?php }?>
                        </div>
                        <div class="detail__column">

                            <div class="detail__section">
                                <div class="detail__section-header">
                                    <img class="detail__section-icon" src="<?php echo $root_url_path?>/static/img/general/icons/datail-3.png" alt=""><span class="detail__section-title">Фильмография:</span><i class="fa fa-angle-down"></i>
                                </div>
                                <div class="detail__section-content">
                                    <ul class="detail__section-list">
                                        <?php
                                        $education_list =  get_field( 'movie_experience' );
                                        $split_list = explode("\n", $education_list);
                                        if(count($split_list) > 0)
                                        {
                                            for ($i = 0; $i < count($split_list); $i++)
                                            {
                                                if($split_list[$i] != '')
                                                {
                                                    echo '<li class="detail__section-item"><span class="detail__section-text">'. $split_list[$i] . '</span></li>';
                                                }
                                            }
                                        }
                                        ?>
                                      <!--  <li class="detail__section-item"><span class="detail__section-text">Нет опыта</span>
                                        </li>-->
                                    </ul>
                                </div>
                            </div>
                            <?php
                            $other_list =  get_field( 'other_info' );
                            $split_list = explode("\n", $other_list);
                            $valid = false;
                            if(count($split_list) > 0) {
                                for ($i = 0; $i < count($split_list); $i++) {
                                    if ($split_list[$i] != '') {
                                        $valid = true;
                                    }
                                }
                            }

                            if($valid)
                            {
                                    ?>
                            <div class="detail__section">
                                <div class="detail__section-header">
                                    <img class="detail__section-icon" src="<?php echo $root_url_path?>/static/img/general/icons/others.svg" alt=""><span class="detail__section-title">Прочее:</span><i class="fa fa-angle-down"></i>
                                </div>
                                <div class="detail__section-content">
                                    <ul class="detail__section-list">
                                        <?php


                                            for ($i = 0; $i < count($split_list); $i++)
                                            {
                                                if($split_list[$i] != '')
                                                {
                                                    echo '<li class="detail__section-item"><span class="detail__section-text">'. $split_list[$i] . '</span></li>';
                                                }
                                            }

                                        ?>
                                        <!--  <li class="detail__section-item"><span class="detail__section-text">Нет опыта</span>
                                          </li>-->
                                    </ul>
                                </div>
                            </div>
                            <?php } ?>
                        </div>


                        <div class="detail__column">
                            <div class="detail__section">
                                <div class="detail__section-header">
                                    <img class="detail__section-icon" src="<?php echo $root_url_path?>/static/img/general/icons/datail-4.png" alt=""><span class="detail__section-title">Театр:</span><i class="fa fa-angle-down"></i>
                                </div>
                                <div class="detail__section-content">
                                    <ul class="detail__section-list">
                                        <?php
                                        $education_list =  get_field( 'theater_experience' );
                                        $split_list = explode("\n", $education_list);
                                        if(count($split_list) > 0)
                                        {
                                            for ($i = 0; $i < count($split_list); $i++)
                                            {
                                                if($split_list[$i] != '')
                                                {
                                                    echo '<li class="detail__section-item"><span class="detail__section-text">'. $split_list[$i] . '</span></li>';
                                                }
                                            }
                                        }
                                        ?>

                                      <!--  <li class="detail__section-item"><span class="detail__section-text">Нет опыта</span>
                                        </li>-->
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




</div>
<script type="text/javascript" src="<?php echo $root_url_path?>/functions/js/form/formArtist.js"></script>


