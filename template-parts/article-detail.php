<?php global $root_url_path;

$current_rating =(int) get_field("article_rating");
$current_rating++;
update_field("article_rating",$current_rating);
// Update with new value.
?>


<div class="main page__main">
    <div class="main__header">
        <div class="main__container container">
            <h3 class="main__title">Статьи</h3>
            <ul class="breadcrumbs main__breadcrumbs">
                <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="/">Главная</a>
                </li>
                <li class="breadcrumbs__item"><a class="breadcrumbs__link" href="#">Статьи</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="main__content">
        <div class="article main__article">
            <div class="article__container container">
                <div class="article__inner">
                    <div class="article__content">
                        <div class="article__header"><span class="article__title"><?php the_title(); ?></span><span class="article__date"><?php echo get_the_date( 'j.m.Y'); ?></span>
                        </div>
                        <div class="article__data">
                            <div class="article__swiper">
                                <div class="article__swiper-container swiper-container js-artcile-swiper">
                                    <div class="article__swiper-wrapper swiper-wrapper">

                                        <?php
                                        $original_galery = load_galery_by_acf('article_media(original)', get_the_ID());

                                        for ($i = 0;$i < count($original_galery);$i++)
                                        {
                                            echo '<div class="article__swiper-slide swiper-slide">
                                            <a class="article__fancybox" href=' . $original_galery[$i][2][0]  . ' data-fancybox="article">
                                                <img class="article__img" src=' . $original_galery[$i][2][0]. ' alt="">
                                            </a>
                                            </div>';
                                        }
                                        ?>

                                    </div>
                                </div>
                                <div class="article__swiper-thumbs">
                                    <div class="article__swiper-container swiper-container js-artcile-thumbs">
                                        <div class="article__swiper-wrapper swiper-wrapper">


                                        <?php
                                        for ($i = 0;$i < count($original_galery);$i++)
                                        {
                                            echo ' <div class="article__swiper-slide swiper-slide" >
                                                <img class="article__img" src=' . $original_galery[$i][2][0]. ' alt="" >
                                            </div>';
                                        }
                                        ?>

                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-controls article__swiper-controls">
                                    <div class="swiper-controls__actions">
                                        <button class="swiper-controls__button swiper-controls__button--prev"></button>
                                        <button class="swiper-controls__button swiper-controls__button--next"></button>
                                    </div>
                                </div>
                            </div>
                            <div class="article__section"><span class="article__section-title"> <?php the_title(); ?></span>
                               <?php the_content(); ?>
                            </div>

                        </div>
                    </div>


